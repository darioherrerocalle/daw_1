uf1

CREATE DATABASE BIBLIOTECA;
USE BIBLIOTECA;
CREATE TABLE USUARIO(
	dni varchar (9) primary key,
	nombre varchar (30) not null
);
CREATE TABLE LIBRO(
	codigo integer primary key,
	titulo varchar (30) not null
);
CREATE TABLE PRESTAMO(
	idPrestamo integer primary key,
	dni varchar (9),
	codigoLibro integer,
	fechaInicio date not null,
	fichaFin date not null
	constraint fk_dni foreign key (dni) references USUARIO(dni),
	constraint fk_codigo_libro foreign key (codigoLibro) references LIBRO(codigo)
);

ukf2

Mostrar el importe de las facturas con un importe superior a 1000 junto con el nombre y el DNI del cliente.
SELECT f.importe, c.nombre, c.DNI 
FROM clientes c INNER JOIN facturas f ON c.clientes=f.clientes
WHERE f.importe>1000;

Mostrar el nombre y el total de facturación (suma del importe de sus facturas) de cada cliente.
SELECT c.nombre as 'CLIENTE', SUM(f.importe) as 'Importe total'
FROM clientes c INNER JOIN facturas f ON c.clientes=f.clientes
GROUP BY c.nombre;

Mostrar el nombre y DNI del cliente que tiene la factura de mayor importe.
SELECT c.nombre, c.DNI 
FROM clientes c INNER JOIN facturas f ON c.clientes=f.clientes
WHERE f.importe=(
	SELECT MAX(importe) 
	FROM facturas);



Inserta un nuevo cliente, puedes inventarte los datos.
INSERT INTO clientes VALUES(
	'C10',
	'12234432A',
	'Gumersindo',
	'Madrid');

Añade un nuevo campo a la tabla Facturas, después de la Fecha, llamado PAGADA para almacenar el valor S o el valor N.
ALTER TABLE facturas
ADD COLUMN
	Pagada varchar(1) CHECK (pagada='S' or pagada='N') 
AFTER Fecha;

Asigna el valor N al campo PAGADA de todas las facturas de Enero con una única instrucción.
UPDATE facturas
SET pagada='N'
WHERE fecha BETWEEN '01/01/2021' AND '31/01/2021';

Bloquea la tabla de Facturas de tal forma que el resto de usuarios no puedan ni leerla ni modificarla.
LOCK TABLE facturas WRITE;


uf3

Crear un tablespace llamado TALLER que se almacena en el fichero c:\bd\taller.dbf de 200MB
CREATE TABLESPACE taller
DATAFILE 'c:\bd\taller.dbf'
SIZE 200M;

Crear un usuario llamado Juan con contraseña 1234, en el tablespace TALLER con espacio ilimitado.
CREATE USER Juan
IDENTIFIED BY "1234"
DEFAULT TABLESPACE taller
QUOTA UNLIMITED ON taller;

Asignar a Juan privilegios para iniciar sesión y para hacer un select en una tabla llamada VEHICULOS.
GRANT CREATE SESSION TO Juan;
GRANT SELECT ON vehiculos TO Juan;


Crear un rol llamado MECANICO.
CREATE ROLE mecanico;

Asignar a este rol los privilegios de iniciar sesión. También tendrá derechos para hacer modificaciones y select en la tabla VEHICULOS.
GRANT CREATE SESSION TO mecanico;
GRANT UPDATE, SELECT ON vehiculos TO mecanico;

Asignar este rol al usuario Juan.
GRANT mecanico TO Juan;



La declaración de un cursor llamado c_trabajadores que contiene el resultado de una consulta que obtiene todos los datos de la tabla trabajadores.

La instrucción que abre el cursor

La instrucción que carga el contenido del cursor en registro.

La instrucción que cierra el cursor.

DECLARE
	registro trabajadores%ROWTYPE;
	CURSOR c_trabajadores IS
		SELECT * FROM trabajadores;
BEGIN
	OPEN c_trabajadores
	LOOP
		FETCH c_trabajadores INTO fila;
		EXIT WHEN c_trabajadores%notfound;
		dbms_output.put_line(registro.dni || ' ' || registro.nombre);
	END LOOP;
	CLOSE c_trabajadores;
END;


Se quiere crear una base de datos objeto-relacional para guardar la información 
de la venta de coches de segunda mano. 
Almacenaremos la información del coche y los datos del comprador y 
del vendedor utilizando un esquema objeto-relacional.

Crearemos un objeto PERSONA para almacenar el nombre y el dni
CREATE OR REPLACE TYPE tipopersona AS OBJECT(
	nombre number,
	dni varchar2(9)
);

Crearemos un objeto COCHE para almacenar: matrícula, marca, modelo, año, km, 
el vendedor (del tipo persona) y el comprador (del tipo persona) y
un método que calcule el precio en función de los km y del año 
(Si tiene más de 100.000 km, el precio será el año. 
En cambio, si tiene menos de 100.000 km, el precio será el año + 1000 €).
CREATE OR REPLACE TYPE tipocoche AS OBJECT(
	matricula varchar2(7),
	marca varchar2(20),
	modelo varchar2(20),
	anno number,
	km number,
	vendedor tipopersona,
	comprador tipopersona,
	MEMBER FUNCTION calculoPrecio RETURN number
);

CREATE OR REPLACE TYPE BODY tipocoche AS
	MEMBER FUNCTION calculoPrecio RETURN number
	IS
	precio number;
	BEGIN
		IF km<=100000
		THEN precio:=anno+1000;
		ELSE precio:=anno;
		END IF;
		RETURN precio;
	END;
END;

Escribe todas las instrucciones necesarias para la definición de estos objetos (incluídas las instrucciones del cuerpo).


Crear una tabla de coches con identidad de objeto.
CREATE TABLE tablacoche OF tipocoche(
	matricula primary key
);

Insertar un coche junto con su comprador y su vendedor.
INSERT INTO tablacoche VALUES(
	'1111aaa',
	'bmw',
	'x6',
	2020,
	1000,
	tipopersona('dario', '12332133k'),
	tipopersona('maria','54445676a')
);

Mostrar los datos del vehículo junto con los de su vendedor.
SELCT *, DEREF(tc.vendedor)
FROM tablacoche tc;