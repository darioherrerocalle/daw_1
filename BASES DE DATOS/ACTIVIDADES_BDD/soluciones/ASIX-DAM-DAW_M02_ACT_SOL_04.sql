/*------------------------------------------------------------------------------------------------------*/
/*-------------------------------------- SOLUCIÓN ACT04 ------------------------------------------------*/
/*------------------------------------------------------------------------------------------------------*/

/* Base de datos CONCESIONARIO */
DROP DATABASE IF EXISTS CONCESIONARIO_SOL;
/*•	Crear una base de datos llamada concesionario*/
CREATE DATABASE CONCESIONARIO_SOL;
USE CONCESIONARIO_SOL;

/*•	Implementar las siguientes tablas con su relación. (El nombre del campo debe coincidir con el 
título de la columna y el tipo de datos estará en función del contenido de las mismas).*/
CREATE TABLE TIENDA(
	idtienda tinyint auto_increment primary key,
    nombre char(30),
    ciudad char(30),
    num_trabajadores tinyint,
    superficie mediumint
);
INSERT INTO TIENDA VALUES(null,'Auto 2','Madrid',5,1250);
INSERT INTO TIENDA VALUES(null,'Multimarca Total','Madrid',8,1750);
INSERT INTO TIENDA VALUES(null,'CarAuto','Barcelona',10,2000);
INSERT INTO TIENDA VALUES(null,'Turismos Díaz','Barcelona',5,1000);
INSERT INTO TIENDA VALUES(null,'BarnaCar','Barcelona',15,3000);
SELECT * FROM TIENDA;

CREATE TABLE VEHICULOS(
	matricula varchar(8) primary key,
    marca varchar(30),
    modelo varchar(30),
    color varchar(30),
    cilindrada decimal(2,1),
    antiguedad mediumint,
    km int,
    precio int,
    tienda tinyint,
    CONSTRAINT fk_vehiculos_tienda FOREIGN KEY (tienda) REFERENCES TIENDA(idtienda)
);
INSERT INTO VEHICULOS VALUES('1213-CRX','Seat','León','Negro',1.8,2004,180000,2000,1);
INSERT INTO VEHICULOS VALUES('3243-HTN','Seat','Altea','Rojo',1.2,2013,85000,7900,2);
INSERT INTO VEHICULOS VALUES('6643-KBM','Seat','Ibiza','Blanco',1.0,2017,25000,10900,2);
INSERT INTO VEHICULOS VALUES('8265-HZL','Nissan','Juke','Blanco',1.6,2014,110000,13900,3);
INSERT INTO VEHICULOS VALUES('8919-HHH','Nissan','Qashqai','Gris',2.0,2011,200000,8500,4);
INSERT INTO VEHICULOS VALUES('7623-GRS','Volkswagen','Tiguan','Gris',2.0,2010,130000,12000,1);
INSERT INTO VEHICULOS VALUES('4901-KPS','Volkswagen','Polo','Azul',1.0,2018,10000,11500,2);
INSERT INTO VEHICULOS VALUES('6841-LBN','Volkswagen','Golf','Rojo',1.6,2019,15000,22500,null);

SELECT * FROM VEHICULOS;


/*1.	En nuestra base de datos concesionario queremos almacenar la información referente 
a las empresas a la que pertenecen cada tienda. 
Crea la siguiente tabla teniendo en cuenta que el nombre del campo debe coincidir 
con el título de la columna y el tipo de datos estará en función del contenido.
Tabla: Grupo
idgrupo	empresa	ciudad	presidente	fundacion
1	Madrid Central	Madrid	Sebastián Amaya	2000
2	Cars Holding	Barcelona	Carmen Álvarez	2003
*/
CREATE TABLE GRUPO(
	idgrupo tinyint primary key,
	empresa char(50),
    ciudad varchar(50),
    presidente char(100),
    fundacion int
);

/*2.	Añade a la tabla tienda un campo llamado idgrupo y crea una relación entre el grupo y las tiendas. 
Un grupo tiene muchas tiendas, y una tienda solo pertenece a un grupo.*/
ALTER TABLE TIENDA ADD COLUMN idgrupo tinyint;
ALTER TABLE TIENDA ADD CONSTRAINT fk_tienda_grupo FOREIGN KEY(idgrupo) REFERENCES GRUPO(idgrupo);

SELECT * FROM TIENDA;

/*3.	Añade la información a la tabla grupo.*/
INSERT INTO GRUPO VALUES(1,'Madrid Central','Madrid','Sebastián Amaya',2000);
INSERT INTO GRUPO VALUES(2,'Cars Holding','Barcelona','Carmen Álvarez',2003);
SELECT * FROM GRUPO;

/*4.	Asigna a cada tienda su grupo: Auto 2 y CarAuto pertenecen al grupo Madrid Central 
y las restantes tiendas a Cars Holding.*/
UPDATE TIENDA
SET idgrupo=1
WHERE nombre IN ('Auto 2','CarAuto');
UPDATE TIENDA
SET idgrupo=2
WHERE nombre NOT IN ('Auto 2','CarAuto');

SELECT * FROM TIENDA;


/*5.	Muestra el nombre del grupo con el total de trabajadores y el total de superficie de exposición.*/
SELECT g.empresa,SUM(t.num_trabajadores),SUM(superficie)
FROM GRUPO g INNER JOIN TIENDA t ON g.idgrupo=t.idgrupo
GROUP BY g.empresa;

/*6.	Muestra para cada vehículo con cilindrada superior a 1.4, su marca, modelo, precio, km, 
la tienda y el grupo al que pertenece.*/
SELECT v.marca,v.modelo,v.precio,v.km,v.cilindrada,t.nombre,g.empresa
FROM GRUPO g INNER JOIN TIENDA t ON g.idgrupo=t.idgrupo
		     INNER JOIN VEHICULOS v ON t.idtienda=v.tienda
WHERE v.cilindrada>1.4;

/*7.	Turismos Díaz amplía su superficie a 2500 metros y ahora dispone de 12 trabajadores. 
Realiza este cambio en la tabla.*/
UPDATE TIENDA
SET superficie=2500,num_trabajadores=12
WHERE nombre='Turismos Díaz';

SELECT * FROM TIENDA;

/*8.	Añade a la tabla vehiculos un campo llamado financiado después del campo precio. 
Rellena este campo con el precio del vehículo más un 10%.*/
ALTER TABLE VEHICULOS
ADD COLUMN financiado decimal(10,2)
AFTER precio;
UPDATE VEHICULOS
SET financiado=precio*1.10;

SELECT * FROM VEHICULOS;

/*9.	Añade a la tabla vehículos un campo llamado descuento. 
Rellena este campo con el 10% de su precio pero solo aquellos vehículos 
con una antigüedad inferior a 2015 y un precio inferior a 10000.*/
ALTER TABLE VEHICULOS
ADD COLUMN descuento decimal(10,2);
UPDATE VEHICULOS
SET descuento=precio*0.10
WHERE antiguedad<2015 and precio<10000;

SELECT * FROM VEHICULOS;

/*10.	Crear una base de datos llamada “copiaseguridad” y copiar todos los datos actuales. 
Deben de utilizarse instrucciones SQL. */
DROP DATABASE IF EXISTS COPIASEGURIDAD;
CREATE DATABASE IF NOT EXISTS COPIASEGURIDAD;
CREATE TABLE COPIASEGURIDAD.GRUPO
AS SELECT * FROM GRUPO;
CREATE TABLE COPIASEGURIDAD.TIENDA
AS SELECT * FROM TIENDA;
CREATE TABLE COPIASEGURIDAD.VEHICULOS
AS SELECT * FROM VEHICULOS;

SELECT cg.*,ct.*,cv.*
FROM COPIASEGURIDAD.GRUPO cg INNER JOIN COPIASEGURIDAD.TIENDA ct ON cg.idgrupo=ct.idgrupo
							 INNER JOIN COPIASEGURIDAD.VEHICULOS cv ON ct.idtienda=cv.tienda;

/*11.	Crea  una vista con todos los vehículos del grupo Madrid Central. 
La vista mostrará el nombre de la tienda y todos los datos del vehículo. 
La vista se llamará VistaMadridCentral*/
CREATE VIEW VistaMadridCentral
AS
SELECT t.nombre,v.*
FROM grupo g INNER JOIN tienda t ON g.idgrupo=t.idgrupo
			 INNER JOIN vehiculos v ON t.idtienda=v.tienda
WHERE g.empresa='Madrid Central';

SELECT * FROM VistaMadridCentral;

/*12.	Crea una vista actualizable con todos los vehículos de la marca Seat. 
Comprueba que puedes añadir un registro a esta vista. 
La vista se llamará VistaSeat. 
Solo podemos añadir vehículos de la marca Seat.*/

-- Creación
CREATE VIEW VistaSeat
AS
SELECT * FROM VEHICULOS
WHERE marca='Seat'
WITH CHECK OPTION;

-- Comprobación
SELECT* FROM VistaSeat;
-- Ésta no se puede añadir
INSERT INTO VistaSeat VALUES ('8402-LBR','Volkswagen','Ateca','Blanco',2.1,2020,84,25000,27500,1,2500);
-- Ésta sí que se puede añadir
INSERT INTO VistaSeat VALUES ('8402-LBR','Seat','Ateca','Blanco',2.0,2020,84,25000,27500,1,2500);
SELECT* FROM VistaSeat;

/*13.	Empieza una transacción con la instrucción BEGIN. 
Incrementa 500 € el precio de todos los vehículos de la marca Seat. 
Ejecuta un ROLLBACK. ¿Qué ha sucedido?*/
BEGIN;
UPDATE VEHICULOS
SET precio=precio+500
WHERE marca='Seat';
ROLLBACK;

-- Comprobación
SELECT * FROM VEHICULOS WHERE marca='Seat';
-- No ha sucedido nada porque se ha desecho la actualización con el rollback

/*14.	Empieza una transacción con la instrucción BEGIN. 
Incrementa 500€ el precio de todos los vehículos de la marca Nissan. 
Ejecuta un COMMIT. 
¿Qué ha sucedido?*/
SELECT * FROM VEHICULOS WHERE marca='Nissan';

BEGIN;
UPDATE VEHICULOS
SET precio=precio+500
WHERE marca='Nissan';
COMMIT;

-- Comprobación
SELECT * FROM VEHICULOS wHERE marca='Nissan';
-- Al encontrar el COMMIT ha ejecutado la actualización

/*15.	Empieza una transacción con la instrucción BEGIN. 
Incrementa 500 € el precio de todos los vehículos de la marca Seat. 
Define un punto de control llamado PASO1. 
Borra todos los vehículos de la marca Nissan. 
Haz un ROLLBACK al PASO1, y luego realiza un COMMIT. 
Comprueba y comenta que ha sucedido.*/
BEGIN;
UPDATE VEHICULOS
SET precio=precio+500
WHERE marca='Seat';
SAVEPOINT PASO1;
DELETE FROM VEHICULOS WHERE marca='Nissan';
ROLLBACK TO PASO1;
COMMIT;

-- Comprobación
SELECT * FROM VEHICULOS WHERE marca='Seat' or marca='Nissan';
-- Al encontrar el rollback ha deshecho la instrucción del borrado

/*16.	Bloquear la tabla vehiculos por escritura e intentar modificar alguna fila de datos. 
Comprobara y explicar, con comentarios en el código, qué sucede.*/
LOCK TABLE VEHICULOS WRITE;
UPDATE VEHICULOS
SET tienda=2
WHERE modelo='Ateca';
SELECT * FROM VEHICULOS WHERE marca='Seat';
UNLOCK TABLES;
-- A mi me permite escribir porque soy el mismo usuario que la ha bloqueado pero cualquier otro
-- usuario no hubiese podido ni leerla ni modificarla.