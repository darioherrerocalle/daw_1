 -- 1. Crear un tablespace de 400MB llamado academia donde se almacenaran todos los datos.
--DROP TABLESPACE academia INCLUDING CONTENTS AND DATAFILES;
CREATE TABLESPACE academia
DATAFILE 'C:\archivos_oracle_marta\academia.dbf'
SIZE 400M
AUTOEXTEND ON;

-- 2. Crea un usuario llamado "world" que tenga todos los privilegios en el sistema Oracle.
CREATE USER world
IDENTIFIED BY "1234"
DEFAULT TABLESPACE academia
QUOTA UNLIMITED ON academia;
GRANT DBA TO world;

--Comprobar que realmente tiene asignados estos permisos.
SELECT * FROM DBA_ROLE_PRIVS
WHERE GRANTEE='WORLD';

/*3. Utilizando el usuario de nombre "world", crea la tabla cursos y la tabla alumnos. 
Utiliza el tipo de campo y la longitud que creas más adecuados para cada uno de los campos.*/

--Realizamos la conexión con el usuario World y luego creamos las tablas
--DROP TABLE CURSOS;
CREATE TABLE CURSOS(
    cod_curso int NOT NULL PRIMARY KEY,
    nombre VARCHAR2(50),
    horario VARCHAR(20),
    fecha_inicio DATE,
    fecha_final DATE,
    precio float,
    Profesor VARCHAR2(50)
);
--DROP TABLE ALUMNOS;
CREATE TABLE ALUMNOS(
    numero_alumno INT PRIMARY KEY,
    nombre VARCHAR2(50),
    curso int,
    fecha_inscripcion DATE,
    CONSTRAINT fk_alumno_curso FOREIGN KEY (curso) REFERENCES CURSOS(cod_curso)
);

--Introduce datos en las tablas.
INSERT INTO CURSOS VALUES (1,'Inglés','09:00-10:00','01/01/2020','30/06/2020',1000,'David');
INSERT INTO CURSOS VALUES (2,'Alemán','11:00-13:00','02/02/2020', '30/06/2020',4000, 'Irene');
INSERT INTO CURSOS VALUES (3,'Chino','15:00-17:00','03/03/2020', '30/08/2020',5000, 'Andrés');
INSERT INTO CURSOS VALUES (4,'Francés','17:00-19:00','04/04/2020', '30/09/2020',4000, 'Inés');
INSERT INTO ALUMNOS VALUES (1,'José García Martínez',1,'20/12/2019');
INSERT INTO ALUMNOS VALUES (2,'Julio Cañizares Rojo',2,'21/03/2019');
INSERT INTO ALUMNOS VALUES (3,'Sara Palomares Tejedor',2,'01/01/2020');
INSERT INTO ALUMNOS VALUES (4,'Sandra Gómez Marín',1,'01/12/2019');

/*4.Crear dos usuarios secre1 y secre2 con password world1234, 
que se encarguen de la gestión de la academia (añadir, modificar, borrar, consultar) 
en la tabla de cursos y en la tabla de alumnos.  Comprueba que los privilegios se han 
asignado de forma correcta y que puede hacer las operaciones asignadas.*/

--Creación y asignación de permisos de secre1
CREATE USER secre1
IDENTIFIED BY "world1234"
DEFAULT TABLESPACE academia
QUOTA UNLIMITED ON academia;
GRANT CREATE SESSION TO secre1;
GRANT INSERT, UPDATE, DELETE, SELECT ON alumnos TO secre1;
GRANT INSERT, UPDATE, DELETE, SELECT ON cursos TO secre1;

--Creación y asignación de permisos de secre2
CREATE USER secre2
IDENTIFIED BY "world1234"
DEFAULT TABLESPACE academia
QUOTA UNLIMITED ON academia;
GRANT CREATE SESSION TO secre2;
GRANT INSERT, UPDATE, DELETE, SELECT ON alumnos TO secre2;
GRANT INSERT, UPDATE, DELETE, SELECT ON cursos TO secre2;

--Comprobamos que los usuarios se han creado y que tienen los permisos asignados:
SELECT * FROM DBA_TAB_PRIVS WHERE TABLE_NAME='ALUMNOS' OR TABLE_NAME='CURSOS';

--Comprobamos que pueden realmente hacer las operacionas asignadas
--Hacemos la conexión con secre1
INSERT INTO world.cursos VALUES (5,'Ruso','19:00-21:00','05/05/2020', '30/10/2020',4000, 'Viktor');
SELECT * FROM world.cursos;
INSERT INTO world.alumnos VALUES (5,'Juan Pozo González',5,'30/04/2020');
SELECT * FROM world.alumnos;
UPDATE world.cursos SET profesor='Aleksander' WHERE nombre='Ruso';
UPDATE world.alumnos SET fecha_inscripcion=fecha_inscripcion+1 WHERE curso=1;
SELECT * FROM world.alumnos;
DELETE FROM world.alumnos WHERE nombre LIKE 'Juan%';
SELECT * FROM world.alumnos;
--Repetimos el proceso haciendo la conexión con secre2

/*5. Se decide que el usuario secre1 pueda crear nuevos usuarios, 
pero no podrá eliminar a ningún usuario.*/
--conectamos al usuario world
GRANT CREATE USER TO secre1;
-- Comprobamos los permisos de secre1
SELECT * FROM DBA_SYS_PRIVS 
WHERE GRANTEE='SECRE1';
--Quitamos el permiso de borrar registros al usuario secre2 sobre la tabla de cursos.
REVOKE DELETE ON cursos from secre2;
--Comprobación
SELECT * FROM DBA_TAB_PRIVS WHERE TABLE_NAME='CURSOS' AND GRANTEE='SECRE2';

/*6. El usuario world concede al usuario secre2 la posibilidad de asignar el permiso 
de lectura (SELECT) de datos a otros usuarios sobre la tabla de alumnos.*/ 
GRANT SELECT ON alumnos TO secre2 WITH GRANT OPTION;
-- Comprobar que realmente tiene asignados estos permisos.
SELECT * FROM DBA_TAB_PRIVS WHERE GRANTEE='SECRE2' AND TABLE_NAME='ALUMNOS';

/*7. Crea un rol llamado rolprofe con las siguientes características: 
Puede iniciar sesión, leer la tabla de cursos y leer y modificar la tabla de alumnos 
(no puede ni borrar ni añadir)*/
CREATE ROLE rolprofe;
GRANT CREATE SESSION TO rolprofe;
GRANT SELECT ON cursos TO rolprofe;
GRANT SELECT, ALTER ON alumnos TO rolprofe;

-- Comprobación
SELECT * FROM DBA_TAB_PRIVS WHERE GRANTEE='ROLPROFE';

/*8. Crea dos usuarios profe1 y profe2 con password profe1234 y le asignas el rol anterior.
Comprueba que tienen los permisos correspondientes.*/
-- Creación y asignación de permisos del usuario profe1
CREATE USER profe1
IDENTIFIED BY "profe1234"
DEFAULT TABLESPACE academia
QUOTA UNLIMITED ON academia;
-- Creación y asignación de permisos del usuario profe2
CREATE USER profe2
IDENTIFIED BY "profe1234"
DEFAULT TABLESPACE academia
QUOTA UNLIMITED ON academia;
--Asignamos el rol a los dos usuarios
GRANT rolprofe TO profe1,profe2;
-- Comprobación
SELECT * FROM DBA_ROLE_PRIVS WHERE GRANTEE='PROFE1' OR GRANTEE='PROFE2';
--Conectamos al usuario profe1 y probamos que pueda seleccionar la tabla cursos y la de alumnos
SELECT * FROM world.alumnos;
SELECT * FROM world.cursos;
--Comprobamos que puede modificar la tabla de los alumnos pero no la de los cursos
ALTER TABLE world.alumnos ADD email VARCHAR2(60);
SELECT * FROM world.alumnos;
ALTER TABLE world.cursos DROP COLUMN fecha_fin;

/*9. Crearemos un perfil para los profesores llamado perfilprofe que tenga un tiempo máximo de conexión de 1 hora,
dos conexiones simultáneas y le obligue a cambiar la contraseña cada 30 días.*/
--Conectamos al usuario world que es el único que tiene permisos para crear perfiles
CREATE PROFILE perfilprofe LIMIT
CONNECT_TIME 60
SESSIONS_PER_USER 2
PASSWORD_LIFE_TIME 30;

--Asigna este perfil al usuario profe1.
ALTER USER profe1
PROFILE perfilprofe;