/*DROP DATABASE ARURAL;*/
CREATE DATABASE ARURAL;
USE ARURAL;
CREATE TABLE ALOJAMIENTO(
	Cod_alojamiento mediumint primary key auto_increment,
    Nombre char(50),
    Direccion char(200),
    Telefono integer,
    Categoria varchar(30),
    Num_habitaciones tinyint
);
CREATE TABLE PERSONAL(
	Cod_personal mediumint primary key auto_increment,
    Nombre char(30),
    Apellido1 char(30),
    Apellido2 char(30) null,
    NIF varchar(9) not null,
    Direccion char(200),
    Cod_alojamiento mediumint,
    CONSTRAINT fk_personal_aloj FOREIGN KEY (Cod_alojamiento) REFERENCES ALOJAMIENTO(Cod_alojamiento)
);
CREATE TABLE HABITACION(
	Num_hab mediumint,
    Banyo_privado bool,
    Precio decimal(5,2),
    Tipo varchar(10) CHECK (Tipo IN ('individual','doble','suite')),
    Cod_alojamiento mediumint,
    CONSTRAINT pk_habitacion PRIMARY KEY (Cod_alojamiento,Num_hab),
    CONSTRAINT fk_hab_aloj FOREIGN KEY (Cod_alojamiento) REFERENCES ALOJAMIENTO(Cod_alojamiento)
);
CREATE TABLE ACTIVIDAD(
	Cod_Actividad mediumint primary key auto_increment,
    Nombre char(30),
    Nivel tinyint check(Nivel IN (1,2,3,4,5,6,7,8,9,10)),
	Descripcion char(100)
);
CREATE TABLE REALIZA(
	Cod_alojamiento mediumint,
    Cod_actividad mediumint,
    dia varchar(10),
    CONSTRAINT pk_realiza PRIMARY KEY (Cod_alojamiento,Cod_actividad,dia),
    CONSTRAINT fk_realiza_aloj FOREIGN KEY (Cod_alojamiento) REFERENCES ALOJAMIENTO(Cod_alojamiento),
    CONSTRAINT fk_realiza_act FOREIGN KEY (Cod_actividad) REFERENCES ACTIVIDAD(Cod_Actividad)
);
/*INDICES*/
CREATE FULLTEXT INDEX Nombre_Alojamiento_Categoria ON ALOJAMIENTO(Categoria,Nombre);
CREATE UNIQUE INDEX NIF_Personal ON PERSONAL(NIF) USING HASH;
