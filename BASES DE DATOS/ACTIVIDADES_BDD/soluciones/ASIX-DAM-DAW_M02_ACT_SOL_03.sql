DROP DATABASE IF EXISTS CONCESIONARIO_SOL;
/*•	Crear una base de datos llamada concesionario*/
CREATE DATABASE CONCESIONARIO_SOL;
USE CONCESIONARIO_SOL;

/*•	Implementar las siguientes tablas con su relación. (El nombre del campo debe coincidir con el 
título de la columna y el tipo de datos estará en función del contenido de las mismas).*/
CREATE TABLE TIENDA(
	idtienda tinyint auto_increment primary key,
    nombre char(30),
    ciudad char(30),
    num_trabajadores tinyint,
    superficie mediumint
);
INSERT INTO TIENDA VALUES(null,'Auto 2','Madrid',5,1250);
INSERT INTO TIENDA VALUES(null,'Multimarca Total','Madrid',8,1750);
INSERT INTO TIENDA VALUES(null,'CarAuto','Barcelona',10,2000);
INSERT INTO TIENDA VALUES(null,'Turismos Díaz','Barcelona',5,1000);
INSERT INTO TIENDA VALUES(null,'BarnaCar','Barcelona',15,3000);
SELECT * FROM TIENDA;

CREATE TABLE VEHICULOS(
	matricula varchar(8) primary key,
    marca varchar(30),
    modelo varchar(30),
    color varchar(30),
    cilindrada decimal(2,1),
    antiguedad mediumint,
    km int,
    precio int,
    tienda tinyint,
    CONSTRAINT fk_vehiculos_tienda FOREIGN KEY (tienda) REFERENCES TIENDA(idtienda)
);
INSERT INTO VEHICULOS VALUES('1213-CRX','Seat','León','Negro',1.8,2004,180000,2000,1);
INSERT INTO VEHICULOS VALUES('3243-HTN','Seat','Altea','Rojo',1.2,2013,85000,7900,2);
INSERT INTO VEHICULOS VALUES('6643-KBM','Seat','Ibiza','Blanco',1.0,2017,25000,10900,2);
INSERT INTO VEHICULOS VALUES('8265-HZL','Nissan','Juke','Blanco',1.6,2014,110000,13900,3);
INSERT INTO VEHICULOS VALUES('8919-HHH','Nissan','Qashqai','Gris',2.0,2011,200000,8500,4);
INSERT INTO VEHICULOS VALUES('7623-GRS','Volkswagen','Tiguan','Gris',2.0,2010,130000,12000,1);
INSERT INTO VEHICULOS VALUES('4901-KPS','Volkswagen','Polo','Azul',1.0,2018,10000,11500,2);
INSERT INTO VEHICULOS VALUES('6841-LBN','Volkswagen','Golf','Rojo',1.6,2019,15000,22500,null);

SELECT * FROM VEHICULOS;




/*1. Consulta que muestre todas las tiendas de más de 1500 metros, ordenadas por el nombre de la tienda.*/
SELECT * FROM TIENDA
WHERE superficie>1500
ORDER BY nombre;

/*2. Consulta que muestre la marca y el modelo de los vehículos que sean blancos o su antigüedad 
sea inferior a 2012.*/
SELECT marca,modelo
FROM VEHICULOS
WHERE color='Blanco' OR antiguedad<2012;

/*3. Consulta que muestre el nombre de la tienda, la marca, el modelo y el precio del vehículo.*/
SELECT t.nombre as 'tienda',v.marca,v.modelo,v.precio
FROM TIENDA t INNER JOIN VEHICULOS v ON t.idtienda=v.tienda;

/*4. ¿Cuántos vehículos tenemos de cada marca?*/
SELECT marca,COUNT(*) as 'cantidad de vehículos'
FROM VEHICULOS
GROUP BY marca;

/*5. ¿Cuál es el importe total de los vehículos de cada tienda?*/
SELECT t.nombre as 'tienda',SUM(v.precio)
FROM TIENDA t INNER JOIN VEHICULOS v ON t.idtienda=v.tienda
GROUP BY t.nombre;

/*6.	Mostrar la marca, modelo, km y la tienda de cada vehículo. 
Si un vehículo no está en ninguna tienda también debe salir.*/
SELECT v.marca,v.modelo,v.km,t.nombre as 'tienda'
FROM TIENDA t RIGHT JOIN VEHICULOS v ON t.idtienda=v.tienda;

/*7. Mostrar la media de km de los vehículos de la marca Seat.*/
SELECT AVG(km) as 'media'
FROM VEHICULOS
WHERE marca='Seat';

/*8.¿Cuál es la media de km y de precio de los vehículos con una antigüedad inferior a 2015?*/
SELECT AVG(km) as 'media de km',AVG(precio) as 'media de precio'
FROM VEHICULOS
WHERE antiguedad<2015;

/*9. Mostrar la tienda y la suma de km de sus vehículos, solo de aquellas tiendas que la suma de km de 
sus vehículos es superior a 150000.*/
SELECT t.nombre as 'tienda',SUM(v.km) as 'suma de km'
FROM TIENDA t INNER JOIN VEHICULOS v ON t.idtienda=v.tienda
GROUP BY t.nombre
HAVING SUM(v.km)>150000;

/*10. Mostrar la marca y el modelo de los vehículos que no están en ninguna tienda.*/
SELECT marca,modelo
FROM VEHICULOS
WHERE tienda IS NULL;

/*11.	Mostrar la marca, el modelo, el precio y una nueva columna con un 10% sobre el precio a la que 
llamaremos descuento de los vehículos con más de 100000 km y un precio menor 10000€.*/
SELECT marca,modelo,precio,precio*0.90 as 'descuento'
FROM VEHICULOS
WHERE km>100000 and precio<10000;

/*12. Marca y modelo del vehículo de mayor antigüedad.*/
SELECT marca,modelo
FROM VEHICULOS
WHERE antiguedad=(SELECT MIN(antiguedad) FROM vehiculos);

/*13. Marca y modelo de los vehículos que tienen un importe superior al vehículo de la marca Seat más caro.*/
SELECT marca,modelo
FROM VEHICULOS
WHERE precio>(SELECT MAX(precio) FROM VEHICULOS WHERE marca='Seat');

/*14. Qué antigüedad tiene el vehículo con más km que no es de color blanco ni de la marca Volkswagen.*/
SELECT antiguedad
FROM VEHICULOS
WHERE km=(SELECT MAX(km) 
          FROM VEHICULOS 
          WHERE NOT color='Blanco' AND NOT marca='Volkswagen');

/*15. Mostrar los vehículos de las tiendas que no son de Madrid*/
SELECT v.*,t.ciudad
FROM TIENDA t INNER JOIN VEHICULOS v ON t.idtienda=v.tienda
WHERE NOT t.ciudad='Madrid';