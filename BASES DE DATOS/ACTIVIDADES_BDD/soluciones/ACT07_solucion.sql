---------------------------------------------------------------------------------------------------------------
------------------------------------------------ ACT07 --------------------------------------------------------
---------------------------------------------------------------------------------------------------------------
/*Se quiere dise�ar una Base de Datos Objeto Relacional para controlar el acceso a las pistas deportivas 
de nuestra ciudad. Se tendr�n en cuenta los siguientes supuestos:
�	Todo aquel que quiera hacer uso de las instalaciones tendr� que registrarse y proporcionar su nombre, 
apellidos, email, tel�fono, y fecha de nacimiento. A cada usuario se le asignar� un c�digo.
�	Hay varios polideportivos en la ciudad, identificados por nombre, direcci�n, extensi�n (en m2) 
y un determinado c�digo.
�	En cada polideportivo hay varias pistas de diferentes deportes. De cada pista guardaremos un c�digo 
que la identifica, el tipo de pista (tenis, f�tbol, p�del, etc.), si est� operativa o
en mantenimiento y el precio.
�	Cada vez que un usuario registrado quiera utilizar una pista tendr� que realizar una reserva previa 
a trav�s de la web que el ayuntamiento ha creado. De cada reserva queremos registrar la fecha en la 
que se reserva la pista, la fecha en la que se usar�. 
Hay que tener en cuenta que todos los jugadores que vayan a hacer uso de las pistas deber�n estar registrados 
en el sistema y ser�n vinculados con la reserva

Implementaremos una posible soluci�n manteniendo la integridad referencial.
1.	Creaci�n de todos los tipos y tablas asociadas.*/

--------------------------------- CREACI�N DE LOS TIPOS ---------------------------------
--Creamos el tipo usuario
CREATE OR REPLACE TYPE tipousuario AS OBJECT(
    codigousuario number,
    nombre varchar2(50),
    apellidos varchar2(100),
    email varchar2(100),
    telefono number(9),
    fechanac date
);
--Creamos el tipo polideportivo
CREATE OR REPLACE TYPE tipopolideportivo AS OBJECT(
    codigopoli number,
    nombre varchar2(100),
    direccion varchar2(200),
    extension number
);
--Creamos el tipo pista
CREATE OR REPLACE TYPE tipopista AS OBJECT(
    codigopista number,
    tipo varchar2(100),
    operativa varchar2(1),
    precio number(4,2),
    polideportivo REF tipopolideportivo
);
--Creamos el tipo reserva
CREATE OR REPLACE TYPE tiporeserva AS OBJECT(
    idreserva number,
    fechareserva date,
    fechausopista date,
    pista REF tipopista,
    usuario REF tipousuario
);

-- CREACI�N DE LAS TABLAS ASOCIADAS A LOS TIPOS
--Creamos la tabla asociada al tipo usuario indicando la clave primaria
CREATE TABLE tablausuarios OF tipousuario(
    codigousuario primary key
);
--Creamos la tabla asociada al tipo polideportivo indicando la clave primaria
CREATE TABLE tablapolideportivos OF tipopolideportivo(
    codigopoli primary key
);
--Creamos la tabla asociada al tipo pista indicando la clave primaria y la referencia al polideportivo
CREATE TABLE tablapistas OF tipopista(
    codigopista primary key,
    polideportivo REFERENCES tablapolideportivos
);
--Creamos la tabla asociada al tipo reserva indicando la clave primaria y las referencias a la pista y al usuario
CREATE TABLE tablareservas OF tiporeserva(
    idreserva primary key,
    pista REFERENCES tablapistas,
    usuario REFERENCES tablausuarios
);

/*2.	Inserci�n de datos en todas las tablas.*/
--------------------------------- INSERCI�N EN LAS TABLAS ---------------------------------
--Inserci�n en la tabla usuarios
INSERT INTO tablausuarios VALUES(1,'Antonio','M�rida Madrid','amerida@gmail.com',921896712,'01/01/1972');
INSERT INTO tablausuarios VALUES(2,'Mar�a','C�ceres S�nchez','acaceres@gmail.com',918279172,'02/01/1995');
INSERT INTO tablausuarios VALUES(3,'Roberto','Collado Villalba','acollado@gmail.com',678654321,'12/04/2000');
--Inserci�n en la tabla polideportivos
INSERT INTO tablapolideportivos VALUES (1,'Municipal','Plaza San Andr�s, s/n',1500);
INSERT INTO tablapolideportivos VALUES (2,'Colegio Santa Mar�a','C/Santa Mar�a, 1',1000);
--Inserci�n en la tabla pistas
INSERT INTO tablapistas VALUES(1,'padel','y',12.75,(SELECT REF(t) FROM tablapolideportivos t WHERE t.codigopoli=1));
INSERT INTO tablapistas VALUES(2,'atletismo','n',4.50,(SELECT REF(t) FROM tablapolideportivos t WHERE t.codigopoli=1));
INSERT INTO tablapistas VALUES(3,'basket','y',7.25,(SELECT REF(t) FROM tablapolideportivos t WHERE t.codigopoli=1));
INSERT INTO tablapistas VALUES(4,'tenis','y',10.75,(SELECT REF(t) FROM tablapolideportivos t WHERE t.codigopoli=2));
INSERT INTO tablapistas VALUES(5,'futbol sala','n',10.00,(SELECT REF(t) FROM tablapolideportivos t WHERE t.codigopoli=2));
--Inserci�n en la tabla reservas
INSERT INTO tablareservas VALUES(1,'23/12/2020','28/12/2020',
                                (SELECT REF(t) FROM tablapistas t WHERE t.codigopista=1),
                                (SELECT REF(t) FROM tablausuarios t WHERE t.codigousuario=1)
);
INSERT INTO tablareservas VALUES(2,'23/12/2020','30/12/2020',
                                (SELECT REF(t) FROM tablapistas t WHERE t.codigopista=4),
                                (SELECT REF(t) FROM tablausuarios t WHERE t.codigousuario=2)
);
INSERT INTO tablareservas VALUES(3,'20/12/2020','23/12/2020',
                                (SELECT REF(t) FROM tablapistas t WHERE t.codigopista=1),
                                (SELECT REF(t) FROM tablausuarios t WHERE t.codigousuario=2)
);

--3.	Consulta de todas las reservas.
SELECT t.idreserva,t.fechareserva as "fecha reserva",t.fechausopista as "fecha de uso",
        DEREF(pista).tipo as pista,
        DEREF(usuario).nombre || ' ' || DEREF(usuario).apellidos as "usuario"
FROM tablareservas t;
--4.	Consulta de las reservas de un determinado usuario.
SELECT t.idreserva,t.fechareserva as "fecha reserva",t.fechausopista as "fecha de uso",
        DEREF(pista).tipo as pista
FROM tablareservas t
WHERE DEREF(usuario).codigousuario=2;
--5.	Consulta de las reservas de un determinado mes.
SELECT t.idreserva,t.fechareserva as "fecha reserva",t.fechausopista as "fecha de uso",
        DEREF(pista).tipo as pista,
        DEREF(usuario).nombre || ' ' || DEREF(usuario).apellidos as "usuario"
FROM tablareservas t
WHERE t.fechareserva>='01/12/2020' AND t.fechareserva<='31/12/2020';
--6.	Consulta de las pistas de un determinado polideportivo.
SELECT t.codigopista,t.tipo
FROM tablapistas t
WHERE DEREF(polideportivo).codigopoli=1;
--7.	Comprobaci�n de la integridad referencial.
--Intentamos borrar un polideportivo
DELETE FROM tablapolideportivos t WHERE t.codigopoli=1;

