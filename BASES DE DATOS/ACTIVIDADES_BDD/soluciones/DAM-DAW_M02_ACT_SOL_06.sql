-------------------------- SOLUCI�N ACTIVIDAD 6 -------------------------------------------
/*Trabajaremos con la misma base de datos del caso pr�ctico anterior. Una academia de idiomas.*/
SET SERVEROUTPUT ON;
--DROP TABLESPACE academia INCLUDING CONTENTS AND DATAFILES;
CREATE TABLESPACE academia
DATAFILE 'C:\archivos_oracle_marta\academia.dbf'
SIZE 400M
AUTOEXTEND ON;
CREATE USER world
IDENTIFIED BY "1234"
DEFAULT TABLESPACE academia
QUOTA UNLIMITED ON academia;
GRANT DBA TO world;
--DROP TABLE CURSOS;
CREATE TABLE CURSOS(
    cod_curso int NOT NULL PRIMARY KEY,
    nombre VARCHAR2(50),
    horario VARCHAR(20),
    fecha_inicio DATE,
    fecha_final DATE,
    precio float,
    Profesor VARCHAR2(50)
);
--DROP TABLE ALUMNOS;
CREATE TABLE ALUMNOS(
    numero_alumno INT PRIMARY KEY,
    nombre VARCHAR2(50),
    curso int,
    fecha_inscripcion DATE,
    CONSTRAINT fk_alumno_curso FOREIGN KEY (curso) REFERENCES CURSOS(cod_curso)
);
INSERT INTO CURSOS VALUES (1,'Ingl�s','09:00-10:00','01/01/2020','30/06/2020',1000,'David');
INSERT INTO CURSOS VALUES (2,'Alem�n','11:00-13:00','02/02/2020', '30/06/2020',4000, 'Angela');
INSERT INTO CURSOS VALUES (3,'Chino','15:00-17:00','03/03/2020', '30/08/2020',5000, 'Liu');
INSERT INTO CURSOS VALUES (4,'Franc�s','17:00-19:00','04/04/2020', '30/09/2020',4000, 'Monique');
INSERT INTO ALUMNOS VALUES (1,'Jos� Garc�a Mart�nez',1,'20/12/2019');
INSERT INTO ALUMNOS VALUES (2,'Julio Ca�izares Rojo',2,'21/03/2019');
INSERT INTO ALUMNOS VALUES (3,'Sara Palomares Tejedor',2,'01/01/2020');
INSERT INTO ALUMNOS VALUES (4,'Sandra G�mez Mar�n',1,'01/12/2019');
SELECT a.*,c.nombre
FROM CURSOS c INNER JOIN ALUMNOS a ON c.cod_curso=a.curso;

/*1. Realizar un procedimiento almacenado �entrar_alumno� que me permita introducir los datos de un
alumno por el teclado. Es necesario controlar como m�nimo que el c�digo del alumno no exista
previamente (clave duplicada).*/
--Creaci�n del procedimiento
CREATE OR REPLACE PROCEDURE entrar_alumno
(numintrod ALUMNOS.numero_alumno%TYPE,nombreintrod ALUMNOS.nombre%TYPE,
cursointrod CURSOS.nombre%TYPE,fechaintrod ALUMNOS.fecha_inscripcion%TYPE)
IS
    codcurso CURSOS.cod_curso%TYPE;
BEGIN
    SELECT cod_curso INTO codcurso FROM CURSOS WHERE nombre=cursointrod;
    INSERT INTO ALUMNOS VALUES(numintrod,nombreintrod,codcurso,fechaintrod);
    DBMS_OUTPUT.PUT_LINE('Alumno introducido correctamente');
EXCEPTION
    WHEN NO_DATA_FOUND THEN DBMS_OUTPUT.PUT_LINE('El curso introducido no existe');
    WHEN DUP_VAL_ON_INDEX THEN DBMS_OUTPUT.PUT_LINE('Ya existe un usuario con ese c�digo');
END;
--Llamada al procedimiento para comprobar
ACCEPT numintrod PROMPT 'C�digo:';
ACCEPT nombreintrod PROMPT 'Nombre:';
ACCEPT fechaintrod PROMPT 'Fecha de inscripci�n (dd/mm/aaaa):';
ACCEPT cursointrod PROMPT 'Nombre del curso:';
DECLARE
    numintrod ALUMNOS.numero_alumno%TYPE:=&numintrod;
    nombreintrod ALUMNOS.nombre%TYPE:='&nombreintrod';
    cursointrod CURSOS.nombre%TYPE:='&cursointrod';
    fechaintrod ALUMNOS.fecha_inscripcion%TYPE:='&fechaintrod';
BEGIN
    entrar_alumno(numintrod,nombreintrod,cursointrod,fechaintrod);
END;
--SELECT * FROM ALUMNOS;
SELECT curso,COUNT(curso) FROM ALUMNOS GROUP BY curso;
/*Implementar un disparador �mostrarcursos� para mostrar por consola, cu�ntos alumnos tenemos en
total de cada curso. El disparador se ejecutar� despu�s de que se inserte o modifique un alumno de la
tabla alumnos.*/
CREATE OR REPLACE TRIGGER mostrarcursos
AFTER INSERT OR UPDATE ON ALUMNOS
DECLARE
    CURSOR cursortotal IS SELECT curso,COUNT(curso) AS total_matriculados FROM ALUMNOS GROUP BY curso;
    nombrecurso CURSOS.nombre%TYPE;
BEGIN
    FOR fila IN cursortotal LOOP
        SELECT nombre INTO nombrecurso FROM CURSOS WHERE cod_curso=fila.curso;
        DBMS_OUTPUT.PUT_LINE('En ' || nombrecurso || ' hay ' || fila.total_matriculados || ' alumnos matriculados');
    END LOOP;
END;
--Probamos a introducir un alumno para comprobar que se ejecuta el trigger
ACCEPT numintrod PROMPT 'C�digo:';
ACCEPT nombreintrod PROMPT 'Nombre:';
ACCEPT fechaintrod PROMPT 'Fecha de inscripci�n (dd/mm/aaaa):';
ACCEPT cursointrod PROMPT 'Nombre del curso:';
DECLARE
    numintrod ALUMNOS.numero_alumno%TYPE:=&numintrod;
    nombreintrod ALUMNOS.nombre%TYPE:='&nombreintrod';
    cursointrod CURSOS.nombre%TYPE:='&cursointrod';
    fechaintrod ALUMNOS.fecha_inscripcion%TYPE:='&fechaintrod';
BEGIN
    entrar_alumno(numintrod,nombreintrod,cursointrod,fechaintrod);
END;
SELECT a.*,c.nombre
FROM CURSOS c INNER JOIN ALUMNOS a ON c.cod_curso=a.curso;