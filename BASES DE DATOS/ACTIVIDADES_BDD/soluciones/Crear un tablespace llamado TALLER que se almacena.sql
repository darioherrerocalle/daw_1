Crear un tablespace llamado TALLER que se almacena en el fichero c:\bd\taller.dbf de 200MB
CREATE TABLESPACE academia
DATAFILE 'C:\archivos_oracle_marta\academia.dbf'
SIZE 400M
AUTOEXTEND ON;

Crear un usuario llamado Juan con contraseña 1234, en el tablespace TALLER con espacio ilimitado.


Asignar a Juan privilegios para iniciar sesión y para hacer un select en una tabla llamada VEHICULOS.
