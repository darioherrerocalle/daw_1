/* 1. Crear un tablespace de 400MB llamado academia donde se almacenaran todos los datos. */
CREATE TABLESPACE academia
DATAFILE 'C:\Users\dario\Dropbox\DAW\BASES DE DATOS\ACTIVIDADES_BDD\act_5\academia.dbf'
SIZE 400M;

SELECT * FROM DBA_TABLESPACES ORDER BY TABLESPACE_NAME;

/* 2. Crea un usuario llamado �world� que tenga todos los privilegios en el sistema Oracle. Comprobar que realmente tiene asignados estos permisos. */
CREATE USER world
IDENTIFIED BY "1234"
DEFAULT TABLESPACE academia;
GRANT CREATE SESSION TO world;
GRANT DBA TO world;

SELECT * FROM DBA_USERS ORDER BY USERNAME;

/* 3. Utilizando el usuario de nombre �world� crea la tabla cursos y la tabla alumnos.
Utilizar el tipo de campo y la longitud que cre�is m�s adecuados para cada uno de los campos.
Introduce datos en las tablas. */
CREATE TABLE cursos (
    idCurso NUMBER(7) primary key,
    nombreCurso VARCHAR2(10) not null,
    horario VARCHAR2(7) not null,
    fechaInicio date,
    fechaFin date,
    profesor VARCHAR2(7) not null,
    precio NUMBER(7) not null
);
CREATE TABLE alumnos (
    idAlumno NUMBER(7) primary key,
    nombreAlumno VARCHAR2(7) not null,
    idCurso NUMBER(7) not null,
    fechaInscripcion date,
    foreign key (idCurso) references CURSOS(idCurso)
);

SELECT * FROM USER_TABLES;

INSERT INTO cursos VALUES (1, 'Ingl�s', 'Ma�ana', '01/02/2021', '01/06/2021', 'Dario', 400);
INSERT INTO cursos VALUES (2, 'Franc�s', 'Ma�ana', '01/02/2021', '01/06/2021', 'Maria', 450);
INSERT INTO cursos VALUES (3, 'Alem�n', 'Tarde', '01/02/2021', '01/06/2021', 'Pablo', 500);

INSERT INTO alumnos VALUES (1, 'Carlota', 1, '01/01/2021');
INSERT INTO alumnos VALUES (2, 'Mario', 2, '01/01/2021');
INSERT INTO alumnos VALUES (3, 'Dani', 3, '01/01/2021');

/* 4. Crear dos usuarios �secre1� y �secre2� con password �world1234�, que se encarguen de la gesti�n de la academia (a�adir, modificar, borrar, consultar) en la tabla de cursos y en la tabla de alumnos.
Comprueba que los privilegios se han asignado de forma correcta y que puede hacer las operaciones asignadas. */
CREATE USER secre1
IDENTIFIED BY "world1234"
DEFAULT TABLESPACE academia;
GRANT CREATE SESSION TO secre1;
GRANT ALTER, DELETE, INSERT, SELECT ON cursos TO secre1;
GRANT ALTER, DELETE, INSERT, SELECT ON alumnos TO secre1;

CREATE USER secre2
IDENTIFIED BY "world1234"
DEFAULT TABLESPACE academia;
GRANT CREATE SESSION TO secre2;
GRANT ALTER, DELETE, INSERT, SELECT ON cursos TO secre2;
GRANT ALTER, DELETE, INSERT, SELECT ON alumnos TO secre2;

SELECT * FROM DBA_TAB_PRIVS WHERE GRANTEE = 'SECRE1';
SELECT * FROM DBA_TAB_PRIVS WHERE GRANTEE = 'SECRE2';

/* 5. Se decide que el usuario �secre1� pueda crear nuevos usuarios, pero no podr� eliminar a ning�n usuario.  Comprobar que realmente tiene asignados estos permisos. 
Quitamos el permiso de borrar registros al usuario �secre2� sobre la tabla de cursos. Comprobaci�n. */
GRANT CREATE USER TO secre1;
SELECT * FROM DBA_SYS_PRIVS WHERE GRANTEE = 'SECRE1';

REVOKE DELETE ON cursos FROM secre2;
SELECT * FROM DBA_TAB_PRIVS WHERE GRANTEE = 'SECRE2';

/* 6. El usuario �world� concede al usuario �secre2�, la posibilidad de asignar el permiso de lectura (SELECT) de datos a otros usuarios sobre la tabla de alumnos.
Comprobar que realmente tiene asignados estos permisos. */
GRANT SELECT ON alumnos TO secre2
WITH GRANT OPTION;

SELECT * FROM DBA_TAB_PRIVS WHERE GRANTEE = 'SECRE2';

/* 7. Crea un rol llamado rolprofe con las siguientes caracter�sticas: 
Puede iniciar sesi�n, leer la tabla de cursos y leer y modificar la tabla de alumnos (no puede ni borrar ni a�adir). */
CREATE ROLE rolprofe
IDENTIFIED BY "1234";
GRANT CREATE SESSION TO rolprofe;
GRANT SELECT ON cursos TO rolprofe;
GRANT SELECT, ALTER ON alumnos TO rolprofe;
/* 8. Crea dos usuarios �profe1� y �profe2� con password �profe1234� y le asignas el rol anterior. Comprueba que tienen los permisos correspondientes. */
CREATE USER profe1
IDENTIFIED BY "profe1234";
GRANT rolprofe TO profe1;

CREATE USER profe2
IDENTIFIED BY "profe1234";
GRANT rolprofe TO profe2;

SELECT * FROM DBA_ROLE_PRIVS WHERE GRANTEE = 'PROFE1';

/* 9. Crearemos un perfil para los profesores llamado perfilprofe que tenga un 
tiempo m�ximo de conexi�n de 1 hora, 
dos conexiones simult�neas 
y le obligue a cambiar la contrase�a cada 30 d�as. 
Asigna este perfil al usuario profe1. */
DROP PROFILE perfilprofe CASCADE;
CREATE PROFILE perfilprofe LIMIT
CONNECT_TIME 60
SESSIONS_PER_USER 2
PASSWORD_LIFE_TIME 30;

ALTER USER profe1
PROFILE perfilprofe;

---------------------------------PR�CTICA 6-------------------------------------
/* 1. Realizar un procedimiento almacenado �entrar_alumno� que me permita introducir los datos de un alumno 
por el teclado. Es necesario controlar como m�nimo que el c�digo del alumno no exista previamente (clave duplicada). */
set serveroutput on;

CREATE OR REPLACE PROCEDURE entrar_alumno(
    dni alumnos.idAlumno%TYPE,
    alumno alumnos.nombreAlumno%TYPE,
    codigoCurso alumnos.idCurso%TYPE,
    fecha alumnos.fechaInscripcion%TYPE)
IS
    contadorDni NUMBER;
    alumnoexiste EXCEPTION;
BEGIN
    SELECT COUNT(idAlumno) INTO contadorDni
    FROM alumnos
    WHERE UPPER (dni) = UPPER (idAlumno);
    IF (contadorDni > 0) THEN
        RAISE alumnoexiste;
    ELSE
        INSERT INTO alumnos VALUES (dni, alumno, codigoCurso, fecha);
        dbms_output.put_line('Alumno registrado correctamente.');
    END IF;
EXCEPTION
    WHEN alumnoexiste THEN
        RAISE_APPLICATION_ERROR(-20010, 'Este alumno ya existe.');
END;

ACCEPT vdni PROMPT 'Introduce tu DNI: ';
ACCEPT valumno PROMPT 'Introcue tu nombre: ';
ACCEPT vcurso PROMPT 'Introdcuce el c�digo del curso seleccionado: ';
ACCEPT vfecha PROMPT 'Introcuce la fecha de inicio; ';
DECLARE
    dni alumnos.idAlumno%TYPE:=&vdni;
    alumno alumnos.nombreAlumno%TYPE:='&valumno';
    codigoCurso alumnos.idCurso%TYPE:=&vcurso;
    fecha alumnos.fechaInscripcion%TYPE:='&vfecha';
BEGIN
entrar_alumno(dni, alumno, codigoCurso, fecha);
END;

select * from alumnos;

/* 2. Implementar un disparador �mostrarcursos� para mostrar por consola, cu�ntos alumnos tenemos en total de cada curso. 
El disparador se ejecutar� despu�s de que se inserte o modifique un alumno de la tabla alumnos. */
CREATE OR REPLACE TRIGGER mostrtarCursos
AFTER INSERT OR UPDATE ON alumnos

DECLARE
    CURSOR totalAlumnos IS 
        SELECT cursos.nombrecurso as Curso, COUNT(*) as Total
        FROM cursos INNER JOIN alumnos ON cursos.idcurso = alumnos.idcurso
        GROUP BY nombreCurso;
BEGIN
    FOR fila IN totalAlumnos LOOP
        DBMS_OUTPUT.PUT_LINE('En el curso de ' || fila.Curso || ' hay ' || fila.Total || ' alumnos');
    END LOOP;
END;