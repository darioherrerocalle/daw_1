/*ALOJAMIENTO(cod_alojamiento, nombre, direccion_alojamiento, telefono, categoria)
PERSONAL(cod_personal, cod_alojamiento, nif, nombre, direccion_personal)
HABITACION(num_habitacion, cod_habitacion, baño, precio, tipo)
ACTIVIDADES(codigo_actividad, tipo, descripcion, nivel)
CELEBRACION(dia, cod_alojamiento, codigo_actividad)*/
create database ARRURAL2;
use ARRURAL2;
create table ALOJAMIENTO(
	cod_alojamiento tinyint primary key auto_increment,
    nombre char (25) not null,
    direccion_alojamiento char (100) not null,
    telefono smallint not null,
    categoria char (50) not null
);
create table PERSONAL(
	cod_personal tinyint primary key auto_increment,
    cod_alojamiento tinyint auto_increment,
    nif tinyint,
    nombre char (25) not null,
    direccion_personal char (100),
    constraint fk_cod_alojamiento foreign key (cod_alojamiento) references ALOJAMIENTO(cod_alojamiento)
);
create table HABITACION(
	num_hab tinyint primary key,
    cod_hab tinyint auto_increment,
    baño char (10) not null,
    precio tinyint not null,
    tipo tinyint not null,
    constraint fk_num_hab foreign key (cod_hab) references ALOJAMIENTO(cod_alojamiento)
);
create table ACTIVIDADES(
	cod_act tinyint primary key,
    nombre char(50),
    nivel char(50),
    descripcion char (100)
);
create table CELBRACION(
	dia date,
    cod_alojamiento tinyint auto_increment,
    cod_act tinyint,
    constraint pk_celebracion primary key (dia, cod_alojamiento, cod_act),
    constraint fk_codaloj foreign key (cod_alojamiento) references ALOJAMIENTO(cod_alojamiento),
    constraint fk_codact foreign key (cod_act) references ACTIVIDADES(cod_act)
);
CREATE UNIQUE INDEX Nombre_Alojamiento_Rural ON ALOJAMIENTO(nombre);
CREATE UNIQUE INDEX NIF ON PERSONAL(nif);