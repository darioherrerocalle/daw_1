SET SERVEROUTPUT ON;

--1.Creación objetos--
CREATE OR REPLACE TYPE tipociudad AS OBJECT(
    idciudad NUMBER,
    nombre VARCHAR2(20)
);

CREATE TYPE tipodireccion AS OBJECT(
    tipovia varchar2(20),
    nombrevia varchar2(100),
    numero number,
    bloque number,
    piso number,
    puerta number,
    cp number
);

CREATE OR REPLACE TYPE tipopolideportivo AS OBJECT(
    idpolideportivo NUMBER,
    nombre VARCHAR2(20),
    direccion tipodireccion,
    extensioin NUMBER,
    ciudad REF tipociudad
);

CREATE OR REPLACE TYPE tipopista AS OBJECT(
    idpista NUMBER,
    tipo VARCHAR2(20),
    operativa VARCHAR2(2),
    precio NUMBER,
    polideportivo REF tipopolideportivo
);

CREATE OR REPLACE TYPE tipousuario AS OBJECT(
    dni NUMBER,
    nombre VARCHAR2(20),
    apellido1 VARCHAR2(20),
    apellido2 VARCHAR2(20),
    email VARCHAR2(20),
    fechanacimiento DATE,
    tlf NUMBER,
    ciudad REF tipociudad
);

CREATE OR REPLACE TYPE tiporeserva AS OBJECT(
    idreserva NUMBER,
    fechareserva DATE,
    fechaevento DATE,
    usuario REF tipousuario,
    pista REF tipopista
);

--2.Creación tablas--
CREATE TABLE tablaciudades OF tipociudad(
    idciudad PRIMARY KEY
);

CREATE TABLE tablapolideportivos OF tipopolideportivo(
    idpolideportivo PRIMARY KEY,
    ciudad REFERENCES tablaciudades not null
);

CREATE TABLE tablapistas OF tipopista(
    idpista PRIMARY KEY,
    polideportivo REFERENCES tablapolideportivos not null,
    constraint chkOperativa check (operativa='si' or operativa='no')
);
/*ALTER TABLE tablapistas
ADD CONSTRAINT chk_pista CHECK (UPPER (operativa)='si' OR UPPER (operativa)='no');*/

CREATE TABLE tablausuarios OF tipousuario(
    dni PRIMARY KEY,
    ciudad REFERENCES tablaciudades not null
);

CREATE TABLE tablareservas OF tiporeserva(
    idreserva PRIMARY KEY,
    usuario REFERENCES tablausuarios not null,
    pista REFERENCES tablapistas not null
);

--3.Inserción de datos en las tablas asociadas--
--3.1. Inserción de datos en tablaciuades--
INSERT INTO tablaciudades VALUES (1, 'Villa Borderline');
SELECT * FROM TABLACIUDADES;
--3.2. Inserción de datos en tablapolideportivos--
INSERT INTO tablapolideportivos VALUES(
    1,
    'polideportivo amador',
    tipodireccion('calle', 'gañan', 1, null, null, null, 28341),
    1000,
    (SELECT REF(tc) FROM tablaciudades tc WHERE tc.nombre='Villa Borderline')
);
INSERT INTO tablapolideportivos VALUES(
    2,
    'polidepor lachusa',
    tipodireccion('calle', 'aquetepinsho', 1, null, null, null, 28341),
    2000,
    (SELECT REF(tc) FROM tablaciudades tc WHERE tc.nombre='Villa Borderline')
);
INSERT INTO tablapolideportivos VALUES(
    3,
    'polideportivo coque',
    tipodireccion('calle', 'huelealimong', 10, null, null, null, 28341),
    3000,
    (SELECT REF(tc) FROM tablaciudades tc WHERE tc.nombre='Villa Borderline')
);
SELECT * FROM tablapolideportivos;
--3.3. Inserción de datos en tablapistas--
INSERT INTO tablapistas VALUES(
    1,
    'tenis',
    'si',
    10,
    (SELECT REF(tpd) FROM tablapolideportivos tpd WHERE tpd.idpolideportivo=1)
);
INSERT INTO tablapistas VALUES(
    2,
    'padel',
    'si',
    10,
    (SELECT REF(tpd) FROM tablapolideportivos tpd WHERE tpd.idpolideportivo=1)
);
INSERT INTO tablapistas VALUES(
    3,
    'fronton',
    'si',
    10,
    (SELECT REF(tpd) FROM tablapolideportivos tpd WHERE tpd.idpolideportivo=1)
);

INSERT INTO tablapistas VALUES(
    4,
    'futbol',
    'si',
    8,
    (SELECT REF(tpd) FROM tablapolideportivos tpd WHERE tpd.idpolideportivo=2)
);
INSERT INTO tablapistas VALUES(
    5,
    'baloncesto',
    'si',
    8,
    (SELECT REF(tpd) FROM tablapolideportivos tpd WHERE tpd.idpolideportivo=2)
);

INSERT INTO tablapistas VALUES(
    6,
    'atletismo',
    'no',
    5,
    (SELECT REF(tpd) FROM tablapolideportivos tpd WHERE tpd.idpolideportivo=3)
);
INSERT INTO tablapistas VALUES(
    7,
    'padel',
    'no',
    7,
    (SELECT REF(tpd) FROM tablapolideportivos tpd WHERE tpd.idpolideportivo=3)
);

SELECT * FROM tablapistas;
--3.4. Inserción de datos en tablausuarios--
INSERT INTO tablausuarios VALUES(
    00000001,
    'Mentefria',
    'Garcia',
    'Fernandez',
    'mentefria@bbdd.es',
    '01/01/1980',
    918950001,
    (SELECT REF(tc) FROM tablaciudades tc WHERE tc.nombre='Villa Borderline')
);
INSERT INTO tablausuarios VALUES(
    00000002,
    'Javi',
    'Sanchez',
    'Lopez',
    'javi@bbdd.es',
    '02/01/1980',
    918950002,
    (SELECT REF(tc) FROM tablaciudades tc WHERE tc.nombre='Villa Borderline')
);
INSERT INTO tablausuarios VALUES(
    00000003,
    'Marcos',
    'Gamo',
    'Sanz',
    'marcos@bbdd.es',
    '03/01/1980',
    918950003,
    (SELECT REF(tc) FROM tablaciudades tc WHERE tc.nombre='Villa Borderline')
);
INSERT INTO tablausuarios VALUES(
    00000004,
    'Dario',
    'Herero',
    'Calle',
    'dario@bbdd.es',
    '04/01/1980',
    918950004,
    (SELECT REF(tc) FROM tablaciudades tc WHERE tc.nombre='Villa Borderline')
);
INSERT INTO tablausuarios VALUES(
    00000005,
    'Maria',
    'Garcia',
    'Bargueño',
    'maria@bbdd.es',
    '05/01/1980',
    918950005,
    (SELECT REF(tc) FROM tablaciudades tc WHERE tc.nombre='Villa Borderline')
);
INSERT INTO tablausuarios VALUES(
    0000006,
    'pablo',
    'fernandez',
    'garcia',
    'pablo@bbdd.es',
    '05/01/1980',
    918950005,
    (SELECT REF(tc) FROM tablaciudades tc WHERE tc.nombre='Villa Borderline')
);
INSERT INTO tablausuarios VALUES(
    00000007,
    'alfonso',
    'decimo',
    'elsabio',
    'alfonso@bbdd.es',
    '23/11/1221',
    918950007,
    (SELECT REF(tc) FROM tablaciudades tc WHERE tc.nombre='Villa Borderline')
);
INSERT INTO tablausuarios VALUES(
    00000008,
    'rodrigo',
    'diaz',
    'de vivar',
    'elcid@bbdd.es',
    '10/07/1099',
    918950008,
    (SELECT REF(tc) FROM tablaciudades tc WHERE tc.nombre='Villa Borderline')
);
INSERT INTO tablausuarios VALUES(
    00000009,
    'isabel',
    'la',
    'catolica',
    'isa@bbdd.es',
    '22/04/1451',
    918950009,
    (SELECT REF(tc) FROM tablaciudades tc WHERE tc.nombre='Villa Borderline')
);
INSERT INTO tablausuarios VALUES(
    00000010,
    'fernando',
    'segundo',
    'de aragon',
    'fer@bbdd.es',
    '10/03/1452',
    918950010,
    (SELECT REF(tc) FROM tablaciudades tc WHERE tc.nombre='Villa Borderline')
);

SELECT * FROM tablausuarios;
--3.5. Inserción de datos en tablareservas--

DROP TABLE tablapistas CASCADE CONSTRAINTS;