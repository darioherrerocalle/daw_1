/* TIENDA (id_tienda, nombre, ciudad, num_trabajadores, superficie); 
VEHICIULOS (matricula, marca, modelo, color, cilindrada, antiguedad, km, precio, id_tienda);*/


CREATE DATABASE CONCESIONARIO;
USE CONCESIONARIO;
CREATE TABLE TIENDA (
	id_tienda tinyint primary key,
    nombre char (50) not null,
    ciudad char (50) not null,
    num_trabajadores int,
    superficie int
);
CREATE TABLE VEHICULO (
	matricula varchar (20) primary key,
    marca char (25) not null,
    modelo char (25) not null,
    color char (25) not null,
    cilindrada int not null,
    antiguedad int not null,
    km int not null,
    precio int not null,
    id_tienda tinyint,
    constraint fk_id_tienda foreign key (id_tienda) references TIENDA(id_tienda)
);

INSERT INTO TIENDA VALUE (1, 'Auto_2', 'Madrid', 5, 1250);
INSERT INTO TIENDA VALUE (2, 'MultiMarca Total', 'Madrid', 8, 1750);
INSERT INTO TIENDA VALUE (3, 'CarAuto', 'Barcelona', 10, 2000);
INSERT INTO TIENDA VALUE (4, 'Turismos Díaz', 'Barcelona', 5, 1000);
INSERT INTO TIENDA VALUE (5, 'BarnaCar', 'Barcelona', 15, 3000);

INSERT INTO VEHICULO VALUE ('1213CRX', 'Seat', 'Leon', 'Negro', 1800, 2004, 180000, 2000, 1);
INSERT INTO VEHICULO VALUE ('3243HTN', 'Seat', 'Altea', 'Rojo', 1200, 2013, 85000, 7900, 2);
INSERT INTO VEHICULO VALUE ('6643KBM', 'Seat', 'Ibiza', 'Blanco', 1000, 2017, 25000, 10900, 2);
INSERT INTO VEHICULO VALUE ('8265HZL', 'Nissan', 'Juke', 'Blanco', 1600, 2014, 110000, 13900, 3);
INSERT INTO VEHICULO VALUE ('8919HHH', 'Nissan', 'Qashqai', 'Gris', 2000, 2011, 200000, 8500, 4);
INSERT INTO VEHICULO VALUE ('7623GRS', 'Volkswagen', 'Tiguan', 'Gris', 2000, 2010, 130000, 12000, 1);
INSERT INTO VEHICULO VALUE ('4901KPS', 'Volkswagen', 'Polo', 'Azul', 1000, 2018, 10000, 11500, 2);
INSERT INTO VEHICULO VALUE ('6841LBN', 'Volkswagen', 'Golf', 'Rojo', 1600, 2019, 15000, 22500, null);

/* 1.	Consulta que muestre todas las tiendas de más de 1500 metros, ordenadas por el nombre de la tienda. */
SELECT nombre FROM TIENDA WHERE superficie > 1500 ORDER BY nombre ASC;

/* 2.	Consulta que muestre la marca y el modelo de los vehículos que sean blancos o su antigüedad sea inferior a 2012. */
SELECT marca, modelo FROM VEHICULO WHERE antiguedad < 2012 OR color = 'blanco';

/* 3.	Consulta que muestre el nombre de la tienda, la marca, el modelo y el precio del vehículo. */
SELECT tienda.nombre, vehiculo.marca, vehiculo.modelo, vehiculo.precio 
FROM tienda INNER JOIN vehiculo on tienda.id_tienda = vehiculo.id_tienda;

/* 4.	¿Cuántos vehículos tenemos de cada marca? */
SELECT marca, COUNT(marca) FROM vehiculo GROUP BY marca;

/* 5.	¿Cuál es el importe total de los vehículos de cada tienda? */
SELECT tienda.nombre as TIENDA, SUM(vehiculo.precio) as PRECIO 
FROM tienda INNER JOIN vehiculo on tienda.id_tienda = vehiculo.id_tienda GROUP BY tienda;

/* 6.	Mostrar la marca, modelo, km y la tienda de cada vehículo. Si un vehículo no está en ninguna tienda también debe salir. */
SELECT tienda.nombre as Tienda, vehiculo.marca as Marca, vehiculo.modelo as Modelo, vehiculo.km as Km 
FROM tienda RIGHT JOIN vehiculo on tienda.id_tienda = vehiculo.id_tienda;

/* 7.	Mostrar la media de km de los vehículos de la marca Seat. */
SELECT marca,  AVG(km) as Media FROM VEHICULO WHERE marca = 'seat';

/* 8.	¿Cuál es la media de km y de precio de los vehículos con una antigüedad inferior a 2015? */
SELECT marca, AVG(km) as 'Media Km', AVG(precio) as 'Precio medio' FROM VEHICULO WHERE antiguedad < 2015;

/* 9.	Mostrar la tienda y la suma de km de sus vehículos, solo de aquellas tiendas que la suma de km de sus vehículos es superior a 150000. */
SELECT tienda.nombre as Tienda, SUM(vehiculo.km) as Km 
FROM tienda INNER JOIN vehiculo on tienda.id_tienda = vehiculo.id_tienda 
GROUP BY tienda HAVING SUM(km)>150000;

/* 10.	Mostrar la marca y el modelo de los vehículos que no están en ninguna tienda. */
SELECT marca, modelo FROM VEHICULO WHERE id_tienda is null;

/* 11.	Mostrar la marca, el modelo, el precio y una nueva columna con un 10% sobre el precio a la que llamaremos descuento de los vehículos con más de 100000 km y un precio menor 10000€. */
SELECT marca, modelo, precio,  precio*0.9 as "Descuento" FROM VEHICULO WHERE km > 100000 AND precio < 10000;

/* 12.	Marca y modelo del vehículo de mayor antigüedad. */
SELECT marca, modelo, MIN(antiguedad) FROM VEHICULO;

/* 13.	Marca y modelo de los vehículos que tienen un importe superior al vehículo de la marca Seat más caro. */
SELECT marca, modelo FROM VEHICULO WHERE precio >(SELECT MAX(precio) FROM VEHICULO where marca = "seat");

/* 14.	Qué antigüedad tiene el vehículo con  más km que no es de color blanco ni de la marca Wolkswagen. */
SELECT marca, modelo, antiguedad FROM VEHICULO 
WHERE km = (SELECT MAX(km) FROM VEHICULO WHERE color != 'Blanco' AND marca != 'Volkswagen');

/* 15.	Mostrar los vehículos de las tiendas que no son de Madrid. */
SELECT vehiculo.marca AS Marca, vehiculo.modelo as Modelo, tienda.nombre
FROM vehiculo INNER JOIN tienda on tienda.id_tienda = vehiculo.id_tienda
HAVING nombre = ANY 
(SELECT nombre as Tienda FROM TIENDA WHERE ciudad != 'Madrid');

SELECT vehiculo.marca AS Marca, vehiculo.modelo as Modelo, tienda.ciudad
FROM vehiculo INNER JOIN tienda on tienda.id_tienda = vehiculo.id_tienda
where ciudad != ANY 
(SELECT ciudad as Tienda FROM TIENDA WHERE ciudad = 'Madrid');

/* 1. En nuestra base de datos concesionario queremos almacenar la información referente a las empresas a la que pertenecen cada tienda.
Crea la siguiente tabla teniendo en cuenta que el nombre del campo debe coincidir con el título de la columna y el tipo de datos 
estará en función del contenido. */
CREATE TABLE GRUPO (
	idgrupo tinyint primary key,
    empresa char (25) not null,
    ciudad char (25) not null,
    presidente char (25) not null,
    fundacion int
);

/* 2. Añade a la tabla tienda un campo llamado idgrupo y crea una relación entre el grupo y las tiendas. Un grupo tiene muchas tiendas, 
y una tienda solo pertenece a un grupo. */
ALTER TABLE TIENDA 
	ADD COLUMN idgrupo tinyint;
ALTER TABLE TIENDA 
	ADD foreign key (idgrupo) references GRUPO(idgrupo);

/* 3. Añade la información a la tabla grupo. */
INSERT INTO GRUPO 
	VALUE(1, "Madrid Central", "Madrid", "Sebastián Amaya", 2000);
INSERT INTO GRUPO 
	VALUE(2, "Cars Holding", "Barcelona", "Carmen Álvarez", 2003);

/* 4. Asigna a cada tienda su grupo: Auto2 y CarAuto pertenecen al grupo Madrid Central y las restantes tiendas a Cars Holding. */

UPDATE TIENDA 
	SET idgrupo = 1
		WHERE nombre = 'Auto_2' or nombre = 'CarAuto';

UPDATE TIENDA
	SET idgrupo = 2
		WHERE nombre != 'Auto_2' and nombre != 'CarAuto';

/* 5. Muestra el nombre del grupo con el total de trabajadores y el total de superficie de exposición. */
SELECT grupo.empresa as GRUPO, 
		SUM(tienda.num_trabajadores), 
        SUM(tienda.superficie)
FROM GRUPO INNER JOIN TIENDA ON tienda.idgrupo = grupo.idgrupo 
GROUP BY grupo.empresa;

/* 6. Muestra para cada vehículo con cilindrada superior a 1.4, su marca, modelo, precio, km, la tienda y el grupo al que pertenece. */
SELECT vehiculo.marca as Marca, vehiculo.modelo as Modelo, vehiculo.precio as Precio, vehiculo.km as Km, vehiculo.id_tienda as Tienda, tienda.idgrupo as Grupo, grupo.empresa
FROM GRUPO
INNER JOIN TIENDA ON grupo.idgrupo = tienda.idgrupO
RIGHT JOIN VEHICULO ON tienda.id_tienda = vehiculo.id_tienda
WHERE cilindrada > 1400; 

/* 7. Turismos Díaz amplía su superficie a 2500 metros y ahora dispone de 12 trabajadores. Realiza este cambio en la tabla. */
UPDATE TIENDA
	SET superficie = 2500, num_trabajadores = 12
		WHERE nombre = 'Turismos Díaz';

/* 8. Añade a la tabla vehiculos un campo llamado financiado después del campo precio. Rellena este campo con el precio del vehículo más un 10%. */
ALTER TABLE VEHICULO 
	ADD COLUMN financiado double AFTER precio;
UPDATE VEHICULO
	SET financiado = precio*1.1;

/* 9. Añade a la tabla vehículos un campo llamado descuento. Rellena este campo con el 10% de su precio pero solo aquellos vehículos 
con una antigüedad inferior a 2015 y un precio inferior a 10000. */
ALTER TABLE VEHICULO
	ADD COLUMN descuento double AFTER financiado;
UPDATE VEHICULO
	SET descuento = precio*0.1
		WHERE antiguedad < 2015
        AND precio < 10000;

/* 10. Crear una base de datos llamada “copiaseguridad” 
y copiar todos los datos actuales. Deben de utilizarse instrucciones SQL. */
DROP DATABASE IF EXISTS copiaseguridad;
CREATE DATABASE COPIASEGURIDAD;
USE COPIASEGURIDAD;

CREATE TABLE copiaseguridad.csgrupo
AS
SELECT * FROM concesionario.grupo;

CREATE TABLE copiaseguridad.cstienda
AS
SELECT * FROM concesionario.tienda;

CREATE TABLE copiaseguridad.csvehiculo
AS
SELECT * FROM concesionario.vehiculo;

/* 11. Crea  una vista con todos los vehículos del grupo Madrid Central. La vista mostrará el nombre de la tienda y todos los datos del vehículo. 
La vista se llamará VistaMadridCentral */
CREATE VIEW VistaMadridCentral
AS
SELECT grupo.empresa as Grupo, tienda.nombre as Tienda, vehiculo.*
FROM GRUPO
INNER JOIN TIENDA ON grupo.idgrupo = tienda.idgrupo
RIGHT JOIN VEHICULO ON tienda.id_tienda = vehiculo.id_tienda
WHERE empresa = 'Madrid Central'; 
SELECT * FROM VistaMadridCentral;

/* 12. Crea una vista actualizable con todos los vehículos de la marca Seat. Comprueba que puedes añadir un registro a esta vista. 
La vista se llamará VistaSeat. Solo podemos añadir vehículos de la marca Seat. */
CREATE VIEW VistaSeat 
AS 
SELECT * FROM VEHICULO 
WHERE marca = 'Seat'
WITH CHECK OPTION;
SELECT * FROM VistaSeat;
INSERT INTO vistaseat
VALUES ('1234AAA', 'Seat', 'Leon', 'Negro', 2000, 2000, 20000, 2000, null, null, 1);
SELECT * FROM VistaSeat;

/* 13. Empieza una transacción con la instrucción BEGIN. Incrementa 500 € el precio de todos los vehículos de la marca Seat. 
Ejecuta un ROLLBACK. ¿Qué ha sucedido? */
BEGIN;
UPDATE VEHICULO
	SET precio = precio + 500
		WHERE marca = 'seat';
ROLLBACK;
SELECT * FROM VEHICULO;
/* Al ejecutar el rollback, tras ejecutar todas las instrucciones de suma, las deshace y es como si  no se hubiera ejecutado nada */

/* 14. Empieza una transacción con la instrucción BEGIN. Incrementa 500€ el precio de todos los vehículos de la marca Nissan. 
Ejecuta un COMMIT. ¿Qué ha sucedido? */
BEGIN;
UPDATE VEHICULO
	SET precio = precio + 500
		WHERE marca = 'nissan';
COMMIT;
SELECT * FROM VEHICULO;

/* 15. Empieza una transacción con la instrucción BEGIN. Incrementa 500 € el precio de todos los vehículos de la marca Seat. 
Define un punto de control llamado PASO1. Borra todos los vehículos de la marca Nissan. Haz un ROLLBACK al PASO1, y luego realiza un COMMIT. 
Comprueba y comenta que ha sucedido. */
BEGIN;
UPDATE VEHICULO
	SET precio = precio + 500
		WHERE marca = 'seat';
SAVEPOINT PASO1;
DELETE FROM VEHICULO
	WHERE marca = 'nissan';
ROLLBACK TO PASO1;
COMMIT;
SELECT * FROM VEHICULO;
/* La transacción ejecuta la suma y guarda la información en el savepoint, después borra los vehículos nissan,
después vuelve al savepoint donde siguen los datos que acababa de borrar, y finalmente nos devuelve el savepoint. */

/* 16. Bloquear la tabla vehiculos por escritura e intentar modificar alguna fila de datos. 
Comprobara y explicar, con comentarios en el código, qué sucede. */
BEGIN;
LOCK TABLE VEHICULO WRITE;
UPDATE VEHICULO
	SET precio = precio - 500
		WHERE modelo = 'golf';
UNLOCK TABLES;
COMMIT;
SELECT * FROM VEHICULO;
/* Al bloquear la tabla por escritura, quien ha realizado el bloqueo sí puede leer o escribir
pero nadie más puede acceder a esa tabla. */
