/* Crear un tablespace nuevo llamado correduria
con 10M que se puedan extender en caso de necesitar m�s*/
--DROP TABLESPACE correduria INCLUDING CONTENTS AND DATAFILES;
CREATE TABLESPACE correduria
DATAFILE 'C:\archivos_oracle_marta\correduria.dbf'
SIZE 10M AUTOEXTEND ON;

/*Consultar la vista de los tablespaces del sistema para comprobar
que se ha creado bien*/
SELECT * FROM DBA_TABLESPACES ORDER BY TABLESPACE_NAME;

/*Crear un usuario llamado administrador que use
el tablespace correduria con cuota ilimitada*/
--DROP USER administrador CASCADE;
CREATE USER administrador
IDENTIFIED BY "1234"
DEFAULT TABLESPACE correduria
QUOTA UNLIMITED ON correduria;
GRANT CREATE SESSION TO administrador; --Para que se pueda conectar

/*Crear un usario llamado gestor que use el tablespace
correduria con una cuota ilimitada*/
--DROP USER gestor CASCADE;
CREATE USER gestor
IDENTIFIED BY "1234"
DEFAULT TABLESPACE correduria
QUOTA UNLIMITED ON correduria;
GRANT CREATE SESSION TO gestor; --Para que se pueda conectar

/*Crear un usario llamado administrativo que use el tablespace
correduria con una cuota de 5M*/
--DROP USER administrativo CASCADE;
CREATE USER administrativo
IDENTIFIED BY "1234"
DEFAULT TABLESPACE correduria
QUOTA 5M ON correduria;
GRANT CREATE SESSION TO administrativo; --Para que se pueda conectar

/*Consultar la vista de los usuarios para comprobar que se han creado*/
SELECT * FROM DBA_USERS ORDER BY USERNAME;

/*Asignar el rol de DBA al administrador
(ahora tiene permisos para hacer de todo)*/
GRANT DBA TO administrador;

/*Consultar la vista de los roles del usuario administrador para ver qu� roles 
tiene asignados*/
SELECT grantee, granted_role
FROM DBA_ROLE_PRIVS
WHERE grantee = 'administrador';

/*Nos conectamos como el usuario administrador
y creamos una tabla llamada seguros con el idseguro,idcliente,matricula,
idcobertura,la fecha de creaci�n y la fecha de caducidad*/
CREATE TABLE seguros(
    idseguro NUMBER(7) primary key,
    idcliente NUMBER(7) not null,
    matricula VARCHAR2(7) not null,
    idcobertura NUMBER(3) not null,
    fecha date,
    caducidad date
);

/*Consultar las tablas del usuario actual para comprobar que se ha creado correctamente*/
SELECT * FROM USER_TABLES;

/*Consultar qui�n es el propietario y a qu� tablespace pertenece la tabla SEGUROS*/
SELECT owner, tablespace_name
from ALL_TABLES
WHERE table_name LIKE 'seguros';

/*�Qu� puede el usuario administrador hacer sobre esta tabla?*/
INSERT INTO seguros VALUES (1,1,'9856KPL',3,'01/02/2020','01/02/2021');
INSERT INTO seguros VALUES (2,1,'3210LBM',2,'10/12/2015','10/12/2020');
INSERT INTO seguros VALUES (3,2,'0001BWZ',1,'19/01/2010','19/01/2021');--S� que puede insertar
SELECT * FROM seguros; --S� que puede seleccionar
UPDATE seguros
SET idseguro=idseguro+1; --S� que puede actualizar
ALTER TABLE seguros
RENAME COLUMN fecha TO fechacreacion; --S� que pude modificar la tabla
DROP TABLE seguros; --S� que puede borrar la tabla

/*Volvemos a crear la tabla seguros y realizamos la conexi�n con el usuario
administrativo. �Puede este usuario consultar la tabla?*/
CREATE TABLE seguros(
    idseguro NUMBER(7,0) primary key,
    idcliente NUMBER(7,0) not null,
    matricula VARCHAR2(7) not null,
    idcobertura NUMBER(3,0) not null,
    fecha date,
    caducidad date
);
--Conexi�n con administrativo
SELECT * FROM administrador.seguros; --No puede ni siquiera ver la tabla

/*Conectamos, de nuevo, al usuario administrador para asignarle al usuario
administrativo permiso de selecci�n i de inserci�n sobre la tabla seguros*/
GRANT SELECT,INSERT ON seguros TO administrativo;

/*Consultamos los permisos del usuario administrativo*/
SELECT * FROM DBA_TAB_PRIVS WHERE GRANTEE='administrativo';

/*Realizamos, de nuevo, la conexi�n como administrativo para comprobar
que ahora s� que puede hacer consultas e insertar informaci�n en la tabla seguros*/
INSERT INTO administrador.seguros VALUES (1,1,'9856KPL',3,'01/02/2020','01/02/2021');
INSERT INTO administrador.seguros VALUES (2,1,'3210LBM',2,'10/12/2015','10/12/2020');
INSERT INTO administrador.seguros VALUES (3,2,'0001BWZ',1,'19/01/2010','19/01/2021');
--S� que puede insertar en la tabla
SELECT * FROM administrador.seguros; --S� que puede seleccionar
DROP TABLE administrador.seguros; --No que puede borrar la tabla
SELECT * FROM USER_TABLES; --Si que puede hacer esta consulta porque son sus tablas
SELECT * FROM DBA_USERS; --Las vistas que empiezan por DBA no son accesibles para �l

/*Realizamos la conexi�n como administrador para crear un rol llamado rolgestor
con permiso de selecci�n y actualizaci�n sobre la tabla seguros*/
--Creamos el rol
CREATE ROLE rolgestor
IDENTIFIED BY "1234";
--Asignamos los permisos al rol
GRANT SELECT,UPDATE ON seguros TO rolgestor;

/*Asignamos el rolgestor al usario gestor*/
GRANT rolgestor TO gestor;
SELECT * FROM DBA_ROLE_PRIVS WHERE GRANTEE='gestor'; --Lo comprobamos consultando los roles asignados a gestor

/*Activamos el rol rolgestor*/
SET ROLE ROLgestor IDENTIFIED BY "1234";
/*Modificamos el usuario gestor para que rolgestor sea su rol por defecto*/
ALTER USER gestor
DEFAULT ROLE ROLgestor;

/*Realizamos la conexi�n como gestor y comprobamos sus roles asignados*/
SELECT * FROM USER_ROLE_PRIVS;

/*Asignamos, al usuario gestor, permiso de selecci�n sobre cualquier tabla
de forma que luego �l pueda asignar este permiso a otros usuarios*/
GRANT SELECT ANY TABLE TO gestor WITH ADMIN OPTION; 
--No podemos hacerlo porque estamos trabajando
--con el usuario gestor y �sto s�lo lo puede hacer el SYSTEM o el administrador
--Nos conectamos con el usuario adminsitrador y lo volvemos a probar
GRANT SELECT ANY TABLE TO gestor WITH ADMIN OPTION; --Ahora s� que nos lo permite
--Conectamos como gestor para comprobarlo
GRANT SELECT ANY TABLE TO administrativo; --S� que puede

--Conectamos al usuario adminsitrador

/*Creamos un perfil nuevo llamado perfilgestor cuya contrase�a caduque a los 60 d�as,
que s�lo pueda estar inactivo un m�ximo de 10 minutos y que s�lo tenga 3 intentos
de inicio de sesi�n*/
CREATE PROFILE perfilgestor LIMIT
PASSWORD_LIFE_TIME 60
FAILED_LOGIN_ATTEMPTS 3
IDLE_TIME 10;

/*Consultamos las limitaciones del perfil que acabamos de crear*/
SELECT *
FROM DBA_PROFILES
WHERE PROFILE='PERFILGESTOR' AND LIMIT<>'DEFAULT';

/*Asignamos el perfil al usuario administrativo*/
ALTER USER administrativo
PROFILE PERFILgestor;

/*Consultamos el perfil asignado al usuario actual*/
SELECT USERNAME,PROFILE FROM DBA_USERS
WHERE USERNAME='administrativo';

/*Intentamos hacer la conexi�n de administrativo, tres veces con la contrase�a equivocada*/
--Podemos observar el mensaje 'the account is locked'
--Conectamos al administrador y le desbloqueamos la cuenta
ALTER USER administrativo
ACCOUNT UNLOCK;