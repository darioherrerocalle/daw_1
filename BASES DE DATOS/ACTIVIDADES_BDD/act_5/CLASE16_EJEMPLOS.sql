DROP TABLESPACE empresa INCLUDING CONTENTS AND DATAFILES;
DROP USER ANDRES CASCADE;
DROP USER ALBERTO CASCADE;
/*Creaci�n de un tablespace llamado empresa de 150MB y que pueda crecer en el futuro*/
CREATE TABLESPACE empresa
DATAFILE 'C:\ARCHIVOS_ORACLE\empresa.dbf'
SIZE 150M
AUTOEXTEND ON;
/*Creaci�n del usuario Alberto, que usar� por defecto 
el tablespace EMPRESA con cuota de 10MB.*/
CREATE USER albeto IDENTIFIED BY "1234"
DEFAULT TABLESPACE empresa
QUOTA 10M ON empresa;
/*Creaci�n del usuario Andr�s que usar� el tablespace
empresa con cuota ilimitada*/
CREATE USER andres IDENTIFIED BY "1234"
DEFAULT TABLESPACE empresa
QUOTA UNLIMITED ON empresa;
/*Borrado del usuario albeto y creaci�n de Alberto*/
DROP USER albeto CASCADE;
CREATE USER alberto
IDENTIFIED BY "1234"
DEFAULT TABLESPACE empresa
QUOTA UNLIMITED ON empresa;
/*Cambiar la contrase�a de Andr�s a 12345*/
ALTER USER ANDRES
IDENTIFIED BY "12345";
/*Mostrar todos los usuarios del sistema*/
SELECT * FROM DBA_USERS;
/*Asignaci�n de permiso de conexi�n para el usuario Andr�s*/
GRANT CREATE SESSION TO andres;
/*Asignaci�n de permiso de consulta sobre cualquier tabla para Andr�s*/
GRANT SELECT ANY TABLE TO andres;
/*Creaci�n de una tabla llamada clientes en el tablespace empresa*/
DROP TABLE CLIENTES;
CREATE TABLE CLIENTES(
    idcliente number(4) primary key,
    nombre varchar(20),
    apellidos varchar(100),
    totalcompras long
);
/*Creaci�n de una tabla llamada proveedores en el tablespace empresa*/
DROP TABLE PROVEEDORES;
CREATE TABLE PROVEEDORES(
    idprov number(4) primary key,
    nombre varchar(20),
    apellidos varchar(100),
    totalventas long
);
/*Asignaci�n de permiso de inserci�n sobre la tabla clientes para Andr�s*/
GRANT INSERT ON SYSTEM.clientes TO andres;
/*Conexi�n como Andr�s*/
DISCONNECT; /*En modo gr�fico hay que desconectar manualmente*/
CONNECT ANDRES; /*En modo gr�fico hay que crear una nueva conexi�n y conectar manualmente*/
/*Comprobaci�n de que Andr�s puede consultar cualquier tabla*/
SELECT * FROM SYSTEM.PROVEEDORES;
SELECT * FROM SYSTEM.CLIENTES;
/*Comprobaci�n de que Andr�s puede insertar en la tabla clientes*/
INSERT INTO SYSTEM.CLIENTES VALUES (1,'Marisa','Garc�a Rodr�guez',1234567);
SELECT * FROM SYSTEM.CLIENTES;
/*Comprobaci�n de que Andr�s no puede insertar en la tabla proveedores*/
INSERT INTO SYSTEM.PROVEEDORES VALUES (1,'Roberto','M�rquez L�pez',98345);
SELECT * FROM SYSTEM.PROVEEDORES;
/*Le quitamos el permiso de inserci�n a Andr�s*/
REVOKE INSERT ON SYSTEM.CLIENTES FROM andres;
/*Como Andr�s no tiene permisos de revocaci�n, conectamos como SYSTEM y lo volvemos a probar*/
DISCONNECT;
CONNECT SYSTEM;
REVOKE INSERT ON SYSTEM.CLIENTES FROM andres;
DISCONNECT;
CONNECT ANDRES;
INSERT INTO SYSTEM.CLIENTES VALUES (1,'Marisa','Garc�a Rodr�guez',1234567);
/*Comprobamos que Andr�s no tiene permiso para crear m�s usuarios*/
CREATE USER empleado
IDENTIFIED BY "1234"
DEFAULT TABLESPACE empresa
QUOTA UNLIMITED ON empresa;
/*Cambiamos al usuario SYSTEM y le asignamos a Andr�s el rol DBA para que pueda crear m�s usuarios*/
DISCONNECT;
CONNECT SYSTEM;
GRANT DBA TO ANDRES;
/*Cambiamos al usuario Andr�s y comprobamos que puede crear usuarios y asignarles permisos*/
DISCONNECT;
CONNECT ANDRES;
CREATE USER empleado
IDENTIFIED BY "1234"
DEFAULT TABLESPACE empresa
QUOTA UNLIMITED ON empresa;
GRANT RESOURCE TO EMPLEADO;
/*Cambiamos al usuario empleado y comprobamos qu� puede hacer*/
GRANT CREATE SESSION TO EMPLEADO;
DISCONNECT;
CONNECT EMPLEADO;
SELECT * FROM SYSTEM.PROVEEDORES; /*No tiene acceso a las tablas de otros usuarios*/
GRANT DBA TO EMPLEADO; /*No puede asignar permisos*/
CREATE TABLE EMPLEADOS( /*S� que puede crear tablas en su tablespace*/
    idempleado number(4) primary key,
    nombre varchar(20),
    apellidos varchar(100),
    sueldo long
);
INSERT INTO EMPLEADOS VALUES (1,'Jose','Pozo Monta�a',45000); /*S� que puede gestionar sus tablas propias*/
SELECT * FROM EMPLEADOS;