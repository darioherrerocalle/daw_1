---------------------------------------EJEMPLO 1: MARCAS CON SU WEB-----------------------------
--Creaci�n del tipo TIPOWEB
CREATE OR REPLACE TYPE TIPOWEB AS OBJECT(
  idweb NUMBER,
  nombreurl VARCHAR(150),
  dominio VARCHAR(4)
);
--Creaci�n de la tabla de webs
CREATE TABLE TABLAWEBS OF TIPOWEB(
    idweb primary key
);
--Inserci�n de webs en la tabla
INSERT INTO TABLAWEBS VALUES(1,'mipaginaweb','.com');
INSERT INTO TABLAWEBS VALUES(2,'cartoon_cars','.uk');
INSERT INTO TABLAWEBS VALUES(3,'esparticular','.es');
INSERT INTO TABLAWEBS VALUES(4,'cosesmeves','.cat');
SELECT * FROM TABLAWEBS;

--Creaci�n del tipo TIPOMARCA. Contiene un campo con la web
CREATE OR REPLACE TYPE TIPOMARCA AS OBJECT(
  idmarca NUMBER,
  nombre VARCHAR2(20),
  web REF TIPOWEB
);
--Creaci�n de la tabla de marcas con la referencia a las webs
CREATE TABLE TABLAMARCAS OF TIPOMARCA(
  PRIMARY KEY(idmarca),
  web REFERENCES TABLAWEBS
);
--Inserci�n de marcas en la tabla
INSERT INTO TABLAMARCAS VALUES(
  1,'mi p�gina web',(SELECT REF(t) FROM TABLAWEBS t WHERE t.idweb=1)
);
INSERT INTO TABLAMARCAS VALUES(
  2,'CARtoon cars',(SELECT REF(t) FROM TABLAWEBS t WHERE t.idweb=2)
);
INSERT INTO TABLAMARCAS SELECT 3,'les coses meves',
REF(t) FROM TABLAWEBS t WHERE t.idweb=4;
SELECT t.* FROM TABLAMARCAS t;
--Consultas a la tabla de marcas
SELECT t.nombre,DEREF(web).nombreurl,DEREF(web).dominio
FROM TABLAMARCAS t;
--Intento de borrado de un registro de la TABLAWEBS para comprobar la
--integridad referencial
DELETE FROM TABLAWEBS WHERE IDWEB=1;

---------------------------------------EJEMPLO 2: RECETAS-----------------------------
--Creaci�n del tipo de los ingredientes
CREATE OR REPLACE TYPE TIPOINGREDIENTE AS OBJECT(
  idingred NUMBER,
  nombre VARCHAR2(20)
);
--Creaci�n de la tabla de ingredientes
CREATE TABLE TABLAINGREDIENTES OF TIPOINGREDIENTE(
    idingred primary key
);
--Inserci�n de ingredientes en la tabla
INSERT INTO TABLAINGREDIENTES VALUES (1,'tomate');
INSERT INTO TABLAINGREDIENTES VALUES (2,'arroz');
INSERT INTO TABLAINGREDIENTES VALUES (3,'pollo');
INSERT INTO TABLAINGREDIENTES VALUES (4,'costilla de cerdo');
INSERT INTO TABLAINGREDIENTES VALUES (5,'jud�as verdes');
INSERT INTO TABLAINGREDIENTES VALUES (6,'garrofones');
INSERT INTO TABLAINGREDIENTES VALUES (7,'azafr�n');
INSERT INTO TABLAINGREDIENTES VALUES (8,'agua');
INSERT INTO TABLAINGREDIENTES VALUES (9,'alcachofa');
INSERT INTO TABLAINGREDIENTES VALUES (10,'patata');
INSERT INTO TABLAINGREDIENTES VALUES (11,'ajo');
INSERT INTO TABLAINGREDIENTES VALUES (12,'garbanzos');
SELECT * FROM TABLAINGREDIENTES;

--Creaci�n del tipo de las recetas
CREATE OR REPLACE TYPE TIPORECETA AS OBJECT(
  idreceta NUMBER,
  nombre VARCHAR2(50),
  duracion NUMBER,
  dificultad VARCHAR2(5)
);
--Creaci�n de la tabla de recetas
CREATE TABLE TABLARECETAS OF TIPORECETA(
  PRIMARY KEY(idreceta)
);
--Inserci�n de recetas en la tabla
INSERT INTO TABLARECETAS VALUES(1,'paella valenciana',120,'media');
INSERT INTO TABLARECETAS VALUES(2,'arroz al horno',100,'baja');
SELECT * FROM TABLARECETAS;

--Creacion del tipo cantidades
CREATE OR REPLACE TYPE TIPOCANTIDADES AS OBJECT(
  id NUMBER,
  receta REF TIPORECETA,
  ingrediente REF TIPOINGREDIENTE,
  cantidad NUMBER
);
--Creaci�n de la tabla
CREATE TABLE TABLACANTIDADES OF TIPOCANTIDADES(
  id PRIMARY KEY,
  receta REFERENCES TABLARECETAS,
  ingrediente REFERENCES TABLAINGREDIENTES
);
SELECT * FROM TABLACANTIDADES;
--Inserci�n de los ingredientes de cada receta en la tabla
INSERT INTO TABLACANTIDADES VALUES(
1, (SELECT REF(TR) FROM TABLARECETAS TR WHERE TR.idreceta=1),
(SELECT REF(TI) FROM TABLAINGREDIENTES TI WHERE TI.idingred=1),
100);
INSERT INTO TABLACANTIDADES VALUES(
2, (SELECT REF(TR) FROM TABLARECETAS TR WHERE TR.idreceta=1),
(SELECT REF(TI) FROM TABLAINGREDIENTES TI WHERE TI.idingred=2),
400);
INSERT INTO TABLACANTIDADES VALUES(
3, (SELECT REF(TR) FROM TABLARECETAS TR WHERE TR.idreceta=1),
(SELECT REF(TI) FROM TABLAINGREDIENTES TI WHERE TI.idingred=3),
1000);
SELECT * FROM TABLACANTIDADES;

--Consulta de los ingredientes de la receta de paella valenciana
SELECT DEREF(ingrediente).nombre AS ingrediente,cantidad
FROM TABLACANTIDADES
WHERE DEREF(receta).nombre='paella valenciana';
--Consulta de las recetas en las que aparezca el ingrediente tomate
SELECT DEREF(receta).nombre AS receta
FROM TABLACANTIDADES
WHERE DEREF(ingrediente).nombre='tomate';

---------------------------------------EJEMPLO 3: MEDICAMENTOS-----------------------------
/*Un medicamento en concreto est� formado por varios componentes y un componente puede aparecer en 
varios medicamentos.*/

--Creaci�n de los tipos
CREATE OR REPLACE TYPE MEDICAMENTO AS OBJECT(
    idmedicamento NUMBER,
    nombremedicamento VARCHAR2(150),
    laboratorio VARCHAR2(50)
);
CREATE OR REPLACE TYPE COMPONENTE AS OBJECT(
    idcomponente NUMBER,
    nombrecomponente VARCHAR2(150)
);
CREATE OR REPLACE TYPE COMPOSICION AS OBJECT(
    idcomposicion number,
    medic REF MEDICAMENTO,
    compon REF COMPONENTE,
    mg NUMBER
);

--Creaci�n de la tabla con identidad de objeto asociada a cada tipo
CREATE TABLE TABLAMEDICAMENTOS OF MEDICAMENTO(
    idmedicamento PRIMARY KEY
);
INSERT INTO TABLAMEDICAMENTOS VALUES(1,'Espidifen','Zambon');
INSERT INTO TABLAMEDICAMENTOS VALUES(2,'GripaVicks','Vicks');
INSERT INTO TABLAMEDICAMENTOS VALUES(5,'Opiren Flas','Takeda');
SELECT * FROM TABLAMEDICAMENTOS;


CREATE TABLE TABLACOMPONENTES OF COMPONENTE(
    idcomponente PRIMARY KEY
);
INSERT INTO TABLACOMPONENTES VALUES(1,'Ibuprofeno');
INSERT INTO TABLACOMPONENTES VALUES(2,'Dimemorfan fosfato');
INSERT INTO TABLACOMPONENTES VALUES(3,'Aspartamo');
INSERT INTO TABLACOMPONENTES VALUES(4,'Sacarosa');
INSERT INTO TABLACOMPONENTES VALUES(5,'Clavulanato pot�sico');
INSERT INTO TABLACOMPONENTES VALUES(6,'amoxicilina trihidrato');
INSERT INTO TABLACOMPONENTES VALUES(7,'lansoprazol');
INSERT INTO TABLACOMPONENTES VALUES(8,'guaifenesina');
INSERT INTO TABLACOMPONENTES VALUES(9,'Fenilefrina hidrocloruro');
INSERT INTO TABLACOMPONENTES VALUES(10,'Paracetamol');
SELECT * FROM TABLACOMPONENTES;

CREATE TABLE TABLACOMPOSICION OF COMPOSICION(
    idcomposicion PRIMARY KEY,
    medic REFERENCES TABLAMEDICAMENTOS,
    compon REFERENCES TABLACOMPONENTES
);

--Espidifen - Ibuprofeno
INSERT INTO TABLACOMPOSICION VALUES(
1,
(SELECT REF(TM) FROM TABLAMEDICAMENTOS TM WHERE idmedicamento=1),
(SELECT REF(TC) FROM TABLACOMPONENTES TC WHERE idcomponente=1),
600
);
--Espidifen - aspartamo
INSERT INTO TABLACOMPOSICION VALUES(
2,
(SELECT REF(TM) FROM TABLAMEDICAMENTOS TM WHERE idmedicamento=1),
(SELECT REF(TC) FROM TABLACOMPONENTES TC WHERE idcomponente=3),
60
);
--Espidifen - sacarosa
INSERT INTO TABLACOMPOSICION VALUES(
3,
(SELECT REF(TM) FROM TABLAMEDICAMENTOS TM WHERE idmedicamento=1),
(SELECT REF(TC) FROM TABLACOMPONENTES TC WHERE idcomponente=4),
2
);
--GripaVicks
INSERT INTO TABLACOMPOSICION VALUES(
4,
(SELECT REF(TM) FROM TABLAMEDICAMENTOS TM WHERE idmedicamento=2),
(SELECT REF(TC) FROM TABLACOMPONENTES TC WHERE idcomponente=10),
500
);
INSERT INTO TABLACOMPOSICION VALUES(
5,
(SELECT REF(TM) FROM TABLAMEDICAMENTOS TM WHERE nombremedicamento='GripaVicks'),
(SELECT REF(TC) FROM TABLACOMPONENTES TC WHERE nombrecomponente='guaifenesina'),
200
);
INSERT INTO TABLACOMPOSICION VALUES(
6,
(SELECT REF(TM) FROM TABLAMEDICAMENTOS TM WHERE nombremedicamento='GripaVicks'),
(SELECT REF(TC) FROM TABLACOMPONENTES TC WHERE nombrecomponente='Fenilefrina hidrocloruro'),
10
);
INSERT INTO TABLACOMPOSICION VALUES(
7,
(SELECT REF(TM) FROM TABLAMEDICAMENTOS TM WHERE nombremedicamento='GripaVicks'),
(SELECT REF(TC) FROM TABLACOMPONENTES TC WHERE nombrecomponente='Sacarosa'),
2000
);
INSERT INTO TABLACOMPOSICION VALUES(
8,
(SELECT REF(TM) FROM TABLAMEDICAMENTOS TM WHERE nombremedicamento='GripaVicks'),
(SELECT REF(TC) FROM TABLACOMPONENTES TC WHERE nombrecomponente='Aspartamo'),
6
);
--Opiren Flas
INSERT INTO TABLACOMPOSICION VALUES(
9,
(SELECT REF(TM) FROM TABLAMEDICAMENTOS TM WHERE nombremedicamento='Opiren Flas'),
(SELECT REF(TC) FROM TABLACOMPONENTES TC WHERE nombrecomponente='lansoprazol'),
30
);
INSERT INTO TABLACOMPOSICION VALUES(
10,
(SELECT REF(TM) FROM TABLAMEDICAMENTOS TM WHERE nombremedicamento='Opiren Flas'),
(SELECT REF(TC) FROM TABLACOMPONENTES TC WHERE nombrecomponente='Aspartamo'),
9
);

--EJEMPLOS DE SELECTS EN TABLAS RELACIONADAS
--Consulta que muestra la composici�n del Espidifen
SELECT DEREF(compon).nombrecomponente,mg
FROM TABLACOMPOSICION
WHERE DEREF(medic).nombremedicamento='Espidifen';
--Consulta de los medicamentos que contienen Aspartamo
SELECT DEREF(medic).nombremedicamento medicamento,mg
FROM TABLACOMPOSICION
WHERE DEREF(compon).nombrecomponente='Aspartamo';

-------------------------------------- FELICES FIESTAS --------------------------------------------
