-------------------------------------------------------------------------------
----------------------------------CLASE 24-------------------------------------
-------------------------------------------------------------------------------

--------------------------------EJEMPLO CON VARRAY-----------------------------
/*Creamos un tipo nuevo con informaci�n sobre una direcci�n*/
CREATE TYPE tipodireccion AS OBJECT(
    tipovia varchar2(20),
    nombrevia varchar2(100),
    numero number,
    bloque number,
    piso number,
    puerta number,
    cp number
);
/*Creamos un colecci�n de direcciones (como sabemos de antemano que de cada
cliente guardaremos la direcci�n de facturaci�n y la direcci�n de env�o, 
usamos un varray con dos casillas*/
CREATE OR REPLACE TYPE arraydirecciones AS VARRAY(2) OF tipodireccion;

/*Creamos una tabla de clientes con sus direcciones*/
CREATE TABLE clientes(
    nif varchar2(9),
    nombre varchar2(50),
    apellidos varchar2(200),
    direcciones arraydirecciones
);

/*Insertamos clientes en la tabla*/
INSERT INTO clientes VALUES(
'78964532T',
'C�ndido',
'P�rez Molina',
arraydirecciones(
                  tipodireccion('Calle','Mayor',3,null,null,null,08342),
                  tipodireccion('Avda','Grande',1,1,1,3,08350)
                 )
);
INSERT INTO clientes VALUES(
'98765432Q',
'Mercedes',
'S�nchez Ortega',
arraydirecciones(
                  tipodireccion('Plaza','Mayor',1,null,null,null,12300),
                  tipodireccion('Plaza','Mayor',1,null,null,null,12300)
                 )
);

/*Consulta que muestre toda la informaci�n de los clientes*/
SELECT * FROM clientes;

/*Consulta que muestre las dos direcciones del cliente con DNI 78964532T*/
SELECT dire.*
FROM clientes cli,TABLE(cli.direcciones) dire
WHERE cli.nif='78964532T';

------------ Otro ejemplo de varray con m�todos que acceden al contenido ---------------------

/*Creamos un tipo nuevo con informaci�n sobre una nota*/
CREATE OR REPLACE TYPE tiponota AS OBJECT(
    semestre number(1),
    nota number(4,2)
);
    
/*Creamos un array de dos casillas del tipo tiponota*/
CREATE OR REPLACE TYPE arraynotas AS VARRAY(2) OF tiponota;

/*Creamos un tipo alumno con el array de notas*/
CREATE OR REPLACE TYPE tipoalumno AS OBJECT(
    idalumno number,
    modulo varchar2(3),
    notas arraynotas,
    MEMBER FUNCTION obtenersemestre(indice NUMBER) RETURN NUMBER,
    MEMBER FUNCTION obtenernota(indice NUMBER) RETURN NUMBER
);
/*Indicamos qu� hace cada funci�n en el cuerpo del tipo*/
CREATE OR REPLACE TYPE BODY tipoalumno AS
    MEMBER FUNCTION obtenersemestre(indice NUMBER) RETURN NUMBER
    IS
    BEGIN
        RETURN notas(indice).semestre;
    END;
    MEMBER FUNCTION obtenernota(indice NUMBER) RETURN NUMBER
    IS
    BEGIN
        RETURN notas(indice).nota;
    END;
END;

/*Creamos una tabla de alumnos con identidad de objeto*/
CREATE TABLE tablaalumnos OF tipoalumno;

/*Insertamos informaci�n en la tabla tablaalumnos*/
INSERT INTO tablaalumnos VALUES(1234,'M02',arraynotas(tiponota(1,2),tiponota(2,8)));
INSERT INTO tablaalumnos VALUES(5678,'M02',arraynotas(tiponota(1,4),tiponota(2,9)));

/*Consulta a la tabla de las notas:*/
SELECT t.* FROM tablaalumnos t;

/* Consulta para mostrar el id de los alumnos junto con la nota que ha
obtenido en el primer semestre y en el segundo semestre*/
SELECT t.idalumno,t.obtenernota(1) as notasemestre1,t.obtenernota(2) as notasemestre2
FROM tablaalumnos t;

/*Consulta que muestre el semestre en el que el alumno 5678
sac� un 4*/
SET SERVEROUTPUT ON;
DECLARE
    notasem1 NUMBER;
    notasem2 NUMBER;
    semestre NUMBER;
BEGIN
    SELECT t.obtenernota(1) INTO notasem1 FROM tablaalumnos t WHERE t.idalumno=5678;
    SELECT t.obtenernota(2) INTO notasem2 FROM tablaalumnos t WHERE t.idalumno=5678;
    IF notasem1=4 THEN
        SELECT t.obtenersemestre(1) INTO semestre FROM tablaalumnos t WHERE t.idalumno=5678;
        DBMS_OUTPUT.PUT_LINE(semestre);
    ELSIF notasem2=4 THEN
        SELECT t.obtenersemestre(2) INTO semestre FROM tablaalumnos t WHERE t.idalumno=5678;
        DBMS_OUTPUT.PUT_LINE(semestre);
    ELSE
        DBMS_OUTPUT.PUT_LINE('No ha sacado un 4 en ning�n semestre');
    END IF;
END;

--------------------------------EJEMPLO CON TABLA ANIDADA-----------------------------
/*Creamos un tipo nuevo con informaci�n sobre una unidad formativa*/
CREATE OR REPLACE TYPE tipouf AS OBJECT(
    iduf number,
    nombre varchar2(150),
    numhoras number
);
/*Creamos una colecci�n (nested table) de unidades formativas*/
CREATE OR REPLACE TYPE tablaufs IS TABLE OF tipouf;

/*Creamos una tabla con informaci�n sobre un m�dulo que contendr� la tabla de las 
unidades formativas de ese m�dulo*/
CREATE TABLE tablamodulos(
    idmodulo VARCHAR2(8),
    nombre VARCHAR(150),
    ufs tablaufs --ser� una nested table
)NESTED TABLE ufs STORE AS unidadesformativas;

/*Insertamos informaci�n en la tabla de los m�dulos*/
INSERT INTO tablamodulos VALUES('DAW-M01','Sistemas inform�ticos', 
tablaufs(
    tipouf(1,'Instalaci�n, configuraci�n y expotaci�n del sistema inform�tico',60),
    tipouf(2,'Gesti�n de la informaci�n y de recursos en una red',80),
    tipouf(3,'Implantaci�n de software espec�fico',25)
    )
);
INSERT INTO tablamodulos VALUES('DAW-M02','Bases de datos', 
tablaufs(
    tipouf(1,'Introducci�n a las bases de datos',33),
    tipouf(2,'Lenguajes SQL: DML y DDL',66),
    tipouf(3,'Lenguaje SQL: DCL y extensi�n procedimental',66),
    tipouf(4,'Bases de datos objeto-relacionales',33)
    )
);
INSERT INTO TABLAMODULOS VALUES('DAW-M10','Formaci�n y orientaci�n laboral', 
tablaufs(
    tipouf(1,'Incorporaci�n al trabajo',66),
    tipouf(2,'Prevenci�n de riesgos laborales',33)
    )
);

/*Consulta que muestre un m�dulo con todas sus unidades formativas*/
SELECT m.idmodulo,m.nombre,u.*
FROM tablamodulos m,TABLE(m.ufs) u;

/*Consulta que muestra las unidades formativas con m�s de 60 horas*/
SELECT m.nombre as "m�dulo",u.iduf as "UF",u.nombre,u.numhoras
FROM tablamodulos m,TABLE(m.ufs) u
WHERE u.numhoras>60;

/*Actualizaci�n de la tabla para modificar el n�mero de horas de la unidad
formativa 1 del m�dulo de FOL*/
UPDATE TABLE(SELECT m.ufs FROM tablamodulos m WHERE m.nombre='Formaci�n y orientaci�n laboral') u
SET u.numhoras=50
WHERE u.iduf=1;



