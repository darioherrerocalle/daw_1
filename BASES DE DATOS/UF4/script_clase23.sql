SET SERVEROUTPUT ON;
CREATE OR REPLACE TYPE articulo AS OBJECT(
    --atributos del objeto
    codigo number,
    nombre varchar2(100),
    altura number,
    anchura number,
    profundidad number,
    precio number,
    --m�todos del objeto
    MEMBER FUNCTION volumen RETURN number,
    MEMBER FUNCTION descuento RETURN number,
    MEMBER PROCEDURE mostrar,
    --m�todo static porque no utiliza datos ya almacenados
    STATIC PROCEDURE articulonuevo(    
    codigo number,
    nombre varchar2,
    altura number,
    anchura number,
    profundidad number,
    precio number)
);
--cuerpo del objeto (d�nde se escribe el c�digo de los m�todos)
CREATE OR REPLACE TYPE BODY articulo AS
    --c�digo del m�todo volumen
    MEMBER FUNCTION volumen RETURN number
    IS
    BEGIN
        RETURN altura*anchura*profundidad;
    END;
    --c�digo del m�todo descuento
    MEMBER FUNCTION descuento RETURN number
    IS
        dto number;
    BEGIN
        dto:=precio*0.95;
        RETURN dto;
    END;
    --c�digo del m�todo est�tico que muestra la informaci�n del art�culo
    MEMBER PROCEDURE mostrar AS
    BEGIN
        DBMS_OUTPUT.PUT_LINE('C�digo: ' || codigo);
        DBMS_OUTPUT.PUT_LINE('Nombre: ' || nombre);
        DBMS_OUTPUT.PUT_LINE('Volumen: ' || volumen() || ' cm3');
        DBMS_OUTPUT.PUT_LINE('Precio con descuento: ' || descuento() || ' �');
    END;
    --c�digo del m�todo static
    STATIC PROCEDURE articulonuevo(    
    codigo number,nombre varchar2,altura number,anchura number,profundidad number,precio number)
    IS
    BEGIN
        INSERT INTO almacen VALUES(codigo,nombre,altura,anchura,profundidad,precio);
    END;
END;
--Creamos la tabla del objeto
drop table almacen;
CREATE TABLE almacen of articulo(
  codigo primary key
);
INSERT INTO almacen VALUES (1,'articulo1',10,2,5,150);
INSERT INTO almacen VALUES (2,'articulo2',1,1,1,15);
SELECT * FROM almacen; --S�lo muestra los atributos
--muestra el resultado de ejecutar las funciones
SELECT a.*,a.volumen() as volumen,a.descuento() as precio_con_descuento
FROM almacen a;
--ejemplo de llamada al m�todo procedimiento
DECLARE
    miarticulo articulo;
BEGIN
    SELECT VALUE(a) INTO miarticulo FROM almacen a
    WHERE a.codigo=3;
    miarticulo.mostrar();
END;

--Ejemplo de llamada al m�todo static
BEGIN
articulo.articulonuevo(3,'articulo3',2,3,5,12);
END;

---------------------------- OTRO EJEMPLO DE OBJETO CON M�TODOS --------------------------------
SET SERVEROUTPUT ON;
drop type obj_persona force;
create or replace type obj_persona as object(
   idpersona number,
   dni varchar2(9),
   nombre varchar2(15),
   apellidos varchar2(30),
   fecha_nacimiento date,
   MEMBER FUNCTION calcularedad RETURN NUMBER
);

-- crear el cuerpo del tipo
CREATE OR REPLACE TYPE BODY obj_persona AS
  MEMBER FUNCTION calcularedad RETURN NUMBER
  IS
    edad NUMBER;
    mesactual NUMBER;
    mesnacimiento NUMBER;
    anyoactual NUMBER;
    anyonacimiento NUMBER;
    diaactual NUMBER;
    dianacimiento NUMBER;
  BEGIN
    SELECT EXTRACT(DAY FROM sysdate) INTO diaactual FROM dual;
    SELECT EXTRACT(MONTH FROM sysdate) INTO mesactual FROM dual;
    SELECT EXTRACT(YEAR FROM sysdate) INTO anyoactual FROM dual;
    SELECT EXTRACT(DAY FROM fecha_nacimiento) INTO dianacimiento FROM dual;
    SELECT EXTRACT(MONTH FROM fecha_nacimiento) INTO mesnacimiento FROM dual;
    SELECT EXTRACT(YEAR FROM fecha_nacimiento) INTO anyonacimiento FROM dual;
    edad:=anyoactual-anyonacimiento;
    IF mesnacimiento>mesactual THEN
      edad:=edad-1;
    ELSIF mesnacimiento=mesactual THEN
      IF dianacimiento>diaactual THEN
        edad:=edad-1;
      END IF;
    END IF;
    RETURN edad;
  END;
END;

drop table socios;
create table socios of obj_persona(
  idpersona primary key
);

insert into socios values( 1, '123456789', 'raul', 'gomez', '19/12/1975'); 
insert into socios values( 2, '123456790', 'ana', 'mas', '10/01/1982');   

select nombre, apellidos, fecha_nacimiento, s.calcularedad() from socios s;