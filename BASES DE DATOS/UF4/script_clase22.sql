------------------------------------ CLASE 22 --------------------------------------------
--Creaci�n de los tipos de datos
CREATE TYPE tipodireccion AS OBJECT(
    tipovia varchar2(20),
    nombrevia varchar2(100),
    numero number,
    bloque number,
    piso number,
    puerta number,
    cp number
);
CREATE TYPE tipotelefono AS OBJECT(
    tipotelef varchar2(20),
    numerotelef number
);
CREATE TYPE tipopersona AS OBJECT(
    nombre varchar2(40),
    direccionfacturacion tipodireccion,
    direccionenvio tipodireccion,
    telefono tipotelefono
);
--creaci�n de las tablas que los usar�n (sin identidad de objeto)
CREATE TABLE clientes(
    nif varchar2(9),
    apellido1 varchar2(100),
    apellido2 varchar2(100),
    datos tipopersona
);
CREATE TABLE proveedores(
    cif varchar2(9),
    datos tipopersona
);
--inserci�n de informaci�n en las tablas
INSERT INTO clientes VALUES (
'83928371J',
'P�rez',
'G�mez',
tipopersona('Ana',
            tipodireccion('calle','Mayor',3,null,null,null,08342),
            tipodireccion('plaza','Mayor',1,null,null,null,08342),
            tipotelefono('personal',682983751)
            )
);
INSERT INTO proveedores VALUES(
    'A8273977B',
    tipopersona('soft1',
                tipodireccion('Avda','Diagonal',234,1,1,1,08300),
                tipodireccion('Avda','Diagonal',234,1,1,1,08300),
                tipotelefono('recepci�n',691092837)
                )
);
--consulta de la informaci�n de las tablas
--consulta que muestra toda la informaci�n de la tabla clientes
SELECT * FROM clientes;
SELECT * FROM proveedores;
--Consulta que muestre el nombre y los apellidos de los clientes
SELECT c.datos.nombre,c.apellido1,c.apellido2
FROM clientes c;
--Consulta que muestre, adem�s, la direcci�n de facturaci�n
SELECT c.datos.nombre,c.apellido1,c.apellido2,
c.datos.direccionfacturacion.tipovia as via,
c.datos.direccionfacturacion.nombrevia as via,
c.datos.direccionfacturacion.numero as numero,
c.datos.direccionfacturacion.bloque as bloque,
c.datos.direccionfacturacion.piso as piso,
c.datos.direccionfacturacion.puerta as puerta,
c.datos.direccionfacturacion.cp as C�digo_postal
FROM clientes c;

--Modificamos el n�mero de la direcci�n de env�o de Ana P�rez
UPDATE CLIENTES c
SET c.datos.direccionenvio.numero=22
WHERE c.datos.nombre='Ana' AND c.apellido1='P�rez';
SELECT c.datos.nombre,c.apellido1,c.apellido2,
c.datos.direccionenvio.tipovia as tipo,
c.datos.direccionenvio.nombrevia as via,
c.datos.direccionenvio.numero as numero,
c.datos.direccionenvio.cp as C�digo_postal
FROM clientes c;

--Borramos el proveedor soft1
DELETE FROM proveedores p
WHERE p.datos.nombre='soft1';
SELECT * FROM proveedores;

--A�adimos un campo identificador al objeto tipodireccion
ALTER TYPE tipodireccion ADD ATTRIBUTE iddireccion number CASCADE;
UPDATE CLIENTES c
SET c.datos.direccionenvio.iddireccion=1
WHERE c.datos.nombre='Ana' AND c.apellido1='P�rez';

--Ejemplo de tabla con identidad de objeto
--Creamos una tabla del tipo TIPODIRECCION con identidad de objeto
CREATE TABLE tabladireccionenvio OF tipodireccion(
  iddireccion PRIMARY KEY
);
SELECT * FROM tabladireccionenvio;
--Insertamos informaci�n en la tabla tabladireccionenvio
INSERT INTO tabladireccionenvio VALUES(
'Calle','del Acebo',23,NULL,NULL,NULL,28016,2);
INSERT INTO tabladireccionenvio VALUES(
'Ronda','de Atocha',2,NULL,NULL,NULL,28012,3);

--Muestra la informaci�n de la tabla TABLADIRECCIONFACTURACION
SELECT * FROM tabladireccionenvio;

--Muestra todos los tipos creados por el usuario
SELECT * FROM USER_TYPES;


    
