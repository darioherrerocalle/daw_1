SET SERVEROUTPUT ON;
/*Creaci�n de la funci�n que devuelve la contrase�a de un usuario*/
CREATE OR REPLACE FUNCTION contrasenya(userintrod usuarios.iduser%TYPE) --recibe por par�metro el identificador de un usuario
RETURN usuarios.pass%TYPE --devolver� un valor del tipo contrase�a
IS
    passtabla usuarios.pass%TYPE;--variable para guardar la contrase�a que recuperaremos de la tabla
BEGIN
    SELECT pass INTO passtabla --guardamos la contrase�a del usuario en la variable passtabla
    FROM usuarios
    WHERE iduser=userintrod;
    --Si el select se ha realizado sin problemas, se ejectur� la siguiente l�nea, si no, saltar� a la excepci�n porque no habr� encontrado la contrase�a del usuario intrododucido
    RETURN passtabla;
EXCEPTION
    --si el usuario introducido no existe en la tabla, la funci�n devolver� un -1
    WHEN NO_DATA_FOUND THEN return -1;
END;

--Uso de la funci�n contrasenya en el procedimiento del login
CREATE OR REPLACE PROCEDURE login --procedimiento que recibel un usuario y una contrase�a
(userintrod usuarios.iduser%TYPE,passintrod usuarios.pass%TYPE)
IS
    passtabla usuarios.pass%TYPE; --variable para guardar lo que devuelva la funci�n contrasenya al llamarla
BEGIN
    passtabla:=contrasenya(userintrod); --llamamso a la funci�n contrasenya y guardamos el resultado en la variable passtabla
    IF (passtabla=-1) THEN --Si la funci�n ha devuelto un -1 entonces
        DBMS_OUTPUT.PUT_LINE('El usuario ' || userintrod || ' no existe');
    ELSIF (passintrod<>passtabla) THEN --si no, si la contrase�a que ha devuelto la funci�n es diferente a la contrase�a introducida
        DBMS_OUTPUT.PUT_LINE('La contrase�a no es correcta');
    ELSE --si no se ha producido ninguna de las dos condiciones anteriores
        DBMS_OUTPUT.PUT_LINE('Bievenido, ' || userintrod);
    END IF;
END;
--Llamada al procedimiento login
ACCEPT userintrod PROMPT 'Usuario:';
ACCEPT passintrod PROMPT 'Contrase�a:';
DECLARE
    userintrod usuarios.iduser%TYPE:='&userintrod';
    passintrod usuarios.pass%TYPE:='&passintrod';
BEGIN
    Login(userintrod,passintrod);
END;

--------------------------------- EXCEPCIONES--------------------------
/*Mismo ejemplo gestionado con excepciones definidas por el usuario*/
CREATE OR REPLACE PROCEDURE login
(userintrod usuarios.iduser%TYPE,passintrod usuarios.pass%TYPE)
IS
    passtabla usuarios.pass%TYPE;
    usernotfound EXCEPTION;
    incorrectpass EXCEPTION;
BEGIN
    passtabla:=contrasenya(userintrod);
    IF (passtabla=-1) THEN
       RAISE usernotfound;
    ELSIF (passintrod<>passtabla) THEN
        RAISE incorrectpass;
    ELSE
        DBMS_OUTPUT.PUT_LINE('Bievenido, ' || userintrod);
    END IF;
EXCEPTION
    WHEN usernotfound THEN
        RAISE_APPLICATION_ERROR(-20014,'El usuario no existe');
    WHEN incorrectpass THEN
        RAISE_APPLICATION_ERROR(-20015,'Contrase�a incorrecta');
END;
--Llamada al procedimiento login
ACCEPT userintrod PROMPT 'Usuario:';
ACCEPT passintrod PROMPT 'Contrase�a:';
DECLARE
    userintrod usuarios.iduser%TYPE:='&userintrod';
    passintrod usuarios.pass%TYPE:='&passintrod';
BEGIN
    Login(userintrod,passintrod);
END;

--------------------------------------------- CURSORES ------------------------------------------------
--A�adimos un campo nuevo a la tabla con el saldo del usuario para poder hacer los ejemplos posteriores
SELECT * FROM usuarios;
ALTER TABLE usuarios ADD saldo NUMBER;
UPDATE usuarios
SET saldo=10
WHERE iduser LIKE 'a%';
UPDATE usuarios
SET saldo=20
WHERE iduser NOT LIKE 'a%';
SELECT * FROM usuarios;

/*ejemplo de procedimiento que calcula el porcentaje del total que representa el saldo de cada usuario*/
CREATE OR REPLACE PROCEDURE porcentajes
IS
    sumasaldos NUMBER; --cursor impl�cito
    CURSOR cursorsaldos IS SELECT * FROM usuarios; --cursor expl�cito con los campos de la tabla usuarios
    fila usuarios%ROWTYPE; --variable para recorrer el cursor
BEGIN
    SELECT SUM(saldo) INTO sumasaldos FROM usuarios; --calculamos la suma de todos los saldos y la guardamos en la variable sumasaldos
    OPEN cursorsaldos; --abrimos el cursor
    LOOP --recorremos el cursor
        FETCH cursorsaldos INTO fila; --coge la siguiente fila del cursor y la guarda en la variable fila
        EXIT WHEN cursorsaldos%NOTFOUND; -- dejar� de recorrer el cursor cuando no queden m�s filas
        DBMS_OUTPUT.PUT_LINE(fila.iduser || ' - ' || ROUND(fila.saldo*100/sumasaldos,2)); --calcula el porcentaje de la fila actual redonde�ndolo a dos decimales y lo muestra por pantalla
    END LOOP;
    CLOSE cursorsaldos; --cerramos el cursor
END;
--Llamada al procedimiento porcentajes sin par�metros
BEGIN
    porcentajes();
END;

--mismo ejemplo pero usando for (que no necesita open ni close ni fetch)
CREATE OR REPLACE PROCEDURE porcentajesfor
IS
    sumasaldos NUMBER; --cursor impl�cito
    CURSOR cursorsaldos IS SELECT * FROM usuarios;
    fila usuarios%ROWTYPE;
BEGIN
    SELECT SUM(saldo) INTO sumasaldos FROM usuarios;
    FOR fila IN cursorsaldos LOOP
        DBMS_OUTPUT.PUT_LINE(fila.iduser || ' - ' || ROUND(fila.saldo*100/sumasaldos,2));
    END LOOP;
END;
--Llamada
BEGIN
    porcentajes();
END;