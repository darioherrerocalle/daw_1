SET SERVEROUTPUT ON;
drop table usuarios;
create table usuarios(
    iduser varchar2(50) primary key,
    pass varchar2(8)
);
INSERT INTO usuarios VALUES('marta','1234');
INSERT INTO usuarios VALUES('roberto','12345');
INSERT INTO usuarios VALUES('alejandro','12345');
INSERT INTO usuarios VALUES('ana','1234');
SELECT * FROM usuarios;

----------------------- CONDICIONES IF --------------------------

/*Ejemplo de IF-ELSE*/
--Pide que se introduzca un nombre de usuario y una constrase�a
ACCEPT vusuario PROMPT 'Usuario:';
ACCEPT vcontra PROMPT 'Contrase�a:';
--Declara una variable para cada valor introducido entre '' porque son cadenas
DECLARE
    userintrod usuarios.iduser%TYPE:='&vusuario';
    passintrod usuarios.pass%TYPE:='&vcontra';
    passtabla usuarios.pass%TYPE; --variable que almacenar� la contrase�a que hay en la tabla para el usuario
BEGIN
    --Consulta que guarda en la variable passtabla, la contrase�a del usuario que hay almacenada en la tabla usuarios
    SELECT pass INTO passtabla
    FROM usuarios
    WHERE iduser=userintrod;
    --Se comprueba si la contrase�a extra�da de la tabla y la introducida coinciden
    IF (passtabla!=passintrod) THEN
        DBMS_OUTPUT.PUT_LINE('Contrase�a incorrecta');
    ELSE
         DBMS_OUTPUT.PUT_LINE('Bienvenido, ' || userintrod);
    END IF;
EXCEPTION
    WHEN NO_DATA_FOUND THEN 
        DBMS_OUTPUT.PUT_LINE('El usuario introducido no existe');
END;

--Ejemplo que pregunta los datos para introducir un usuario nuevo en la tabla usuarios
ACCEPT vusuario PROMPT 'Usuario:';
ACCEPT vcontra1 PROMPT 'Contrase�a:';
ACCEPT vcontra2 PROMPT 'Repite la contrase�a:';
--Declara una variable para cada valor introducido entre '' porque son cadenas
DECLARE
    userintrod usuarios.iduser%TYPE:='&vusuario';
    passintrod1 usuarios.pass%TYPE:='&vcontra1';
    passintrod2 usuarios.pass%TYPE:='&vcontra2';
BEGIN
    --Se comprueba que las dos contrase�as introducidas coincidan
    IF (passintrod1=passintrod2) THEN
        INSERT INTO usuarios VALUES(userintrod,passintrod1);
        DBMS_OUTPUT.PUT_LINE('Usuario registrado correctamente');
    ELSE
        DBMS_OUTPUT.PUT_LINE('Las contrase�as no coinciden');
    END IF;
EXCEPTION
    WHEN DUP_VAL_ON_INDEX THEN 
        DBMS_OUTPUT.PUT_LINE('El usuario ya existe');
END;
/*Mismo ejemplo pero sin usar la excepci�n*/
ACCEPT vusuario PROMPT 'Usuario:';
ACCEPT vcontra1 PROMPT 'Contrase�a:';
ACCEPT vcontra2 PROMPT 'Repite la contrase�a:';
DECLARE
    userintrod usuarios.iduser%TYPE:='&vusuario';
    passintrod1 usuarios.pass%TYPE:='&vcontra1';
    passintrod2 usuarios.pass%TYPE:='&vcontra2';
    cuantoshay NUMBER;
BEGIN
    --Se comprueba que el usuario no exista ya en la tabla
    SELECT COUNT(iduser) INTO cuantoshay
    FROM usuarios
    WHERE UPPER(userintrod)=UPPER(iduser);
    --Comprobamos si en la tabla hab�a alg�n usuario con ese nombre
    IF cuantoshay>0 THEN
        DBMS_OUTPUT.PUT_LINE('Ese usuario ya existe');
    --Si el usuario no existe:
    --Se comprueba que las dos contrase�as introducidas coincidan
    ELSIF (passintrod1=passintrod2) THEN
        INSERT INTO usuarios VALUES(userintrod,passintrod1);
        DBMS_OUTPUT.PUT_LINE('Usuario registrado correctamente');
    ELSE
        DBMS_OUTPUT.PUT_LINE('Las contrase�as no coinciden');
    END IF;
END;

----------------------- PROCEDIMIENTOS Y FUNCIONES --------------------------
/*Ejemplo de procedimiento para introducir un nuevo usuario en la tabla*/
--Definici�n del procedimiento
CREATE OR REPLACE PROCEDURE NuevoUsuario(userintrod usuarios.iduser%TYPE, passintrod1 usuarios.pass%TYPE, passintrod2 usuarios.pass%TYPE)
IS
    cuantoshay NUMBER;
BEGIN
    --Se comprueba que el usuario no exista ya en la tabla
    SELECT COUNT(iduser) INTO cuantoshay
    FROM usuarios
    WHERE UPPER(userintrod)=UPPER(iduser);
    --Comprobamos si en la tabla hab�a alg�n usuario con ese nombre
    IF cuantoshay>0 THEN
        DBMS_OUTPUT.PUT_LINE('Ese usuario ya existe');
    --Si el usuario no existe:
    --Se comprueba que las dos contrase�as introducidas coincidan
    ELSIF (passintrod1=passintrod2) THEN
        INSERT INTO usuarios VALUES(userintrod,passintrod1);
        DBMS_OUTPUT.PUT_LINE('Usuario registrado correctamente');
    ELSE
        DBMS_OUTPUT.PUT_LINE('Las contrase�as no coinciden');
    END IF;
END;
--Llamada al procedimiento
ACCEPT vusuario PROMPT 'Usuario:';
ACCEPT vcontra1 PROMPT 'Contrase�a:';
ACCEPT vcontra2 PROMPT 'Repite la contrase�a:';
DECLARE
    userintrod usuarios.iduser%TYPE:='&vusuario';
    passintrod1 usuarios.pass%TYPE:='&vcontra1';
    passintrod2 usuarios.pass%TYPE:='&vcontra2';
BEGIN
    NuevoUsuario(userintrod,passintrod1,passintrod2);
END;

/*Ejemplo de funci�n RecuperarContrase�a que recibir� por par�metro
un id de usuario y devolver� su contrase�a*/
CREATE OR REPLACE FUNCTION RecuperarContrase�a(userintrod usuarios.iduser%TYPE)
RETURN usuarios.pass%TYPE
IS
    passtabla usuarios.pass%TYPE;
BEGIN
    SELECT pass INTO passtabla
    FROM usuarios
    WHERE UPPER(userintrod)=UPPER(iduser);
    RETURN passtabla;
EXCEPTION
    WHEN NO_DATA_FOUND THEN RETURN -1;
END;

CREATE OR REPLACE PROCEDURE Login(userintrod usuarios.iduser%TYPE,passintrod usuarios.pass%TYPE)
IS
BEGIN
    IF (RecuperarContrase�a(userintrod)=-1) THEN
         DBMS_OUTPUT.PUT_LINE('El usuario no existe');
    ELSIF (RecuperarContrase�a(userintrod)=passintrod) THEN
        DBMS_OUTPUT.PUT_LINE('Bienvenido, ' || userintrod);
    ELSE
        DBMS_OUTPUT.PUT_LINE('Contrase�a incorrecta');
    END IF;
END;
--Llamada al procedimiento Login
ACCEPT vusuario PROMPT 'Usuario:';
ACCEPT vcontra PROMPT 'Contrase�a:';
DECLARE
    userintrod usuarios.iduser%TYPE:='&vusuario';
    passintrod usuarios.pass%TYPE:='&vcontra';
BEGIN
    Login(userintrod,passintrod);
END;

----------------------------------- BUCLES -------------------------------------
/*Ejemplo de un procedimiento que introduce un n�mero de usuarios en la tabla
usando el bucle WHILE*/
CREATE OR REPLACE PROCEDURE Ataque(num NUMBER)
IS
    contador NUMBER:=0;
BEGIN
    WHILE (contador<num) LOOP
        INSERT INTO usuarios VALUES(TO_CHAR(contador),TO_CHAR(contador));
        contador:=contador+1;
    END LOOP;
EXCEPTION
    WHEN DUP_VAL_ON_INDEX THEN DBMS_OUTPUT.PUT_LINE('Ya hay un usuario con ese n�mero');
END;
--Llamada al procedimiento Ataque
ACCEPT numero PROMPT '�Cu�ntos usuarios quieres insertar?';
DECLARE
    vnum NUMBER:=&numero;
BEGIN
    Ataque(vnum);
END;
SELECT * FROM usuarios;
/*Ejemplo de un procedimiento que introduce un n�mero de usuarios en la tabla
usando el bucle LOOP*/
CREATE OR REPLACE PROCEDURE Ataque2(num NUMBER)
IS
    contador NUMBER:=100;
BEGIN
    LOOP
        INSERT INTO usuarios VALUES(TO_CHAR(contador),TO_CHAR(contador));
        contador:=contador+1;
        exit when (contador=num+100);
    END LOOP;
EXCEPTION
    WHEN DUP_VAL_ON_INDEX THEN DBMS_OUTPUT.PUT_LINE('Ya hay un usuario con ese n�mero');
END;
--Llamada al procedimiento Ataque
ACCEPT numero PROMPT '�Cu�ntos usuarios quieres insertar?';
DECLARE
    vnum NUMBER:=&numero;
BEGIN
    Ataque2(vnum);
END;
SELECT * FROM usuarios;

/*Ejemplo de un procedimiento que introduce un n�mero de usuarios en la tabla
usando el bucle FOR*/
CREATE OR REPLACE PROCEDURE Ataque3(num NUMBER)
IS
    final NUMBER;
BEGIN
    final:=num+1000;
    FOR contador IN 1000...final LOOP
        INSERT INTO usuarios VALUES(TO_CHAR(contador),TO_CHAR(contador));
    END LOOP;
EXCEPTION
    WHEN DUP_VAL_ON_INDEX THEN DBMS_OUTPUT.PUT_LINE('Ya hay un usuario con ese n�mero');
END;
--Llamada al procedimiento Ataque3
ACCEPT numero PROMPT '�Cu�ntos usuarios quieres insertar?';
DECLARE
    vnum NUMBER:=&numero;
BEGIN
    Ataque3(vnum);
END;
SELECT * FROM usuarios;