SET SERVEROUTPUT ON;
/*Ejemplo de salida por pantalla*/
BEGIN
DBMS_OUTPUT.PUT_LINE('Hola mundo');
END;

/*Ejemplo tonto de uso de variables y constantes*/
DECLARE
    pi CONSTANT NUMBER:=3.1415;
    factor NUMBER:=5;
    resultado NUMBER;
BEGIN
    resultado:=pi*factor;
    DBMS_OUTPUT.PUT_LINE('El resultado es: ' || resultado);
END;

/*Ejemplo de introducci�n del valor en un cuadro de di�logo*/
ACCEPT vfactor PROMPT 'Introduce el factor multiplicador:';
DECLARE
    pi CONSTANT NUMBER:=3.1415;
    factor NUMBER:=&vfactor;
    resultado NUMBER;
BEGIN
    resultado:=pi*factor;
    DBMS_OUTPUT.PUT_LINE('El resultado es: ' || resultado);
END;

/*Tabla para los ejemplos*/
CREATE TABLE USUARIO(
    idusuario NUMBER primary key,
    nombre VARCHAR2(50)
);
DROP TABLE CUENTAS;
CREATE TABLE CUENTAS(
    idcuenta NUMBER primary key,
    idusuario NUMBER,
    saldo NUMBER,
    CONSTRAINT fk_usuario_cuenta FOREIGN KEY (idusuario) REFERENCES USUARIO(idusuario)
);
INSERT INTO USUARIO VALUES (1,'aaa');
INSERT INTO USUARIO VALUES (2,'bbb');
INSERT INTO CUENTAS VALUES (1111,1,1000);
INSERT INTO CUENTAS VALUES (1112,1,100);
INSERT INTO CUENTAS VALUES (2222,2,10);

/*Ejemplo de almacenamiento en una variable del resultado de ejecutar una consulta*/
ACCEPT vcuenta PROMPT 'Introduce el n�mero de cuenta';
DECLARE
    vcuenta CUENTAS.idcuenta%TYPE:=&vcuenta;
    vsaldo CUENTAS.saldo%TYPE;
BEGIN
    SELECT saldo INTO vsaldo
    FROM CUENTAS
    WHERE idcuenta=vcuenta;
    DBMS_OUTPUT.PUT_LINE('El saldo es: ' || vsaldo);
EXCEPTION
    WHEN NO_DATA_FOUND THEN DBMS_OUTPUT.PUT_LINE('La cuenta no existe');
END;

