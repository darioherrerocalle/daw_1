---------------------------------------------------------------------------
------------------------------CLASE 21-------------------------------------
---------------------------------------------------------------------------
--Creaci�n del tablespace TIENDA
DROP TABLESPACE TIENDA including contents and datafiles;
CREATE TABLESPACE TIENDA
DATAFILE 'c:\archivos_oracle_marta\tienda.dbf'
SIZE 20M;

--Creaci�n del usuario administ
--ALTER SESSION SET "_ORACLE_SCRIPT"=true;
CREATE USER administ
IDENTIFIED BY "1234"
DEFAULT TABLESPACE TIENDA
QUOTA UNLIMITED ON TIENDA;
GRANT DBA TO administ;

--Creaci�n de la tabla CATEGORIAS
DROP TABLE CATEGORIAS;
CREATE TABLE CATEGORIAS(
  idcategoria number primary key,
  descripcion varchar2(30)
);
INSERT INTO CATEGORIAS VALUES(1,'carnicer�a');
INSERT INTO CATEGORIAS VALUES(2,'charcuter�a');
INSERT INTO CATEGORIAS VALUES(3,'l�cteos');
INSERT INTO CATEGORIAS VALUES(4,'bebidas');
INSERT INTO CATEGORIAS VALUES(5,'horno y boller�a');
INSERT INTO CATEGORIAS VALUES(6,'congelados');
INSERT INTO CATEGORIAS VALUES(7,'pescader�a');
INSERT INTO CATEGORIAS VALUES(8,'perfumer�a');
INSERT INTO CATEGORIAS VALUES(9,'limpieza');
INSERT INTO CATEGORIAS VALUES(10,'cosm�tica');
INSERT INTO CATEGORIAS VALUES(11,'puericultura');
INSERT INTO CATEGORIAS VALUES(12,'aperitivos');
INSERT INTO CATEGORIAS VALUES(13,'legumbres y arroz');
INSERT INTO CATEGORIAS VALUES(14,'caf� y t�');
INSERT INTO CATEGORIAS VALUES(15,'conservas');
INSERT INTO CATEGORIAS VALUES(16,'fruter�a y verduler�a');
INSERT INTO CATEGORIAS VALUES(17,'huevos');
INSERT INTO CATEGORIAS VALUES(18,'pizzas y masas');
INSERT INTO CATEGORIAS VALUES(19,'postres');
INSERT INTO CATEGORIAS VALUES(20,'sopas, caldos y pur�s');
INSERT INTO CATEGORIAS VALUES(21,'cereales y az�car');
INSERT INTO CATEGORIAS VALUES(22,'desayunos y meriendas');
INSERT INTO CATEGORIAS VALUES(23,'platos preparados');

--Creaci�n de la tabla PRODUCTOS
DROP TABLE  PRODUCTOS;
CREATE TABLE PRODUCTOS(
  idproducto number primary key,
  nombre varchar2(50),
  precio number(4,2),
  categoria number,
  stock number,
  constraint productos_categoria_fk foreign key (categoria) references CATEGORIAS(idcategoria)
);
INSERT INTO PRODUCTOS VALUES(1,'pollo entero',6.46,1,20);
INSERT INTO PRODUCTOS VALUES(2,'pollo filetes pechuga',5.10,1,25);
INSERT INTO PRODUCTOS VALUES(3,'leche entera 1 litro',0.64,3,50);
INSERT INTO PRODUCTOS VALUES(4,'leche semidesnatada 1 litro',0.89,3,50);
INSERT INTO PRODUCTOS VALUES(5,'leche desnatada 1 litro',0.85,3,4);
INSERT INTO PRODUCTOS VALUES(6,'helado bote chocolate gold',2.30,6,15);
INSERT INTO PRODUCTOS VALUES(7,'helado cono mini nata 12 unidades',2.35,6,15);
INSERT INTO PRODUCTOS VALUES(8,'helado palo bomb�n almendrado caja 6 unidades',2.10,6,15);
INSERT INTO PRODUCTOS VALUES(9,'mandarina bandeja 1400 gramos',1.95,16,10);
INSERT INTO PRODUCTOS VALUES(10,'manzana golden bolsa 1500 gramos',2.13,16,10);
INSERT INTO PRODUCTOS VALUES(11,'pera conferencia bolsa 1000 gramos',1.52,16,10);
INSERT INTO PRODUCTOS VALUES(12,'jud�a verde plana paquete 750 gramos',1.70,16,10);
INSERT INTO PRODUCTOS VALUES(13,'pepino unidad',0.39,16,6);
INSERT INTO PRODUCTOS VALUES(14,'pimiento verde fre�r malla 750 gramos',1.59,16,5);
INSERT INTO PRODUCTOS VALUES(15,'tomate canario malla 2 kg',2.65,16,8);

--Consultas a las tablas
--SELECT * FROM CATEGORIAS;
--SELECT * FROM PRODUCTOS;




------------------------------------- M�S EJEMPLOS ---------------------------------------------
--Creaci�n del tablespace VIDEOJUEGOS
DROP TABLESPACE VIDEOJUEGOS INCLUDING CONTENTS AND DATAFILES;
CREATE TABLESPACE VIDEOJUEGOS
DATAFILE 'c:\app\Marta\oradata\orcl\clase21.dbf'
SIZE 100M;

--Creaci�n del usuario administrador con rol DBA
CREATE USER administrador
IDENTIFIED BY "1234"
DEFAULT TABLESPACE VIDEOJUEGOS
QUOTA UNLIMITED ON VIDEOJUEGOS;
GRANT DBA TO administrador;
connect administrador;

-- Creaci�n de las tablas
DROP TABLE ciudades;
 CREATE TABLE ciudades(
	id_ciudad NUMBER,
  cpro VARCHAR2(2),
	cp VARCHAR2(4),
  nombre_ciudad VARCHAR2(50),
  CONSTRAINT pk_ciudades PRIMARY KEY(id_ciudad)
);
DROP TABLE video_juegos;
CREATE TABLE video_juegos(  
    id_juego NUMBER, 
    nombre_juego VARCHAR2(130) NOT NULL,
    importe_juego NUMBER NOT NULL,
    edad_min NUMBER NOT NULL,
    cant_total NUMBER DEFAULT 1 NOT NULL,
    versionde NUMBER DEFAULT NULL,
    CONSTRAINT pk_video_juegos PRIMARY KEY (id_juego),
    CONSTRAINT fk_versionde FOREIGN KEY(versionde) REFERENCES video_juegos(id_juego)
); 
DROP TABLE clientes;
CREATE TABLE clientes(
	id_cliente NUMBER,
	nombre_cliente VARCHAR2(50) NOT NULL,
	edad NUMBER NOT NULL,
  telefono VARCHAR2(9) NOT NULL,
  ciudad NUMBER,
	CONSTRAINT pk_clientes PRIMARY KEY(id_cliente),
  CONSTRAINT fk_cp_clientes FOREIGN KEY(ciudad) REFERENCES ciudades(id_ciudad)
); 
DROP TABLE empleados;
CREATE TABLE empleados(
	id_empl NUMBER,
	nombre_empl VARCHAR2(50) NOT NULL,
	sueldo DECIMAL(6,2) NOT NULL,
	edad NUMBER NOT NULL,
  ciudad NUMBER,
	CONSTRAINT pk_empleados PRIMARY KEY(id_empl),
  CONSTRAINT fk_cp_empleados FOREIGN KEY(ciudad) REFERENCES ciudades(id_ciudad)
);
DROP TABLE alquiler_juegos;
CREATE TABLE alquiler_juegos(
	id_juego NUMBER,
	id_cliente NUMBER,
	fecha_alquiler DATE,
	fecha_dev DATE,
	id_empl NUMBER NOT NULL,
	CONSTRAINT pk_alquiler_juegos PRIMARY KEY(id_juego, id_cliente, fecha_alquiler),
	CONSTRAINT fk_video_juegos FOREIGN KEY(id_juego) REFERENCES video_juegos(id_juego),
	CONSTRAINT fk_clientes FOREIGN KEY(id_cliente) REFERENCES clientes(id_cliente),
	CONSTRAINT fk_empleados FOREIGN KEY(id_empl) REFERENCES empleados(id_empl)
); 
 
-- Inserci�n de filas en las tablas 

-- Inserci�n de registros en la tabla ciudades
INSERT INTO ciudades VALUES (1,'01','4001','Alegr�a-Dulantzi');					
INSERT INTO ciudades VALUES (2,'01','9002','Amurrio');					
INSERT INTO ciudades VALUES (3,'01','3049','A�ana');					
INSERT INTO ciudades VALUES (4,'01','5003','Aramaio');					
INSERT INTO ciudades VALUES (5,'01','6006','Armi��n');					
INSERT INTO ciudades VALUES (6,'01','6037','Arraia-Maeztu');					
INSERT INTO ciudades VALUES (7,'01','8008','Arrazua-Ubarrundia');					
INSERT INTO ciudades VALUES (8,'01','0004','Artziniega');					
INSERT INTO ciudades VALUES (9,'01','1009','Asparrena');					
INSERT INTO ciudades VALUES (10,'01','5010','Ayala/Aiara');					
INSERT INTO ciudades VALUES (11,'01','2011','Ba�os de Ebro/Ma�ueta');					
INSERT INTO ciudades VALUES (12,'01','3013','Barrundia');					
INSERT INTO ciudades VALUES (13,'01','8014','Berantevilla');					
INSERT INTO ciudades VALUES (14,'01','4016','Bernedo');					
INSERT INTO ciudades VALUES (15,'01','0017','Campezo/Kanpezu');					
INSERT INTO ciudades VALUES (16,'01','0021','Elburgo/Burgelu');					
INSERT INTO ciudades VALUES (17,'01','5022','Elciego');					
INSERT INTO ciudades VALUES (18,'01','1023','Elvillar/Bilar');					
INSERT INTO ciudades VALUES (19,'01','8046','Erriberagoitia/Ribera Alta');					
INSERT INTO ciudades VALUES (20,'01','5056','Harana/Valle de Arana');					
INSERT INTO ciudades VALUES (21,'01','5901','Iru�a Oka/Iru�a de Oca');					
INSERT INTO ciudades VALUES (22,'01','8027','Iruraiz-Gauna');					

-- Inserci�n de registros en la tabla video_juegos
INSERT INTO video_juegos VALUES(1, 'PS4 Battlefield Hardline' ,80, 14, 15, null);
INSERT INTO video_juegos VALUES(2, 'Assassins Creed: Origin' ,90, 18, 19, null);
INSERT INTO video_juegos VALUES(3, 'PS4 Minecraft Story Mode, Season 2' ,40, 8, 40, null); 
INSERT INTO video_juegos VALUES(4, 'PS4 FIFA 18' ,18, 18, 30, null); 
INSERT INTO video_juegos VALUES(5, 'Xbox One FIFA 18' ,80, 12, 40, 4); 
INSERT INTO video_juegos VALUES(6, 'NINTENDO Switch FIFA 18' ,90, 18, 20, 4); 
INSERT INTO video_juegos VALUES(7, 'PS4 Minecraft' ,40, 4, 10, null);
INSERT INTO video_juegos VALUES(8, 'Xbox Minecraft' ,45, 4, 10, 7);
INSERT INTO video_juegos VALUES(9, 'NINTENDO Switch Minecraft' ,60, 6, 18, 7);
INSERT INTO video_juegos VALUES(10, 'Xbox One SoulCalibur VI' ,30, 16, 17, null);
INSERT INTO video_juegos VALUES(11, 'Xbox One Tom Clancys Ghost Recon: Wildlands' ,70, 18, 15, null);
INSERT INTO video_juegos VALUES(12, 'Xbox One Halo: The Master Chief Collection' ,90, 18, 6, null);
INSERT INTO video_juegos VALUES(13, 'XBOXONE JUST DANCE 2019' ,40, 6, 1, null);
INSERT INTO video_juegos VALUES(14, 'WIIU JUST DANCE 2019' ,20, 6, 1, 13);
INSERT INTO video_juegos VALUES(15, 'NINTENDO Switch Super Mario Odyssey' ,60, 3, 2, null);
INSERT INTO video_juegos VALUES(16, 'NINTENDO Switch Lego DC Super Villanos' ,55, 7, 10, null);
INSERT INTO video_juegos VALUES(17, 'Just Dance' ,55, 7, 1, null);

-- Inserci�n de registros en la tabla clientes
INSERT INTO clientes VALUES(1, 'Pablo Rojo', 18, '934505151',22); 
INSERT INTO clientes VALUES(2, 'Mar�a Ba�os', 21, '916800000',12); 
INSERT INTO clientes VALUES(3, 'Pedro Alto', 14, '933500000',12); 
INSERT INTO clientes VALUES(4, 'Ana Ru�z', 18, '932660000',1); 
INSERT INTO clientes VALUES(5, 'Mario Caro', 21, '974600000',1); 
INSERT INTO clientes VALUES(6, 'Pepe P�rez', 15, '913000000',15); 
INSERT INTO clientes VALUES(7, 'Clara D�az', 18, '982428000',15); 
INSERT INTO clientes VALUES(8, 'Pepe P�rez', 21, '938900000',15); 
INSERT INTO clientes VALUES(9, 'Ra�l Cano', 50, '981560000',15);
INSERT INTO clientes VALUES(10, 'Alberto S�nchez', 20, '',15);
INSERT INTO clientes VALUES(11, 'Juan Garc�a', 21, '',15);
INSERT INTO clientes VALUES(12, 'Ramiro L�pez', 32, '',15);
INSERT INTO clientes VALUES(13, 'Ana Romero', 43, '',15);
INSERT INTO clientes VALUES(14, 'Toni Casals', 18, '',15);
INSERT INTO clientes VALUES(15, 'Marta Guill�n', 20, '',15);
INSERT INTO clientes VALUES(16, 'Lara Berm�dez', 19, '',15);
INSERT INTO clientes VALUES(17, 'Marta Mart�nez', 18, '',22);
INSERT INTO clientes VALUES(18, 'Ana Collado', 20, '',12);
INSERT INTO clientes VALUES(19, 'Pedro Villalba', 20, '',1);
INSERT INTO clientes VALUES(20, 'Ricardo Ru�z', 21, '',1);
INSERT INTO clientes VALUES(21, 'Nekane Azuaga', 16, '981450000',22);

-- Inserci�n de registros en la tabla empleados
INSERT INTO empleados VALUES(1, 'Ramon Pi', 350, 21, 1); 
INSERT INTO empleados VALUES(2, 'Sara Ruso', 400, 40, 12);
INSERT INTO empleados VALUES(3, 'Juan Paz', 600, 25, 1);
INSERT INTO empleados VALUES(4, 'Angel Ros', 350.25, 18, 33);
INSERT INTO empleados VALUES(5, 'Marcos Pi', 500, 40, 20);
INSERT INTO empleados VALUES(6, 'Diego Tornero', 500, 37, 1); 
INSERT INTO empleados VALUES(7, 'Raul Fandi�o', 500, 30, 1); 
INSERT INTO empleados VALUES(8, 'Vicente Amiguet', 1500, 41, 1); 
INSERT INTO empleados VALUES(9, 'Sara Amargo', 1500, 21, 20); 
INSERT INTO empleados VALUES(10, 'Amareto L�pez', 1500, 55, 10); 
INSERT INTO empleados VALUES(11, 'Di�genes Layes', 1200, 43, 1); 
INSERT INTO empleados VALUES(12, 'Sergio Hern�ndez', 1300, 61, 20); 
INSERT INTO empleados VALUES(13, 'Juana Fern�ndez', 1100, 32, 3); 
INSERT INTO empleados VALUES(14, 'Mar�a Su�rez', 800, 42, 12); 
INSERT INTO empleados VALUES(15, 'Ana Carapeto', 900, 50, 8); 

-- Inserci�n de registros en la tabla alquiler_juegos
INSERT INTO alquiler_juegos VALUES(1, 1, '27/02/2018', NULL, 1);
INSERT INTO alquiler_juegos VALUES(2, 1, '20/02/2018', '23/03/2018', 1);
INSERT INTO alquiler_juegos VALUES(4, 1, '15/01/2018', NULL, 2);
INSERT INTO alquiler_juegos VALUES(3, 5, '10/02/2018', NULL, 1);
INSERT INTO alquiler_juegos VALUES(4, 2, '10/02/2018', NULL, 2);
INSERT INTO alquiler_juegos VALUES(2, 2, '30/01/2018', '03/02/2018', 2);
INSERT INTO alquiler_juegos VALUES(5, 6, '03/02/2018', '07/02/2018', 2);
INSERT INTO alquiler_juegos VALUES(5, 7, '03/02/2018', NULL, 1);
INSERT INTO alquiler_juegos VALUES(5, 8, '03/02/2018', NULL, 3);
INSERT INTO alquiler_juegos VALUES(5, 9, '07/09/2018', NULL, 1);
INSERT INTO alquiler_juegos VALUES(2, 2, '07/09/2018', '27/02/2018', 2);
INSERT INTO alquiler_juegos VALUES(2, 1, '13/10/2018', NULL, 3);
INSERT INTO alquiler_juegos VALUES(5, 6, '13/10/2018', NULL, 1);
INSERT INTO alquiler_juegos VALUES(2, 6, '13/10/2018', '15/10/2018',3);
INSERT INTO alquiler_juegos VALUES(4, 1, '03/03/2018', NULL, 3);
INSERT INTO alquiler_juegos VALUES(4, 2, '03/03/2018', '13/03/2018', 2);
INSERT INTO alquiler_juegos VALUES(5, 1, '03/03/2018', NULL, 1);
INSERT INTO alquiler_juegos VALUES(16, 21, '05/11/2018', NULL, 3);   
SELECT * FROM alquiler_juegos;
SELECT * FROM empleados;
SELECT * FROM video_juegos;
SELECT * FROM Clientes;

 /*Disparador que antes de vender un videojuego, comprueba
 que hay suficientes unidades en stock*/
 CREATE OR REPLACE TRIGGER controlstock
 BEFORE UPDATE OF cant_total ON video_juegos
 FOR EACH ROW
  BEGIN
    IF :old.cant_total<=1 THEN
      RAISE_APPLICATION_ERROR(-20099,'No hay suficiente stock');
    END IF;
  END;
/*intentamos actualizar el stock de un juego del que solamente hay una unidad 
en stock y comprobamos que se ejecuta el trigger autom�ticamente*/
BEGIN
UPDATE video_juegos
SET cant_total=cant_total-1
WHERE id_juego=17;
END;

--Disparador que muestra un mensaje si la edad del cliente es inferior a la 
--edad m�nima del juego
CREATE OR REPLACE TRIGGER control_edad
BEFORE INSERT ON alquiler_juegos
FOR EACH ROW
DECLARE
  v_edadmin video_juegos.id_juego%TYPE; --variable para guardar la edad m�nima del juego
  v_edadcliente clientes.edad%TYPE; --variable para guardar la edad del cliente que hace el alquiler
BEGIN
  SELECT edad_min INTO v_edadmin FROM video_juegos WHERE :new.id_juego=id_juego;
  SELECT edad INTO v_edadcliente FROM clientes WHERE :new.id_cliente=id_cliente;
  IF v_edadcliente < v_edadmin THEN
     RAISE_APPLICATION_ERROR(-20099,'El cliente no tiene edad para alquilar este juego');
  END IF;
 END;
 
/*Intentamos introducir un alquiler en el que la edad del cliente no supera
la edad permitida por el juego y comprobamos que el disparador se ejecuta
autom�ticamente*/
BEGIN
 INSERT INTO alquiler_juegos VALUES(2, 21, '06/11/2018', NULL, 3);
END;

/*DISPARADOR CON CURSOR que despu�s de borrar o actualizar un videojuego cuenta cu�ntas veces
ha sido alquilado cada juego*/
CREATE OR REPLACE TRIGGER CuentaAlquileres
AFTER DELETE OR UPDATE ON VIDEO_JUEGOS
DECLARE
  CURSOR totalalquilados IS SELECT id_juego,COUNT(*) as CUENTA
                            FROM ALQUILER_JUEGOS
                            GROUP BY ID_JUEGO;
  vnombre VIDEO_JUEGOS.NOMBRE_JUEGO%TYPE;
BEGIN
  FOR fila IN totalalquilados LOOP
    SELECT NOMBRE_JUEGO INTO vnombre FROM video_juegos WHERE fila.id_juego=id_juego;
    DBMS_OUTPUT.PUT_LINE('El juego '||vnombre||' ha sido alquilado '||fila.CUENTA||' veces');
  END LOOP;
END;

SELECT * FROM alquiler_juegos;
SELECT * FROM VIDEO_JUEGOS;
--Borramos un videojuego para comprobar el disparador
ACCEPT vnombre PROMPT '�Qu� juego quieres borrar?';
DECLARE
  vnombre VIDEO_JUEGOS.NOMBRE_JUEGO%TYPE:='&vnombre';
BEGIN
  DELETE VIDEO_JUEGOS
  WHERE VIDEO_JUEGOS.NOMBRE_JUEGO=vnombre;
END;

