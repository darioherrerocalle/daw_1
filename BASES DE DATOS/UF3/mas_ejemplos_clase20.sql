--DROP TABLESPACE TIENDA including contents and datafiles;
--DROP USER administ CASCADE;
--DROP TABLE CATEGORIAS;
--DROP TABLE PRODUCTOS;
--DROP FUNCTION ObtenerPrecio;
--DROP FUNCTION PorcentajeCategoria;
--DROP FUNCTION ComprobarReposicion;
--DROP FUNCTION ActualizasStock;
--DROP FUNCTION BuscarNombreProducto;
SET SERVEROUTPUT ON;
---------------------------------------------------------------------------
------------------------------CLASE 20-------------------------------------
---------------------------------------------------------------------------

--Creaci�n del tablespace TIENDA
CREATE TABLESPACE TIENDA
DATAFILE 'c:\archivos_oracle\tienda.dbf'
SIZE 20M;

--Creaci�n del usuario admin
ALTER SESSION SET "_ORACLE_SCRIPT"=true;
CREATE USER administ
IDENTIFIED BY "1234"
DEFAULT TABLESPACE TIENDA
QUOTA UNLIMITED ON TIENDA;
GRANT DBA TO administ;

--Creaci�n de la tabla CATEGORIAS
DROP TABLE CATEGORIAS;
CREATE TABLE CATEGORIAS(
  idcategoria number primary key,
  descripcion varchar2(30)
);
INSERT INTO CATEGORIAS VALUES(1,'carnicer�a');
INSERT INTO CATEGORIAS VALUES(2,'charcuter�a');
INSERT INTO CATEGORIAS VALUES(3,'l�cteos');
INSERT INTO CATEGORIAS VALUES(4,'bebidas');
INSERT INTO CATEGORIAS VALUES(5,'horno y boller�a');
INSERT INTO CATEGORIAS VALUES(6,'congelados');
INSERT INTO CATEGORIAS VALUES(7,'pescader�a');
INSERT INTO CATEGORIAS VALUES(8,'perfumer�a');
INSERT INTO CATEGORIAS VALUES(9,'limpieza');
INSERT INTO CATEGORIAS VALUES(10,'cosm�tica');
INSERT INTO CATEGORIAS VALUES(11,'puericultura');
INSERT INTO CATEGORIAS VALUES(12,'aperitivos');
INSERT INTO CATEGORIAS VALUES(13,'legumbres y arroz');
INSERT INTO CATEGORIAS VALUES(14,'caf� y t�');
INSERT INTO CATEGORIAS VALUES(15,'conservas');
INSERT INTO CATEGORIAS VALUES(16,'fruter�a y verduler�a');
INSERT INTO CATEGORIAS VALUES(17,'huevos');
INSERT INTO CATEGORIAS VALUES(18,'pizzas y masas');
INSERT INTO CATEGORIAS VALUES(19,'postres');
INSERT INTO CATEGORIAS VALUES(20,'sopas, caldos y pur�s');
INSERT INTO CATEGORIAS VALUES(21,'cereales y az�car');
INSERT INTO CATEGORIAS VALUES(22,'desayunos y meriendas');
INSERT INTO CATEGORIAS VALUES(23,'platos preparados');

--Creaci�n de la tabla PRODUCTOS
DROP TABLE  PRODUCTOS;
CREATE TABLE PRODUCTOS(
  idproducto number primary key,
  nombre varchar2(50),
  precio number(4,2),
  categoria number,
  stock number,
  constraint productos_categoria_fk foreign key (categoria) references CATEGORIAS(idcategoria)
);
INSERT INTO PRODUCTOS VALUES(1,'pollo entero',6.46,1,20);
INSERT INTO PRODUCTOS VALUES(2,'pollo filetes pechuga',5.10,1,25);
INSERT INTO PRODUCTOS VALUES(3,'leche entera 1 litro',0.64,3,50);
INSERT INTO PRODUCTOS VALUES(4,'leche semidesnatada 1 litro',0.89,3,50);
INSERT INTO PRODUCTOS VALUES(5,'leche desnatada 1 litro',0.85,3,4);
INSERT INTO PRODUCTOS VALUES(6,'helado bote chocolate gold',2.30,6,15);
INSERT INTO PRODUCTOS VALUES(7,'helado cono mini nata 12 unidades',2.35,6,15);
INSERT INTO PRODUCTOS VALUES(8,'helado palo bomb�n almendrado caja 6 unidades',2.10,6,15);
INSERT INTO PRODUCTOS VALUES(9,'mandarina bandeja 1400 gramos',1.95,16,10);
INSERT INTO PRODUCTOS VALUES(10,'manzana golden bolsa 1500 gramos',2.13,16,10);
INSERT INTO PRODUCTOS VALUES(11,'pera conferencia bolsa 1000 gramos',1.52,16,10);
INSERT INTO PRODUCTOS VALUES(12,'jud�a verde plana paquete 750 gramos',1.70,16,10);
INSERT INTO PRODUCTOS VALUES(13,'pepino unidad',0.39,16,6);
INSERT INTO PRODUCTOS VALUES(14,'pimiento verde fre�r malla 750 gramos',1.59,16,5);
INSERT INTO PRODUCTOS VALUES(15,'tomate canario malla 2 kg',2.65,16,8);

--Consultas a las tablas
SELECT * FROM CATEGORIAS;
SELECT * FROM PRODUCTOS;

--Funci�n que recibe un nombre de producto y devuelve su precio
DROP FUNCTION ObtenerPrecio;
CREATE OR REPLACE FUNCTION ObtenerPrecio(n PRODUCTOS.NOMBRE%TYPE) RETURN PRODUCTOS.PRECIO%TYPE
IS
    vprecio number(4,2);
BEGIN
    SELECT PRECIO INTO vprecio FROM PRODUCTOS WHERE NOMBRE=n;
    RETURN(vprecio);
EXCEPTION
    WHEN NO_DATA_FOUND THEN return 0;
END;

/*Funci�n que recibe el nombre de una categoria y muestra el porcentaje de productos 
que pertenecen a esa categor�a*/
SELECT * FROM PRODUCTOS;
SET SERVEROUTPUT ON;
CREATE OR REPLACE FUNCTION CalcularPorcentajeCategoria(c CATEGORIAS.DESCRIPCION%TYPE) RETURN DECIMAL
IS
    porcentaje decimal(5,2);
    totalproductos number;
    nproductos number;
BEGIN
    SELECT COUNT(IDPRODUCTO) INTO nproductos FROM PRODUCTOS GROUP BY CATEGORIA HAVING CATEGORIA=(SELECT IDCATEGORIA FROM CATEGORIAS WHERE DESCRIPCION=c);
    SELECT COUNT(IDPRODUCTO) INTO totalproductos FROM PRODUCTOS;
    porcentaje:=nproductos*100/totalproductos;
    return(porcentaje);
EXCEPTION
    WHEN NO_DATA_FOUND THEN return -1;
    WHEN ZERO_DIVIDE THEN return -2;
END;

/*Funci�n que recibe el nombre de un producto, comprueba si su stock es menor que 10 y devuelve una frase
indicando si es o no necesario reponer el producto*/
CREATE OR REPLACE FUNCTION ComprobarReposicion(n PRODUCTOS.NOMBRE%TYPE) RETURN VARCHAR2
IS
    vstock PRODUCTOS.STOCK%TYPE;
BEGIN
    SELECT STOCK INTO vstock FROM PRODUCTOS WHERE NOMBRE=n;
    IF vstock<5 THEN
        return 'NECESITA REPOSICI�N URGENTE';
    ELSIF vstock<10 THEN
        return 'NECESITO REPOSICI�N';
    ELSE
        return 'STOCK OK';
    END IF;
EXCEPTION
    WHEN NO_DATA_FOUND THEN return 'El producto no se encuentra en la base de datos';
END;


/*Ejemplo de uso de funciones predefinidas*/
SET SERVEROUTPUT ON;
DECLARE    
cadena  VARCHAR2(20) := 'Clase19 DAW-M2';
cadena2  VARCHAR2(20) := '200';
BEGIN   
	dbms_output.put_line('Funci�n SYSDATE: ' || SYSDATE);
 	dbms_output.put_line('Fecha larga: ' || TO_CHAR(SYSDATE,'Day, DD "de" Month "de" YYYY'));
 	dbms_output.put_line('Cadena convertida a n�mero: ' || TO_NUMBER(cadena2));
	dbms_output.put_line('Cadena: ' || cadena);
 	dbms_output.put_line('Longitud: ' || LENGTH(cadena)); 
 	dbms_output.put_line('Posici�n M2: ' || INSTR(cadena,'M2',1));
 	dbms_output.put_line('substituci�n e por a: ' || REPLACE(cadena,'e','a'));
 	dbms_output.put_line('2 caracteres a partir posici�n 4: ' ||SUBSTR(cadena,4,2));
 	dbms_output.put_line('may�sculas: ' || UPPER(cadena));
 	dbms_output.put_line('min�sculas: ' || LOWER(cadena));
END;

/*Procedimiento que recibe un nombre de producto y un n�mero y aumenta el stock del producto
en el n�mero indicado.*/
CREATE OR REPLACE PROCEDURE ActualizarStock(vnombre PRODUCTOS.NOMBRE%TYPE,vnumero number)
IS
    --No hace falta ninguna variable local pero el IS tiene que estar igual
BEGIN
    UPDATE PRODUCTOS
    SET STOCK=STOCK+vnumero
    WHERE nombre=vnombre;
EXCEPTION
    WHEN NO_DATA_FOUND THEN DBMS_OUTPUT.PUT_LINE('El producto ' || vnombre || ' no existe');
END;
SELECT * FROM PRODUCTOS;
--LLAMADAS A LOS PROCEDIMIENTOS Y FUNCIONES PREVIOS
--Llamada a la funci�n ObtenerPrecio
ACCEPT producto PROMPT 'Nombre del producto:';
DECLARE
    producto PRODUCTOS.NOMBRE%TYPE:='&producto';
    resultado NUMBER;
BEGIN
    resultado:=ObtenerPrecio(producto);
    IF resultado=0 THEN
        DBMS_OUTPUT.PUT_LINE(producto || ' no es un nombre de producto v�lido');
    ELSE
      DBMS_OUTPUT.PUT_LINE(producto || ' vale ' || resultado);
    END IF;
END;

--Llamada a la funci�n CalcularPorcentajeCategoria
SELECT * FROM PRODUCTOS;
ACCEPT categoria PROMPT 'Introduce el nombre de la categoria para saber su porcentaje';
DECLARE
  categoria PRODUCTOS.nombre%TYPE:='&categoria';
  resultado DECIMAL(5,2);
BEGIN
  resultado:=CalcularPorcentajeCategoria(categoria);
  IF resultado=-1 THEN
    DBMS_OUTPUT.PUT_LINE(categoria || ' no es un nombre de categoria v�lido');
  ELSIF resultado=-2 THEN
    DBMS_OUTPUT.PUT_LINE('No se puede calcular el porcentaje si no hay productos');
  ELSE
    DBMS_OUTPUT.PUT_LINE('Hay un ' || resultado || '% de productos de la categor�a ' || categoria);
  END IF;
END;

--Llamada a la funci�n ComprobarReposicion
ACCEPT producto PROMPT 'Introduce el nombre del producto para ver si hay que reponerlo';
DECLARE
  producto PRODUCTOS.NOMBRE%TYPE:='&producto';
BEGIN
  DBMS_OUTPUT.PUT_LINE(ComprobarReposicion(producto));
END;

--Llamada al procedimiento ActualizarStock
ACCEPT producto PROMPT 'Introduce el nombre del producto';
ACCEPT operacion PROMPT '�Entrada (E) o Salida(S)?';
ACCEPT unidades PROMPT '�Cu�ntas unidades quieres registrar?';
DECLARE
  producto PRODUCTOS.NOMBRE%TYPE:='&producto';
  operacion VARCHAR2(1):='&operacion';
  unidades PRODUCTOS.STOCK%TYPE:='&unidades';
  unidadesconsigno PRODUCTOS.STOCK%TYPE;
BEGIN
    IF (upper(operacion)='E') THEN
        ActualizarStock(producto,unidades);
        DBMS_OUTPUT.PUT_LINE('stock sumado');
    ELSIF upper(operacion)='S' THEN
        unidadesconsigno:=unidades*(-1);
        ActualizarStock(producto,unidadesconsigno);
        DBMS_OUTPUT.PUT_LINE('stock restado');
    ELSE
        DBMS_OUTPUT.PUT_LINE('Introduce E para registrar una entrada o S para registrar una salida');
    END IF;
END;
SELECT * FROM PRODUCTOS;


--Ejemplo procedimiento que muestra el nombre de los productos que empiezan por una determinada letra o palabras
CREATE OR REPLACE PROCEDURE BuscarProductoPorInicial(inicial VARCHAR2)
IS
  cantidadproductos NUMBER;
  producto PRODUCTOS.nombre%TYPE;
  contador NUMBER(3):=0;
BEGIN
  SELECT COUNT(*) INTO cantidadproductos FROM PRODUCTOS;
  FOR i IN 1..cantidadproductos LOOP
    SELECT nombre INTO producto FROM PRODUCTOS WHERE idproducto=i;
    IF SUBSTR(producto,0,1)=inicial THEN
      DBMS_OUTPUT.PUT_LINE(producto);
      contador:=contador+1;
    END IF;
  END LOOP;
  IF contador=0 THEN
    DBMS_OUTPUT.PUT_LINE('No hay ning�n producto con esa inicial');
  END IF;
END;

--Llamada al procedimiento BuscarNombreProducto
ACCEPT inicial PROMPT '�Qu� quieres buscar?';
DECLARE
  inicial VARCHAR2(1):='&inicial';
BEGIN
  BuscarProductoPorInicial(inicial);
END;

/*Procedimiento que recibe un nombre de producto y un n�mero y disminuye el stock del producto
en el n�mero indicado comprobando que hay stock suficiente.*/
--Crea el procedimiento con dos par�metros
CREATE OR REPLACE PROCEDURE Venta(nombreproducto VARCHAR2, cantidad NUMBER)
--declaraci�n de variables
IS
  registroproducto PRODUCTOS%ROWTYPE; --variable que guarda el stock del producto introducido
  stockinsuficiente EXCEPTION; --variable para gestionar el error
BEGIN
  --guardamos la fila del producto en la variable registroproducto
  SELECT * INTO registroproducto FROM PRODUCTOS WHERE nombre=nombreproducto;
  --comprobamos si hay m�s stock que la cantidad introducida
  IF registroproducto.stock<cantidad THEN
    --si no hay suficiente stock salta a la excepci�n que lleva por nombre stockinsuficiente
    RAISE stockinsuficiente;
  --Si hay sufiente stock
  ELSE  
    --actualiza la tabla productos restando la cantidad
    UPDATE PRODUCTOS
    SET stock=stock-cantidad
    WHERE nombre=nombreproducto;
    DBMS_OUTPUT.PUT_LINE('Venta realizada correctamente');
  END IF;
EXCEPTION
  WHEN NO_DATA_FOUND THEN
    RAISE_APPLICATION_ERROR(-20019,'Producto no encontrado');
  WHEN stockinsuficiente THEN
    RAISE_APPLICATION_ERROR(-20020,'Stock insuficiente');
END;
--Llamada al procedimiento Venta
ACCEPT nomprod PROMPT 'Producto';
ACCEPT cantprod PROMPT 'Unidades';
DECLARE
  nomprod PRODUCTOS.nombre%TYPE:='&nomprod';
  cantprod NUMBER:=&cantprod;
BEGIN
--Venta('leche desnatada 1 litro',10);
Venta(nomprod,cantprod); --Llamada para que se ejecute el procedimiento Venta 
                        --con el nombre y la cantidad introducida por el usuario
END;
--SELECT * FROM PRODUCTOS;

----------------------------------------------------------------------------------------------
/*Procedimiento que recibe el nombre de una categor�a y calcula la media de los
precios de esa categor�a.*/
--SELECT * FROM CATEGORIAS;
--SELECT * FROM PRODUCTOS;
CREATE OR REPLACE PROCEDURE MediaPreciosCategoria(nombrecat CATEGORIAS.DESCRIPCION%TYPE)
IS
    media DECIMAL(5,2);
    totalproductoscategoria NUMBER:=0;
    nohayproductos EXCEPTION;
BEGIN
    SELECT COUNT(*) INTO totalproductoscategoria FROM PRODUCTOS
    WHERE CATEGORIA=(SELECT IDCATEGORIA FROM CATEGORIAS WHERE DESCRIPCION=nombrecat);
    IF totalproductoscategoria=0 THEN
        RAISE nohayproductos;
    ELSE
        SELECT AVG(PRECIO) INTO media
        FROM PRODUCTOS
        WHERE CATEGORIA=(SELECT IDCATEGORIA FROM CATEGORIAS WHERE DESCRIPCION=nombrecat);
        DBMS_OUTPUT.put_line('La media de los productos de ' || nombrecat || ' es ' || media);
    END IF;
EXCEPTION
    WHEN nohayproductos THEN
        DBMS_OUTPUT.put_line('No hay productos de ' || nombrecat);
END;
--Llamada al procedimiento
SET SERVEROUTPUT ON;
ACCEPT categ PROMPT 'Categor�a:';
DECLARE
    categ CATEGORIAS.DESCRIPCION%TYPE:='&categ';
BEGIN
  MediaPreciosCategoria(categ);
END;

--------------------------------------------------------------------------------------
--Mismo procedimiento usando un cursor
CREATE OR REPLACE PROCEDURE MediaPrecios(nombrecat VARCHAR2)
IS --Declaraci�n de las variables
  CURSOR productosdeunacategoria IS --Cursor con los productos de la categor�a
  SELECT precio FROM PRODUCTOS
  WHERE categoria=(SELECT idcategoria FROM CATEGORIAS WHERE descripcion=nombrecat);
  cadafila PRODUCTOS%ROWTYPE;--variable para recorrer el cursor
  sumaprecios NUMBER:=0; --variable que acumular� las sumas de los precios
  cantidadfilas NUMBER:=0; --variable que contar� cu�ntas filas hay en el cursor
BEGIN
  --recorremos las filas del cursor sumando el precio de cada fila con la variable sumaprecios
  FOR cadafila IN productosdeunacategoria LOOP
    sumaprecios:=sumaprecios+cadafila.precio;
    cantidadfilas:=cantidadfilas+1;
  END LOOP;
    --Calculamos la media
    --cantidadfilas:=productosdeunacategoria%ROWCOUNT;
    dbms_output.put_line('Media='||sumaprecios/cantidadfilas);
  EXCEPTION
    WHEN ZERO_DIVIDE THEN
      dbms_output.put_line('No hay productos de la categor�a introducida');
END; --final del procedimiento

--Llamada al procedimiento
SET SERVEROUTPUT ON;
ACCEPT categ PROMPT 'Categor�a:';
DECLARE
    categ CATEGORIAS.DESCRIPCION%TYPE:='&categ';
BEGIN
  MediaPreciosCategoria(categ);
END;

-----------------------------------------------------------------------------------------
--Procedimiento que baja el precio de todos los productos en un porcentaje
CREATE OR REPLACE PROCEDURE Rebajas(porcentaje NUMBER)
IS
  CURSOR cproductos IS
  SELECT precio FROM PRODUCTOS
  FOR UPDATE;
  cadafila PRODUCTOS%ROWTYPE;
BEGIN
  FOR cadafila IN cproductos LOOP
    UPDATE PRODUCTOS
    SET precio=precio*((100-porcentaje)/100)
    WHERE CURRENT OF cproductos;
  END LOOP;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      dbms_output.put_line('No hay productos');
END;
--Llamada al procedimiento rebajas
BEGIN
    ACCEPT porcentaje PROMPT 'Porcentaje de rebaja:';
DECLARE
    porcentaje DECIMAL(4,2):=&porcentaje;
    Rebajas(porcentaje);
END;
--SELECT * FROM PRODUCTOS;


