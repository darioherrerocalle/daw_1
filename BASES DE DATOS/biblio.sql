CREATE DATABASE BIBLIOTECA;
USE BIBLIOTECA;
CREATE TABLE USUARIO(
	dni varchar (9) primary key,
	nombre varchar (30) not null
);
CREATE TABLE LIBRO(
	codigo integer primary key,
	titulo varchar (30) not null
);
CREATE TABLE PRESTAMO(
	idPrestamo integer primary key,
	dni varchar (9),
	codigoLibro integer,
	fechaInicio date not null,
	fechaFin date not null,
	constraint fk_dni foreign key (dni) references USUARIO(dni),
	constraint fk_codigo_libro foreign key (codigoLibro) references LIBRO(codigo)
);