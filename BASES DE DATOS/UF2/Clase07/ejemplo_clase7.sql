/*MODELO RELACIONAL
PROPIETARIO(NIF,Nombre,Apellido1,Apellido2,Telefono)
INMUEBLE(Codigo,Direccion,m2,tipo,NIF_propietario)
CLIENTE(Codigo,Nombre,Apellido1,Apellido2,Telefono)
VISITA(Codigo_in,Codigo_cli,FechayHora)*/
CREATE DATABASE INMOBILIARIA;
USE INMOBILIARIA;
CREATE TABLE PROPIETARIO(
	NIF VARCHAR(9) PRIMARY KEY,
    Nombre char(25) not null,
    Apellido1 char(50) not null,
    Apellido2 char(50) null,
    Telefono int
);
CREATE TABLE INMUEBLE(
	Codigo mediumint primary key auto_increment,
    Direccion char(100),
    m2 smallint,
    tipo char(50) check (tipo IN ('piso','chalet','casa')),
    NIF_propietario varchar(9),
    amueblado bool,
    CONSTRAINT fk_inm_prop FOREIGN KEY (NIF_propietario) REFERENCES PROPIETARIO(NIF)
);
CREATE TABLE CLIENTE(
	Codigo mediumint primary key auto_increment,
    Nombre char(25) not null,
    Apellido1 char(50) not null,
    Apellido2 char(50) null,
    Telefono int
);
CREATE TABLE VISITA(
	Codigo_in mediumint,
    Codigo_cli mediumint,
    FechayHora timestamp,
    CONSTRAINT pk_visita PRIMARY KEY (Codigo_in,Codigo_cli,FechayHora),
    CONSTRAINT fk_visita_in FOREIGN KEY (Codigo_in) REFERENCES INMUEBLE(Codigo),
    CONSTRAINT fk_visita_cli FOREIGN KEY (Codigo_cli) REFERENCES CLIENTE(Codigo)
);
/*CREACION DE INDICES*/
CREATE FULLTEXT INDEX buscadireccion ON INMUEBLE(Direccion);
CREATE UNIQUE INDEX buscarpropietario ON INMUEBLE(NIF_propietario,Codigo);

