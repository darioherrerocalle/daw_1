/*Creación de la base de datos CICLISMO*/
create schema if not exists CICLISMO;
USE CICLISMO;

/*Creación de la tabla EQUIPO*/
create table if not exists EQUIPO(
	IdEquipo int auto_increment not null primary key,
    NombreEquipo varchar(25) not null,
    Director varchar(50)
);

/*Creación de la table CICLISTA*/
create table if not exists CICLISTA(
	IdCiclista int auto_increment not null primary key,
    NombreCiclista varchar(25) not null,
    Edad varchar(50),
    Equipo int,
    constraint fk_equipo foreign key (Equipo) references EQUIPO(IdEquipo)
);

/*Creación de la table CARRERA*/
create table if not exists CARRERA(
	IdCarrera int auto_increment not null primary key,
    NombreCarrera varchar(75),
    Fecha date
);

/*Creación de la table ETAPA*/
create table if not exists ETAPA(
	NumEtapa tinyint not null,
    IdCarrera int,
    km tinyint,
    Salida varchar(40),
    Meta varchar(40),
    constraint pk_etapa primary key (NumEtapa, IdCarrera),
    constraint fk_carrera foreign key (IdCarrera) references CARRERA(IdCarrera)
);

/*Creación de la table PARTICIPA*/
create table if not exists PARTICIPA(
	NumEtapa tinyint,
    IdCiclista int,
    Dorsal smallint,
    constraint pk_participa primary key (NumEtapa, IdCiclista),
    constraint fk_ciclista foreign key (IdCiclista) references CICLISTA(IdCiclista),
    constraint fk_etapa foreign key (NumEtapa) references ETAPA(NumEtapa)
);