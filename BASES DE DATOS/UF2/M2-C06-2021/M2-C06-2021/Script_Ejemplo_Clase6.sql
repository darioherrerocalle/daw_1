CREATE DATABASE FACTURACION;
USE FACTURACION;
CREATE TABLE CLIENTE(
	id_cliente integer primary key,
    nombre char(50) not null,
    apellido char(50) not null,
    direccion char(100),
    fecha_nac date,
    telef smallint,
    email varchar(100)
);
CREATE TABLE MODO_PAGO(
	num_pago tinyint primary key auto_increment,
    nombre char(50),
    otro_detalles text
);
CREATE TABLE FACTURA(
	num_factura integer primary key auto_increment,
    id_cliente integer,
    fecha date,
    modo_pago tinyint,
    CONSTRAINT fk_fact_cli FOREIGN KEY (id_cliente) REFERENCES CLIENTE(id_cliente),
    CONSTRAINT fk_fact_modopago FOREIGN KEY (modo_pago) REFERENCES MODO_PAGO(num_pago)
);