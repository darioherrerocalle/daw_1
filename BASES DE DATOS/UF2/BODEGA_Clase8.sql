DROP SCHEMA IF EXISTS BODEGA;
CREATE SCHEMA BODEGA;
USE BODEGA;

/*----------------- CREACIÓN DE LAS TABLAS-----------------*/

CREATE TABLE CLIENTE(
	Cli_num SMALLINT,
	Cli_nombre CHAR(75) NOT NULL,
	Direccion CHAR(100) NOT NULL,
	Ciudad CHAR(50) NOT NULL,
	Telefono VARCHAR(20),
	NIF VARCHAR(25) UNIQUE,
	Tipo VARCHAR(10) DEFAULT 'Bronze',
	Fecha_ultimo_pedido DATE,
	CONSTRAINT PK_CLIENTE PRIMARY KEY (Cli_num)
);

CREATE TABLE PRODUCTO(
	Pro_num SMALLINT,
	Pro_nombre CHAR(150) NOT NULL,
	Precio DECIMAL(7,2) NOT NULL,
	Stock INTEGER NOT NULL,
	CONSTRAINT PK_PRODUCTO PRIMARY KEY (Pro_num)
	);

CREATE TABLE PEDIDO(
	Pedido_num int auto_increment,
	Cli_num SMALLINT,
	Fecha_pedido DATE,
    CONSTRAINT PEDIDO_PK PRIMARY KEY (Pedido_num),
	CONSTRAINT PEDIDO_CLIENTE_FK FOREIGN KEY (Cli_num) REFERENCES CLIENTE(Cli_num)
	);
CREATE TABLE LINEA_PEDIDO(
	Lin_num INTEGER,
    Pedido_num INTEGER,
	Pro_num SMALLINT NOT NULL,
    Unidades INTEGER NOT NULL CHECK (Unidades>0),
    total_linea DECIMAL(7,2),
	CONSTRAINT PK_FACTURA PRIMARY KEY (Lin_num,Pedido_num),
    CONSTRAINT LINEA_PEDIDO FOREIGN KEY (Pedido_num) REFERENCES PEDIDO(Pedido_num),
    CONSTRAINT LINEA_PRODUCTO_FK FOREIGN KEY (Pro_num) REFERENCES PRODUCTO(Pro_num)
	);
    
/*-----------------MODIFICACIÓN DE LAS TABLAS-----------------*/
ALTER TABLE CLIENTE
	-- Borrar el atributo Fecha_ultimo_pedido de la tabla cliente
    DROP COLUMN Fecha_ultimo_pedido,
	-- Añadir una restricción al altributo Tipo de la tabla cliente
	ADD CONSTRAINT CLIENTE_Tipo_CK CHECK(Tipo IN ('Platinum','Gold','Silver','Bronze'));
    
-- Añadir el atributo Importe a la tabla PEDIDO
ALTER TABLE PEDIDO
	ADD COLUMN Importe DECIMAL(5,2);
    
/*----------------- INSERCIÓN DE REGISTROS EN LAS TABLAS-----------------*/

-- INSERCIÓN DE REGISTROS EN LA TABLA CLIENTE
INSERT INTO CLIENTE VALUES(1,'Pepito Pérez','C/Mayor,32','Barcelona','932729283','87156297H',null);
INSERT INTO CLIENTE (Cli_num,Cli_nombre,Direccion,Ciudad,Telefono,Tipo,NIF) VALUES(101,'Joan Alemany ','C. Muntaner, 80','Barcelona','933131313','Gold','176784267B');
INSERT INTO CLIENTE (Cli_num,Cli_nombre,Direccion,Ciudad,Telefono,Tipo,NIF) VALUES(102,'Pep Álvarez','C. Lleò, 32','Badalona','933951313','Bronze','453420307D');
INSERT INTO CLIENTE (Cli_num,Cli_nombre,Direccion,Ciudad,Telefono,Tipo,NIF) VALUES(103,'Cinta Armengol','C. Casp, 50','Barcelona','933131212','Bronze','893084415G');
INSERT INTO CLIENTE (Cli_num,Cli_nombre,Direccion,Ciudad,Telefono,Tipo,NIF) VALUES(104,'Carme Balasc','C. Nou, 27','Vic','937399191','Bronze','139208509C');
INSERT INTO CLIENTE (Cli_num,Cli_nombre,Direccion,Ciudad,Telefono,Tipo,NIF) VALUES(105,'Víctor Bonastre','C. Muntaner, 40','Barcelona','933131111','Bronze','175880731F');
INSERT INTO CLIENTE (Cli_num,Cli_nombre,Direccion,Ciudad,Telefono,Tipo,NIF) VALUES(106,'Lidia Bosch','C. Mar, 43','Badalona',null,null,'196249494Q');
INSERT INTO CLIENTE (Cli_num,Cli_nombre,Direccion,Ciudad,Telefono,Tipo,NIF) VALUES(107,'Josep Casajuana','C. Anglí, 63','Barcelona','934131313','Silver','182581783D');
INSERT INTO CLIENTE (Cli_num,Cli_nombre,Direccion,Ciudad,Telefono,Tipo,NIF) VALUES(108,'José Antonio Díaz','C. Pedraforca, 48','Vic','937399292','Silver','471549505D');
INSERT INTO CLIENTE (Cli_num,Cli_nombre,Direccion,Ciudad,Telefono,Tipo,NIF) VALUES(109,'Isabel Fernández','C. Aribau, 169','Barcelona','934131212','Platinum','316823834F');
INSERT INTO CLIENTE (Cli_num,Cli_nombre,Direccion,Ciudad,Telefono,Tipo,NIF) VALUES(110,'Denis García','C. Sardenya, 157','Barcelona','934131111','Silver','160862984H');

-- INSERCIÓN DE REGISTROS EN LA TABLA PRODUCTO
INSERT INTO PRODUCTO VALUES(201,'Tempranillo',8.5,100);
INSERT INTO PRODUCTO VALUES(202,'Malvasia',4.0,130);
INSERT INTO PRODUCTO VALUES(203,'Bobal',5.5,120);
INSERT INTO PRODUCTO VALUES(204,'Muscat',6.0,150);
INSERT INTO PRODUCTO VALUES(205,'Garnatxa',3.5,100);
INSERT INTO PRODUCTO VALUES(206,'Xarel·lo',4.5,50);
INSERT INTO PRODUCTO VALUES(207,'Verdejo',5.0,200);
INSERT INTO PRODUCTO VALUES(208,'Cabernet sauvignon',7.0,120);
INSERT INTO PRODUCTO VALUES(209,'Parellada',6.5,145);
INSERT INTO PRODUCTO VALUES(210,'Merlot',4.0,70);
INSERT INTO PRODUCTO VALUES(211,'Chardonnay',6.0,168);
INSERT INTO PRODUCTO VALUES(212,'Gewürtztraminer',9.0,90);
INSERT INTO PRODUCTO VALUES(213,'Monastrell',3.5,177);
INSERT INTO PRODUCTO VALUES(214,'Syrah',5.0,48);
INSERT INTO PRODUCTO VALUES(215,'Macabeu',4.5,75);

-- INSERCIÓN DE REGISTROS EN LA TABLA PEDIDO
INSERT INTO PEDIDO VALUES(2,101,'2015-06-05',407.5);
INSERT INTO PEDIDO VALUES(3,101,'2015-11-04',140);
INSERT INTO PEDIDO VALUES(4,101,'2015-01-18',210);
INSERT INTO PEDIDO VALUES(5,102,'2015-06-17',39);
INSERT INTO PEDIDO VALUES(6,103,'2016-02-04',190);
INSERT INTO PEDIDO VALUES(7,104,'2015-06-30',14);
INSERT INTO PEDIDO VALUES(8,105,'2015-07-08',130);
INSERT INTO PEDIDO VALUES(9,102,'2016-02-12',13);
INSERT INTO PEDIDO VALUES(10,105,'2015-02-27',40);

-- INSERCIÓN EN LA TABLA LINEA_PEDIDO
INSERT INTO LINEA_PEDIDO VALUES(1,2,201,25,25*8.5);
INSERT INTO LINEA_PEDIDO VALUES(2,2,204,10,10*6.0);
INSERT INTO LINEA_PEDIDO VALUES(3,2,215,30,30*4.5);
INSERT INTO LINEA_PEDIDO VALUES(1,3,201,20,20*8.5);
INSERT INTO LINEA_PEDIDO VALUES(2,3,204,15,15*6.0);
INSERT INTO LINEA_PEDIDO VALUES(1,4,215,8,8*4.5);
INSERT INTO LINEA_PEDIDO VALUES(2,4,201,50,50*8.5);
INSERT INTO LINEA_PEDIDO VALUES(3,4,204,28,28*6.0);
INSERT INTO LINEA_PEDIDO VALUES(4,4,215,45,45*4.5);
INSERT INTO LINEA_PEDIDO VALUES(1,5,207,50,50*5.0);
INSERT INTO LINEA_PEDIDO VALUES(2,5,204,11,11*6.0);
INSERT INTO LINEA_PEDIDO VALUES(3,5,215,33,33*4.5);
INSERT INTO LINEA_PEDIDO VALUES(1,6,207,120,120*5.0);
INSERT INTO LINEA_PEDIDO VALUES(1,7,204,20,20*6.0);
INSERT INTO LINEA_PEDIDO VALUES(2,7,215,40,40*4.5);
INSERT INTO LINEA_PEDIDO VALUES(1,8,201,5,5*8.5);
INSERT INTO LINEA_PEDIDO VALUES(2,8,204,1,1*6.0);
INSERT INTO LINEA_PEDIDO VALUES(3,8,215,3,3*4.5);
INSERT INTO LINEA_PEDIDO VALUES(1,9,201,25,25*8.5);
INSERT INTO LINEA_PEDIDO VALUES(2,9,207,15,15*5.0);
INSERT INTO LINEA_PEDIDO VALUES(1,10,215,30,30*4.5);

/*----------------- CONSULTAS -----------------*/
-- SELECT campo FROM tabla WHERE condiciones

-- Mostrar todos los campos de todos los clientes
SELECT * FROM CLIENTE;
-- Mostrar todos los campos de todos los productos
SELECT * FROM PRODUCTO;
-- Mostrar todos los campos de todos los pedidos
SELECT * FROM PEDIDO;
-- Mostrar todos los campos de todas las líneas
SELECT * FROM LINEA_PEDIDO;

-- Mostrar solamente el nombre, la ciudad y el tipo de todos los clientes
SELECT Cli_nombre,Ciudad,Tipo FROM CLIENTE;
SELECT Cli_nombre AS 'Cliente',Ciudad,Tipo FROM CLIENTE;

-- Mostrar la dirección y la ciudad de los clientes en una misma columna llamada DIRECCION COMPLETA
SELECT CONCAT(Direccion,' - ',Ciudad) AS 'DirEccion' FROM CLIENTE;

-- Mostrar el código de los productos de los que se ha hecho algun pedido
SELECT Pro_num FROM LINEA_PEDIDO;

-- Mostrar el número de los productos de los que se ha hecho algun pedido sin repeticiones
SELECT DISTINCT Pro_num FROM LINEA_PEDIDO;

-- Mostrar el importe de los pedidos añadiendo el iva (multiplicando el precio por 1,21)
SELECT Importe AS 'Subtotal',Importe*1.21 AS 'Total con IVA' FROM PEDIDO;

-- Mostrar los tres primeros números del teléfono del cliente con el título PREFIJO
SELECT substring(Telefono,1,3) as 'Prefijo' FROM CLIENTE;

-- Mostrar el nombre y la dirección de los clientes de Barcelona
SELECT Cli_nombre,Direccion,Ciudad FROM CLIENTE WHERE Ciudad='Barcelona';

-- Mostrar el nombre y la dirección de los clientes que NO sean de Barcelona
SELECT Cli_nombre,Direccion,Ciudad FROM CLIENTE WHERE Ciudad!='Barcelona';

-- Mostrar el nombre y la ciudad de los clientes de Barcelona y de Vic
SELECT Cli_nombre,Direccion,Ciudad FROM CLIENTE WHERE Ciudad='Barcelona' OR Ciudad='Vic';

-- Mostrar el número de los pedidos en los que se ha comprado el producto 201
SELECT Pedido_num FROM LINEA_PEDIDO WHERE Pro_num=201;

-- Mostrar las líneas del pedido 3
SELECT * FROM LINEA_PEDIDO WHERE Pedido_num=3;

-- Mostrar el nombre y el precio de los productos que tengan un precio entre 5 y 10
SELECT Pro_nombre,Precio FROM PRODUCTO WHERE Precio>=5 AND Precio<=10;
SELECT Pro_nombre,Precio FROM PRODUCTO WHERE Precio between 5 and 10;

-- Mostrar el nombre y la ciudad de los clientes de Barcelona, de Vic o de Reus
SELECT Cli_nombre,Ciudad FROM CLIENTE WHERE Ciudad IN ('Barcelona','Vic','Reus');

-- Mostrar los clientes sin teléfono (con un valor nulo en el campo teléfono)
SELECT * FROM CLIENTE WHERE Telefono IS NULL;

-- Mostrar el nombre de los clientes que sí que tengan teléfono
SELECT * FROM CLIENTE WHERE Telefono IS NOT NULL;

-- Mostrar el nombre de los clientes cuyo nombre empiece por P
SELECT Cli_nombre FROM CLIENTE WHERE Cli_nombre LIKE ('P%');
-- Mostrar el nombre de los clientes cuyo nombre empiece por P
SELECT Cli_nombre FROM CLIENTE WHERE Cli_nombre LIKE ('P_');--aquí solo muestra 1 único carácter después de la A--


-- Mostrar el nombre de los clientes cuyo teléfono esté en el rango 933XXXXXX
SELECT Cli_nombre,Telefono FROM CLIENTE WHERE Telefono LIKE ('933______');

-- Mostrar las líneas del pedido 2 en las que se hayan comprado más de 20 unidades
SELECT * 
FROM LINEA_PEDIDO 
WHERE Pedido_num=2 AND Unidades>20;

-- Mostrar el nombre de los productos cuyo código esté entre 201 y 207 o cuyo precio sea 5 o menos
SELECT Pro_nombre
FROM PRODUCTO
WHERE Pro_num BETWEEN 201 AND 207 OR Precio<=5;

-- Mostrar el nombre de los productos cuyo código esté entre 201 y 207 y cuyo precio sea 5 o menos
SELECT Pro_nombre
FROM PRODUCTO
WHERE Pro_num BETWEEN 201 AND 207 AND Precio<=5;

-- Mostrar las líneas que no sean ni del pedido 1, ni del 2, ni del 3
SELECT * FROM LINEA_PEDIDO WHERE Pedido_num NOT IN (1,2,3);

-- Mostrar los pedidos con fecha de hoy
SELECT * FROM PEDIDO WHERE Fecha_pedido=current_date();

-- Mostrar los pedidos del año 2016
SELECT * FROM PEDIDO WHERE Fecha_pedido BETWEEN '2016-01-01' AND '2016-12-31';

-- Mostrar los pedidos posteriores al 1 de Junio del 2015
SELECT * FROM PEDIDO WHERE Fecha_pedido>'2015-06-01';

