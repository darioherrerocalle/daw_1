package actividad07.ejercicio02;

import actividad07.ejercicio01.cuentaCorriente.Utilidades;

public class MenuPedido {
	//funcion para mostrar el menu
	public static void mostrarMenu(Comida[] pedido) {
		
        boolean salir = false;

        while (!salir) {
            try {
                System.out.println();
                System.out.println("MENU");
                System.out.println();
                
                imprimirPedido(pedido);
                System.out.println();
                
                String opciones[] = new String[3];
                opciones[0] = "Salir";
                opciones[1] = "Mostrar pedido";
                opciones[2] = "Sustituir por horchata";

                for (int i = 0; i < opciones.length; i++) {
                    System.out.println(i + ": " + opciones[i]);
                }
                
                System.out.println();
                Integer entrada = Utilidades.pideEntero("Introduce la operacion que quieres realizar: ");

                switch (entrada) {
                    case 0:
                        System.out.println("Hasta pronto!");
                        salir = true;
                        break;
                    case 1:
                        System.out.println("Has seleccionado la opcion 1. Vamos a mostrar el pedido.");
                        MenuPedido.imprimirPedido(pedido);
                        break;
                    case 2:
                        System.out.println("Has seleccionado la opcion 2. Vamos a cambiar una linea de pedido por una horchata.");
                        MenuPedido.sustituirHorchata(pedido);
                        break;
                    default:
                        System.out.println("Solo tengo 3 opciones, lo siento :( ");
                        break;
                }

            } catch (Exception e) {
                System.out.println("Eso no es un n�mero.");
            }
        }
    }
	
	//funcion para imprimir el pedido

	public static void imprimirPedido(Comida[] pedido) {
		System.out.println("Informacion del pedido: ");
		
		for (int i = 0; i < pedido.length; i++) {
			System.out.println(i + 1 + ". " + pedido[i].toString());
		}
	}
	
	//funcion para sustituir una lidea de pedido por hoorchata
	
	public static void sustituirHorchata(Comida[] pedido) {
		Integer entrada = Utilidades.pideEntero("Introduce la linea de pedido que quieres cambiar: ");
		String nombre = Utilidades.pideString("Introduce el nombre de la horchata: ");
		Double precio = Utilidades.pideDouble("Introduce el precio de la horchata: ");
		Integer kcal = Utilidades.pideEntero("Introduce las kcal: ");
		Integer cantidad = Utilidades.pideEntero("Introduce la cantidad de la horchata: ");
		Integer chufa = Utilidades.pideEntero("Introduce el porcentaje de chufa: ");
		
		pedido[entrada - 1] = new Horchata(nombre, precio, kcal, cantidad, chufa);
		
	}
}
