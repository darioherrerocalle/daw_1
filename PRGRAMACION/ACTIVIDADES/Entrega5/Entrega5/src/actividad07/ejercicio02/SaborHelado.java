package actividad07.ejercicio02;

public class SaborHelado extends Comida {
	protected Integer porcentajeGrasa;
	protected String tipoEdulcorante;
	
	//CONSTRUCTORES
	public SaborHelado() {
		super();
	}
	public SaborHelado(String nombre, Double precio, Integer kcal) {
		this(nombre, precio, kcal, 0, "");
	}
	public SaborHelado(String nombre, Double precio, Integer kcal, Integer porcentajeGrasa, String tipoEdulcorante) {
		super(nombre, precio, kcal);
		this.porcentajeGrasa = porcentajeGrasa;
		this.tipoEdulcorante = tipoEdulcorante;
	}
	
	//GETTERS AND SETTERES
	
	public Integer getPorcentajeGrasa() {
		return porcentajeGrasa;
	}
	public void setPorcentajeGrasa(Integer porcentajeGrasa) {
		this.porcentajeGrasa = porcentajeGrasa;
	}
	public String getTipoEdulcorante() {
		return tipoEdulcorante;
	}
	public void setTipoEdulcorante(String tipoEdulcorante) {
		this.tipoEdulcorante = tipoEdulcorante;
	}
	@Override
	public  String toString() {
		return super.toString() + ", porcentaje de grasa: " + porcentajeGrasa + ", edulcorante: " + tipoEdulcorante + "\n";
	}
}

/*
 * 4. A�ade al package una clase SaborHelado que herede de Comida y que permita
 * estructurar la informaci�n de distintas bolas de helado, de manera que pueda
 * almacenar:
 * 
 * � % de grasa 
 * � Tipo de edulcorante 
 * 
 * A�ade como m�nimo los siguientes m�todos:
 * 
 * � Constructor que establezca los atributos. 
 * � Sobrescribe toString para que retorne una string con la informaci�n de la bola de helado.
 */