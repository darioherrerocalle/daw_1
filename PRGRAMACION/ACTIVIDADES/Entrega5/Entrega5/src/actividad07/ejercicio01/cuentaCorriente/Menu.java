package actividad07.ejercicio01.cuentaCorriente;

public class Menu {
	//funcion para mostrar el menu
	public static void mostrarMenu(Cuenta[] cuentas) {
		
        boolean salir = false;

        while (!salir) {
            try {
                System.out.println();
                System.out.println("MENU DE OPCIONES");

                String opciones[] = new String[5];
                opciones[0] = "Salir";
                opciones[1] = "Consultar saldo";
                opciones[2] = "Ingresar dinero";
                opciones[3] = "Sacar dinero";
                opciones[4] = "Realizar transferencia";

                for (int i = 0; i < opciones.length; i++) {
                    System.out.println(i + ": " + opciones[i]);
                }
                Integer entrada = Utilidades.pideEntero("Introduce la operacion que quieres realizar: ");

                switch (entrada) {
                    case 0:
                        System.out.println("Hasta pronto!");
                        salir = true;
                        break;
                    case 1:
                        System.out.println("Has seleccionado la opcion 1. Vamos a mostrar las cuentas.");
                        Menu.mostrarCuentas(cuentas);
                        break;
                    case 2:
                        System.out.println("Has seleccionado la opcion 2. Vamos a ingresar dinero.");
                        Menu.ingresarDinero(cuentas);
                        break;
                    case 3:
                        System.out.println("Has seleccionado la opcion 3. Vamos a retirar dinero.");
                        Menu.retirarDinero(cuentas);
                        break;
                    case 4:
                        System.out.println("Has seleccionado la opcion 4. Vamos a realizar una transferencia.");
                        Menu.realizarTransferencia(cuentas);
                        break;
                    default:
                        System.out.println("Solo tengo 5 opciones, lo siento :( ");
                        break;
                }

            } catch (Exception e) {
                System.out.println("Eso no es un n�mero.");
            }
        }
    }

	//METODOS PROPIOS
	
		//funcion para mostrar las cuentas
		public static void mostrarCuentas(Cuenta[] cuentas) { 
			for (int i = 0; i <	cuentas.length; i++) { 
				System.out.println(i + ". " + cuentas[i].toString());
			} 
		}
		//funcion para mostrar el saldo de una cuenta
		public static void mostrarSaldo(Cuenta[] cuentas) {
			
			mostrarCuentas(cuentas);
			
			System.out.println();		
			Integer entrada = Utilidades.pideEntero("Introduce el numero de cuenta cuyo saldo quieres consultar: ");
			
			for (int i = 0; i < cuentas.length; i++) {
				if (cuentas[i].getNumCuenta() == entrada) {
					System.out.println("El saldo de la cuenta " + cuentas[i].getNumCuenta() + " es: " + cuentas[i].getSaldo() + " euros.");
				}
			}
		}
		//funcion para ingresar el dinero en una cuenta
		public static void ingresarDinero(Cuenta[] cuentas) {
			
			mostrarCuentas(cuentas);
			
			System.out.println();		
			Integer entrada = Utilidades.pideEntero("Introduce el numero de cuenta en la que queires realizar el ingreso: ");
			Double ingreso = Utilidades.pideDouble("Introduce la cantidad que quieres ingresar: ");
			
			for (int i = 0; i < cuentas.length; i++) {
				if (cuentas[i].getNumCuenta() == entrada) {
					cuentas[i].ingresarCantidad(ingreso);
					System.out.println("El saldo de la cuenta " + cuentas[i].getNumCuenta() + " es: " + cuentas[i].getSaldo() + " euros.");
				}
			}
		}
		//funcion para retirar dinero
		public static void retirarDinero(Cuenta[] cuentas) {
			
			mostrarCuentas(cuentas);
			
			System.out.println();
			Integer entrada = Utilidades.pideEntero("Introduce el numero de cuenta en la que quieres realizar el extracto: ");
			Double extracto = Utilidades.pideDouble("Introduce la cantidad a retirar: ");
			
			for (int i = 0; i < cuentas.length; i++) {
				if (cuentas[i].getNumCuenta() == entrada) {
					cuentas[i].retirarCantidad(extracto);
					System.out.println("El saldo de la cuenta " + cuentas[i].getNumCuenta() + " es: " + cuentas[i].getSaldo() + " euros.");
				}
			}
		}
		//funcion para realizar una transferencia
		public static void realizarTransferencia(Cuenta[] cuentas) {
			mostrarCuentas(cuentas);
			
			System.out.println();
			Integer origen = Utilidades.pideEntero("Inteoduce el numero de la cuenta origen: ");
			Integer destino = Utilidades.pideEntero("Introduce el numero de la cuenta destino: ");
			Double cantidad = Utilidades.pideDouble("Introduce la cantidad a transferir: ");
			
			for (int i = 0; i < cuentas.length; i++) {
				if (cuentas[i].getNumCuenta() == origen) {
					cuentas[i].retirarCantidad(cantidad);
				}
				if (cuentas[i].getNumCuenta() == destino) {
					cuentas[i].ingresarCantidad(cantidad);
				}
			}
			
			mostrarCuentas(cuentas);
		}
}
