package actividad07.ejercicio01.cuentaCorriente;

import java.util.Scanner;

public class Utilidades {
	public static Integer pideEntero(String peticion) {
        Scanner sc = new Scanner(System.in);
        Integer entrada = 1;
        boolean salir = false;

        do {
            try {
                System.out.println(peticion);
                entrada = sc.nextInt();
                salir = true;

            } catch (Exception e) {
                System.out.println("No has introducido un entero!");
                sc.next();
            }
        } while (salir == false);
        return entrada;
    }
	
	public static Double pideDouble(String peticion) {
        Scanner sc = new Scanner(System.in);
        Double entrada = 0.0;
        boolean salir = false;

        do {
            try {
                System.out.println(peticion);
                entrada = sc.nextDouble();
                salir = true;

            } catch (Exception e) {
                System.out.println("No has introducido un double!");
                sc.next();
            }
        } while (salir == false);
        return entrada;
    }

    public static String pideString(String peticion) {
        Scanner sc = new Scanner(System.in);
        String entrada = "";
        boolean salir = false;

        do {
            try {
                System.out.println(peticion);
                entrada = sc.nextLine();
                salir = true;

            } catch (Exception e) {
                System.out.println("No has introducido un car�cter v�lido!");
                sc.next();
            }
        } while (salir == false);
        return entrada;
    }

    public static boolean validarEntrada(Integer entrada) {
        if (entrada < 0 || entrada > 6) {
            /*System.out.println("Opci�n incorrecta, vuelve a intentarlo");*/
            return true;
        } else return false;
    }
    
}
