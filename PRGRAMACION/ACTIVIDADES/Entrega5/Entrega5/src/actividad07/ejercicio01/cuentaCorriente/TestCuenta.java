package actividad07.ejercicio01.cuentaCorriente;

public class TestCuenta {

	public static void main(String[] args) {
		//instanciamos las cuentas
		Cuenta c1 = new Cuenta(11, 1000.0, "Nose");
		Cuenta c2 = new Cuenta(22, 2000.0, "Rick");
		//creamos un array de cuentas
		Cuenta[] cuentas = new Cuenta[2];
		cuentas[0] = c1;
		cuentas[1] = c2;
		//llamamso a la fn para mostrar el menu
		Menu.mostrarMenu(cuentas);
		
	}

}
