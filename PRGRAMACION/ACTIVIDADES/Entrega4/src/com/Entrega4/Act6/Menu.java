package com.Entrega4.Act6;

import com.Entrega4.IntroduciendoDatos.Pregunta;

import java.io.*;
import java.util.Scanner;

public class Menu {

    /*1.	Nuevo Archivo: debe llamar a una función que pida un nombre de archivo al usuario y
    cree un archivo con el nombre indicado dentro de la carpeta de nombre “archivos” ubicada en
    la carpeta del proyecto, y que rellene el archivo con un texto introducido por el usuario*/

    public static void crearArhivo() throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        String separador = File.separator;
        String rutaProyecto = System.getProperty("user.dir");
        String rutaCarpeta = rutaProyecto + separador + "archivos";

        //System.out.println(rutaProyecto);
        //System.out.println(rutaCarpeta);

        System.out.println("Introduce el nombre del archivo nuevo: ");
        String nombre = br.readLine();

        System.out.println("Introduce el texto del archivo: ");
        String texto = br.readLine();

        String saltoLinea = System.getProperty("linea.separator");
        String nombreFichero = rutaCarpeta + separador + nombre;

        FileWriter fw = new FileWriter(nombreFichero);
        BufferedWriter bw = new BufferedWriter(fw);

        bw.write(texto);
        bw.flush();
        bw.close();
    }

    /*2.	Listar Archivos debe llamar a una función que muestre los nombres de archivos (sin carpetas)
    dentro de la carpeta “archivos” numerados: e.j.: 1-Archivo1.txt 2-Archivo2.txt
    y retorne un array con las rutas de los archivos.*/

    public static String[] listarArchivos() throws IOException {
        String separador = File.separator;
        String rutaProyecto = System.getProperty("user.dir");
        String rutaCarpeta = rutaProyecto + separador + "archivos";

        File ruta = new File(rutaCarpeta);
        //System.out.println(ruta.getAbsolutePath());

        String[] array_archivos = ruta.list();
        for (int i = 0; i < array_archivos.length; i++) {
            System.out.println(i + 1 + ". " + array_archivos[i]);
        }

        return array_archivos;
    }

    /*3.	Muestra un Archivo debe de llamar a ListarArchivos para mostrar los archivos disponibles y
    permitir al usuario elegir qué documento quiere ver según su número y
    mostrar el contenido del documento por consola*/

    public static void mostrarArchivos() throws IOException {
        String[] numArchivo = listarArchivos();

        int eleccion = Pregunta.pideEntero("Introduce la posición del archivo que quieres leer: ");
        eleccion--;

        String separador = File.separator;
        String rutaProyecto = System.getProperty("user.dir");
        String rutaCarpeta = rutaProyecto + separador + "archivos";
        String saltoLinea = System.getProperty("linea.separator");
        String nombreFichero = rutaCarpeta + separador + numArchivo[eleccion];

        FileReader ruta = new FileReader(nombreFichero);

        int c = 0;

        while (c != -1) {
            c = ruta.read();
            char letra = (char) c;
            System.out.print(letra);
        }
        ruta.close();
        System.out.println();
    }

    /*4.	Borrar un Archivo: debe llamar a una función que muestre los archivos dentro de la
    carpeta “archivos” numerados y permitir al usuario elegir qué documento quiere borrar según su número*/

    public static void borrarArchivos() throws IOException {
        String[] numArchivo = listarArchivos();

        int eleccion = Pregunta.pideEntero("Introduce la posición del archivo que quieres borrar: ");
        eleccion--;

        String separador = File.separator;
        String rutaProyecto = System.getProperty("user.dir");
        String rutaCarpeta = rutaProyecto + separador + "archivos";
        String saltoLinea = System.getProperty("linea.separator");
        String nombreFichero = rutaCarpeta + separador + numArchivo[eleccion];

        File ruta = new File(nombreFichero);
        if (ruta.delete()) {
            System.out.println("Archivo eliminado.");
        } else {
            System.out.println("No ha sido posible eliminar el archivo.");
        }
    }

    /*5.	Renombrar un Archivo: debe mostrar los archivos dentro de la carpeta “archivos” numerados
     y permitir al usuario elegir qué documento quiere renombrar según su número. A continuación,
     le pregunte el nuevo nombre y lo renombre si es válido. Si es un nombre inválido se debe mostrar
     un mensaje por consola al usuario y volver a ejecutar el menú*/

    public static void renombrarArchivos() throws IOException {
        String[] numArchivo = listarArchivos();
        int eleccion = Pregunta.pideEntero("Introduce la posición del archivo que quieres renombrar: ");
        eleccion--;
        String nuevoNombre = Pregunta.pideString("Renombra el archivo: ");

        String separador = File.separator;
        String rutaProyecto = System.getProperty("user.dir");
        String rutaCarpeta = rutaProyecto + separador + "archivos";
        String saltoLinea = System.getProperty("linea.separator");

        String nombreFichero = rutaCarpeta + separador + numArchivo[eleccion];
        String nuevoFichero = rutaCarpeta + separador + nuevoNombre;

        File viejaRuta = new File(nombreFichero);
        File nuevaRuta = new File(nuevoFichero);

        if(viejaRuta.renameTo(nuevaRuta)){
            System.out.println("Archivo renombrado.");
        }else{
            System.out.println("Lo siento, no he podido renombrar el archivo");
        }

    }

    /*6.	Reemplazar caracteres de un Archivo: debe de llamar a ListarArchivos para permitir al usuario elegir
    qué documento se quiere modificar según su número y a continuación pida qué carácter se quiere reemplazar y
    por qué nuevo carácter. Si el documento no existe o es inválido, se debe mostrar un mensaje por consola al
    usuario y volver a ejecutar el menú. Es preferible el uso de RandomAccessFile*/

    public static void reemplazarArchivos() throws IOException {
        String[] numArchivo = listarArchivos();
        int eleccion = Pregunta.pideEntero("Introduce la posición del archivo que quieres borrar: ");
        eleccion--;

        String separador = File.separator;
        String rutaProyecto = System.getProperty("user.dir");
        String rutaCarpeta = rutaProyecto + separador + "archivos";
        String saltoLinea = System.getProperty("linea.separator");
        String nombreFichero = rutaCarpeta + separador + numArchivo[eleccion];

        File ruta = new File(numArchivo[eleccion]);
        FileWriter fw = new FileWriter(nombreFichero);
        String texto = Pregunta.pideString("Introduce el nuevo texto: ");
        fw.write(texto);
        fw.close();
    }

    public static void mostrarMenu() {

        boolean salir = false;

        while (!salir) {
            try {
                System.out.println();
                System.out.println("MENU DE OPCIONES");

                String opciones[] = new String[7];
                opciones[0] = "Salir";
                opciones[1] = "Nuevo archivo";
                opciones[2] = "Listar archivos";
                opciones[3] = "Mostrar archivos";
                opciones[4] = "Borrar archivos";
                opciones[5] = "Renombrar archivos";
                opciones[6] = "Reemplazar caracteres archivos";

                for (int i = 0; i < opciones.length; i++) {
                    System.out.println(i + ": " + opciones[i]);
                }
                Integer entrada = Pregunta.pideEntero("Introduce la operacion que quieres realizar: ");

                switch (entrada) {
                    case 0:
                        System.out.println("Hasta pronto!");
                        salir = true;
                        break;
                    case 1:
                        System.out.println("Has seleccionado la opcion 1. Vamos a crear un nuevo archivo.");
                        Menu.crearArhivo();
                        break;
                    case 2:
                        System.out.println("Has seleccionado la opcion 2. Vamos a listar los archivos.");
                        Menu.listarArchivos();
                        break;
                    case 3:
                        System.out.println("Has seleccionado la opcion 3. Vamos a mostrar un archivo.");
                        Menu.mostrarArchivos();
                        break;
                    case 4:
                        System.out.println("Has seleccionado la opcion 4. Vamos a borrar un archivo.");
                        Menu.borrarArchivos();
                        break;
                    case 5:
                        System.out.println("Has seleccionado la opcion 5. Vamos a renombrar un archivo.");
                        Menu.renombrarArchivos();
                        break;
                    case 6:
                        System.out.println("Has seleccionado la opcion 6. Vamos a reemplazar el contenido de un archivo.");
                        Menu.reemplazarArchivos();
                        break;
                    default:
                        System.out.println("Solo tengo 6 opciones, lo siento :( ");
                        break;
                }

            } catch (Exception e) {
                System.out.println();
            }
        }
    }
}
