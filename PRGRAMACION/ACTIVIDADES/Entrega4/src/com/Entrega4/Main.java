package com.Entrega4;

import com.Entrega4.Act6.Menu;
import com.Entrega4.IntroduciendoDatos.Pregunta;

import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {

            try {
               Menu.mostrarMenu();
            } catch (Exception e) {
                System.out.println("Vuelve a intentarlo.");
            }
    }
}
