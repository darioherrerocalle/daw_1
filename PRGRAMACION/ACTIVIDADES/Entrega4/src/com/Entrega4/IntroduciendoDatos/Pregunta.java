package com.Entrega4.IntroduciendoDatos;

import java.util.Scanner;

public class Pregunta {
    public static Integer pideEntero(String peticion) {
        Scanner sc = new Scanner(System.in);
        Integer entrada = -1;

        do {
            try {
                System.out.println(peticion);
                entrada = sc.nextInt();

            } catch (Exception e) {
                System.out.println("No has introducido un entero!");
                sc.next();
            }
        } while (entrada == -1);
        return entrada;
    }

    public static String pideString(String peticion) {
        Scanner sc = new Scanner(System.in);
        String entrada = "";

        do {
            try {
                System.out.println(peticion);
                entrada = sc.nextLine();

            } catch (Exception e) {
                System.out.println("No has introducido un carácter válido!");
                sc.next();
            }
        } while (entrada == "");
        return entrada;
    }

    public static boolean validarEntrada(Integer entrada) {
        if (entrada < 0 || entrada > 6) {
            /*System.out.println("Opción incorrecta, vuelve a intentarlo");*/
            return true;
        } else return false;
    }
}
