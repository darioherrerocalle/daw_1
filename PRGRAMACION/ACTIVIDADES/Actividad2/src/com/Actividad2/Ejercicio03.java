package com.Actividad2;

import jdk.swing.interop.SwingInterOpUtils;

import java.util.Scanner;

public class Ejercicio03 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String entrada;
        String[][] puntosPeli = new String[3][5];

        puntosPeli[0][0] = "ET";
        puntosPeli[0][1] = "8";
        puntosPeli[0][2] = "5";
        puntosPeli[0][3] = "10";
        puntosPeli[0][4] = "4";

        puntosPeli[1][0] = "Sharknado";
        puntosPeli[1][1] = "3";
        puntosPeli[1][2] = "8";
        puntosPeli[1][3] = "9";
        puntosPeli[1][4] = "7";

        puntosPeli[2][0] = "Godzilla";
        puntosPeli[2][1] = "9";
        puntosPeli[2][2] = "7";
        puntosPeli[2][3] = "6";
        puntosPeli[2][4] = "8";

        /*IMPRESION POR PANTALLA DE TODAS LAS PUNTUACIONES*/

        System.out.println("IMPRESION POR PANTALLA DE TODAS LAS PUNTUACIONES");
        for (int i = 0; i < puntosPeli.length; i++) {
            for (int j = 0; j < puntosPeli[i].length; j++) {
                if (j == 0) {
                    System.out.println("Pelicula: " + puntosPeli[i][j]);
                } else {
                    System.out.println("Puntuacion " + j + ": " + puntosPeli[i][j]);
                }
            }
        }
        System.out.println();

        /*TODAS LAS NOTAS DE LA PRIMERA PELICULA*/

        System.out.println("TODAS LAS NOTAS DE LA PRIMERA PELICULA");
        for (int j = 0; j < puntosPeli[0].length; j++) {
            if (j == 0) {
                System.out.println("Pelicula: " + puntosPeli[0][j]);
            } else {
                System.out.println("Puntuacion " + j + ": " + puntosPeli[0][j]);
            }
        }
        System.out.println();

        /*MEDIA SEGUNDA PELICULA*/

        System.out.println("MEDIA SEGUNDA PELICULA");
        double total;
        double media;
        int pos = 1;
        total = 0;
        media = 0;
        for (int j = 0; j < puntosPeli[pos].length; j++) {
            if (j == 0) {
                System.out.println("La puntuacion media de la segunda pelicula " +
                        puntosPeli[pos][j] +
                        " es: ");
            } else {
                total += Double.parseDouble(puntosPeli[pos][j]);
            }
        }
        media = total / 4;
        System.out.println(String.format("%.2f", media));
        System.out.println();


        /*VALOR MAS ALTO DE LA TERCERA PELICULA*/

        System.out.println("VALOR MAS ALTO DE LA TERCERA PELICULA");

        int pos2 = 2;
        double notaMax = -1;

        for (int i=1; i<puntosPeli[pos2].length; i++){
            if (Double.parseDouble(puntosPeli[pos2][i]) > notaMax){
                notaMax = Double.parseDouble(puntosPeli[pos2][i]);
            }
        }
        System.out.println("La puntuacion maxima de la tercera pelicula " +
                puntosPeli[2][0] +
                " es: " +
                notaMax);

    }
}





/*
3.Ejercicio03.java:escribe un programa que almacene en un array bidimensional cuatro puntuaciones
        de tres películas con los valores mostrados en el esquema mostrado después del enunciado.
        Seguidamente se mostrará por consola todos los valores almacenados en el array
        y los siguientes resultados(calculados según los valores del array):
        •    Todas las notas de la primera película.
        •    La media de la segunda película.
        •    El valor más alto de la tercera película.
        “E.T.”
        “8”
        “5”
        “10”
        “4”
        “Sharknado”
        “3”
        “8”
        “9”
        “7”
        “Godzilla”
        “9”
        “7”
        “6”
        “8”*/
