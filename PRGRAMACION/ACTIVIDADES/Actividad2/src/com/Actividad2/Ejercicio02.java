package com.Actividad2;

import java.util.Arrays;
import java.util.Scanner;

public class Ejercicio02 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String entrada;
        boolean salir = false;
        int miArray[] = {9, 8, 7, 6, 5};

        do {
            System.out.println("MENU DE OPCIONES \n" +
                    "0. Finalizar \n" +
                    "1. Modificar el valor almacenado en una posicion \n" +
                    "2. Mostrar el resultado de sumar todos sus numeros \n" +
                    "3. Mostrar el mayor y menor numero del array \n" +
                    "4. Ordenar el array, primero pares y luego impares \n");

            entrada = sc.next();

            switch (entrada) {
                case "0":
                    salir = true;
                    break;

                case "1":

                    try {

                        int aux = 0;
                        boolean tryagain = true;

                        do {
                            System.out.println("Introcude la posicion del array: ");
                            aux = sc.nextInt();

                            if (aux >= 0 && aux <= 100) {

                                System.out.println("Introcude un valor para esa posicion: ");
                                miArray[aux] = sc.nextInt();

                                tryagain = false;

                            } else {
                                System.out.println("El valor introducido debe ser entre 0 y 100!");
                                tryagain = true;
                            }
                        } while (tryagain == true);
                    } catch (Exception e) {
                        System.out.println("valor introducido incorrecto");
                        sc.next();
                    }

                    System.out.println("Los valores de array son: ");
                    for (int i = 0; i < miArray.length; i++) {
                        System.out.println(miArray[i]);
                    }
                    break;

                case "2":
                    int suma = 0;
                    for (int i = 0; i < miArray.length; i++) {
                        suma += miArray[i];
                    }
                    System.out.println("La suma de los valores de array es: " + suma);
                    break;

                case "3":
                    Arrays.sort(miArray);
                    System.out.println("El mayor numero del array es: " +
                            miArray[4] +
                            "\ny el menor numero del array es: " +
                            miArray[0]);

                    break;

                case "4":
                    int pares[] = new int[5];
                    int impares[] = new int[5];
                    int a = 0, b = 0;

                    for (int i = 0; i < miArray.length; i++) {
                        if ((miArray[i] % 2) == 0) {
                            pares[a] = miArray[i];
                            a++;
                        } else {
                            impares[b] = miArray[i];
                            b++;
                        }
                    }

                    System.out.print("El array de pares es: \n");
                    for (int j = 0; j < pares.length; j++) {
                        if (pares[j] != 0) {
                            System.out.println(pares[j]);
                        }
                    }

                    System.out.print("El array de impares es: \n");
                    for (int k = 0; k < impares.length; k++) {
                        if (impares[k] != 0) {
                            System.out.println(impares[k]);
                        }
                    }

                    break;

                default:
                    System.out.println("Opcion incorrecta");
                    break;

            }

        } while (!salir);
    }
}



/*2.	Ejercicio02.java: escribe un programa que pida al usuario 5 valores decimales del 0 al 100.
Se deben almacenar en un array y mostrarlos por consola.
Si el usuario introduce un valor incorrecto, se ha de volver a pedir.
Mediante un SWITCH, el programa debe mostrar el siguiente mensaje al usuario:
Introduzca la operación a realizar del siguiente menú de opciones:
1-	Modificar el valor almacenado en una posición.
2-	Mostrar el resultado de sumar todos los números
3-	Mostrar el número más alto y más bajo.
4-	Ordena el array situando primero todos los números pares y luego los impares
0-Finalizar
Cada vez que se realice la operación 1, 2, 3 y 4 se ha de mostrar por pantalla los valores almacenados en el array.
Controla mediante un DO WHILE que una vez realizada la operación seleccionada se vuelva a mostrar el menú de operaciones
excepto si el usuario ha introducido un 0. Si se introduce un valor menor a 0,
indica que se ha introducido un valor incorrecto y vuelve a mostrar el menú.

Si el usuario selecciona la opción 1 el programa debe pedir al usuario una posición de array válida
y un valor numérico entre 0 y 100 para situarlo en la posición indicada.
Si el usuario selecciona la opción 2 el programa debe sumar todos los números almacenados en el array y mostrar el resultado.
Si el usuario selecciona la opción 3 el programa debe mostrar el número más alto y el más bajo almacenados en el array.
Si el usuario selecciona la opción 4 el programa debe ordenar el array teniendo en cuenta solo si son pares o impares
(primero los pares y luego los impares) , no importa que un valor sea mayor o menor.
*/