import java.io.File;

public class Ficheros {
	public static void main(String[] args) throws IOException {
		//obtenemos la ruta absoluta al proyecto del sistema
		String rutaProyecto = System.getProperty("user.dir");
		System.out.println(rutaProyecto);
		//obtenemos el separador del sistema, cambia de un so a otro
		String separador = File.separator;
		//creamos la ruta de la carpeta agnadiendo el separador y el nombre
		//esto no crea la carpeta todavia
		String rutaCarpeta = rutaProyecto + separador + "ejemplo";
		System.out.println(rutaCarpeta);
		//la variable file nos permite operar sobre esa ruta
		File carpeta = new File(rutaCarpeta);
		//ahora creamos el directorio
		carpeta.mkdir();
		//ahora creamos un archivo dentro del directorio
		String rutaArchivo = rutaCarpeta + separador + "archivo.txt";
		File archivo = new File(rutaArchivo);
		archivo.createNewFile();	
		
		throw IOException("mensaje");
	}
}