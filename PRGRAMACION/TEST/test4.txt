¿Qué indica la palabra static acompañando a un atributo de una clase?

Select one:
Answers
a. Un atributo no puede tener la propiedad static. Es solo para métodos
b. El valor de ese atributo no cambiará nunca
c. Se crea una instancia de ese atributo para cada objeto, pero todos comparten el mismo valor
d. Se va a crear para esa clase solo una instancia de ese atributo

¿Cómo debemos declarar una variable única común para todos los objetos?

Select one:
Answers
a. Como variable estática
b. Los objetos no pueden compartir una variable única
c. Como constante
d. Como variable global

¿Qué indica la palabra static acompañando a un atributo de una clase?

Select one:
Answers
a. Un atributo no puede tener la propiedad static. Es solo para métodos
b. Se crea una instancia de ese atributo para cada objeto, pero todos comparten el mismo valor
c. Se va a crear para esa clase solo una instancia de ese atributo
d. El valor de ese atributo no cambiará nunca

¿El paso de objetos como parámetros se hacen por referéncia o por valor?

Select one:
Answers
a. Por referencia solo cuando los objetos sean static.
b. Por valor siempre.
c. Por valor solo cuando los objetos contengan primitivas.
d. Por referencia siempre.

¿Qué tipo de retorno asignaremos a un método constructor?

Select one:
Answers
a. El objeto creado
b. ninguno
c. booleano
d. int

¿Como mínimo un Setter cuandos parámetros suele recibir?

Select one:
Answers
a. uno pero de tipo Array siempre
b. dos
c. uno
d. ninguno

¿Cómo se llaman las funciones encargadas de establecer el valor de un atributo?

Select one:
Answers
a. Getters
b. Filters
c. Setters
d. Wrappers

¿Qué palabra clave nos permite acceder a la implementación de un método en la superclase?

Select one:
Answers
a. final
b. father
c. super
d. static

Si quiero que un método de una clase sea solo accedido por clases del mismo paquete, ¿qué nivel de acceso especificaré?

Select one:
Answers
a. No especificado
b. public
c. private
d. protected

¿Cuál de las siguientes afirmaciones es falsa?

Select one:
Answers
a. En Java una clase puede tener más de una superclase
b. Los programadores no tienen que saber nada de los datos de las clases ni del interior de sus métodos, sólo deben conocer su interfaz
c. En Java podemos simular la herencia múltiple utilizando interfaces
d. En la POO se abstraen las características de los objetos del programa y se crean las clases con sus atributos y sus métodos

¿Qué palabra reservada de Java tenemos que poner en una clase que implemente una interfaz?

Select one:
Answers
a. implements
b. extends
c. interface
d. public

¿Cuál de las siguientes afirmaciones es cierta?

Select one:
Answers
a. una interfaz siempre ha de retornar un valor del tipo String
b. en una interfaz se pueden implementar funciones.
c. una clase puede implementar varias interfaces
d. una clase que implemente una interfaz puede o no implementar las funciones definidas en la interfaz.

¿Cuál de las siguientes afirmaciones es cierta?

Select one:
Answers
a. una clase que implemente una interfaz debe implementar las funciones definidas en la interfaz.
b. una interfaz siempre ha de retornar un valor del tipo String
c. en una interfaz se pueden implementar funciones.
d. una clase no puede implementar varias interfaces a la vez.

¿Cuál de las siguientes afirmaciones es cierta?

Select one:
Answers
a. una clase abstracta solo puede implementar una única función.
b. en una clase abstracta todas sus funciones han de ser abstractas.
c. en una clase abstracta se pueden implementar funciones.
d. una clase puede extender de múltiples clases abstractas.

¿Cuál de las siguientes afirmaciones es cierta?

Select one:
Answers
a. una clase no puede implementar varias interfaces a la vez.
b. en una interfaz no se pueden implementar funciones.
c. "la palabra clave para implementar una interfaz es ""extends"""
d. una clase que implemente una interfaz puede o no implementar las funciones definidas en la interfaz.

¿Cuál de las siguientes afirmaciones es falsa?

Select one:
Answers
a. Java genera por defecto un constructor para cada clase.
b. El método constructor se ejecuta automáticamente al crear un objeto.
c. Una clase como máximo sólo tendrá un método constructor.
d. El método constructor sirve para inicializar el objeto.

¿Qué nombre recibe la propiedad de la POO que permite impedir el acceso a ciertos métodos en una clase.?

Select one:
Answers
a. Sobrecarga
b. Polimorfismo
c. Encapsulamiento
d. Herencia

¿Qué es una clase?

Select one:
Answers
a. Es una instancia de los objetos.
b. Es el molde del cual se generan los objetos.
c. Es un contenedor exclusivo de métodos.
d. Es un valor específico de un objeto.

¿Cuál de las siguientes afirmaciones es falsa?

Select one:
Answers
a. Un método no static puede acceder a miembros static y no static
b. Una clase como máximo sólo tendrá un método static
c. Un método static no puede acceder a miembros que no sean static
d. Los métodos static no tienen referencia this

¿Cuál de los siguientes NO es un beneficio que proporciona la programación orientada a objetos?

Select one:
Answers
a. Centralización del código en un único archivo
b. Reutilización de código
c. Facilidad de testeo y reprogramación
d. Estructuración del código

¿De qué clase heredan todas las clases sus métodos?

Select one:
Answers
a. Class
b. String
c. Path
d. Object

¿Cómo se llaman las funciones encargadas de establecer el valor de un atributo?

Select one:
Answers
a. Setters
b. Getters
c. Wrappers
d. Filters

¿Qué palabra reservada de Java tenemos que poner en una clase que herede los atributos y métodos de otra?

Select one:
Answers
a. extends
b. public
c. protected
d. implements

¿Cuál de las siguientes afirmaciones es falsa sobre la sobreescritura de métodos?

Select one:
Answers
a. El método sobrescrito debe devolver el mismo tipo de dato que el método padre
b. El método sobrescrito debe tener la misma lista de argumentos que el método padre
c. El método sobrescrito debe tener el mismo nombre que el método padre
d. El método sobrescrito debe tener las mismas instrucciones que el método padre

Qué método nos permite transformar una clase a String?

Select one:
Answers
a. transform()
b. clone()
c. toString()
d. json()

¿Cuál de las siguientes afirmaciones es cierta?

Select one:
Answers
a. "la palabra clave para extender de una clase abstracta es ""implements"""
b. una clase abstracta solo puede implementar una única función.
c. en una clase abstracta todas sus funciones han de ser abstractas.
d. una clase solo puede extender de una única clase abstracta.

¿Cuál de las siguientes afirmaciones es cierta?

Select one:
Answers
a. una clase abstracta puede tener o algunas funciones abstractas y otras no.
b. una clase puede extender de múltiples clases abstractas.
c. "la palabra clave para extender de una clase abstracta es ""implements"""
d. una clase no abstracta puede definir una función abstracta sin implementarla.

¿Cuál de las siguientes afirmaciones es cierta?

Select one:
Answers
a. en una interfaz no se pueden implementar funciones.
b. una clase no puede implementar varias interfaces a la vez.
c. una clase que implemente una interfaz puede o no implementar las funciones definidas en la interfaz.
d. "la palabra clave para implementar una interfaz es ""extends"""

