package linkia.clase1;

public class Ejemplo1 {

    //psvm

    public static void main(String[] args) {
       
        // comentario

            /*
           comentarios de varias líneas con 

           control+mayúscula + /
             */

         // control + /  --> para comentar una linea
           

        //sout
        System.out.println("hola mundo");
        /***********************************************************************/
        //variables y tipos de datos
        /***********************************************************************/
        
         // de -128 a 127  
         byte num=100;//da error si pones 1000

        //Enteros
        int numero1 = 3; //si no la inicializas da error
        System.out.println(numero1);
        System.out.println("el valor de la variable es: "+numero1);

        //Con decimales
        double numero2= 14.5;
        System.out.println(numero2);

        //Texto
        String mensaje = "Hola ";
        String nombre = "Paz";
        System.out.println(mensaje + nombre);

        int edad = 87;
        System.out.println(nombre + " tiene " + edad + " años");

        //Caracteres
        //char caracter1= "a"; error por las comillas
        char caracter1 = 'a';
        System.out.println(caracter1);

        if (caracter1 == 'a') {  //con comillas "b" da error
            System.out.println("es igual a 'a'");
        }

        //boolean
        boolean procesoCompletado = false;
        System.out.println(procesoCompletado);

        if (procesoCompletado==true){
            System.out.println("Se completó el proceso, enhorabuena!");
        }

        //operaciones con números
        int varint1 = 4;
        double varDob1 = 5.9;
        System.out.println(varint1 + varDob1);


        //conversiones automáticas - Implícitas
        double varDob2 = varint1;
        System.out.println(varDob2);

        //casting - Explícita
        int varInt2 = (int) varDob1; //sin el casting da error
        System.out.println(varInt2);


        //cálculos sin guardar en variables --- Paréntesis!!! si no, concatena
        System.out.println("sumando: "+ (varint1 + varDob1));


        //calculos guardados en variables
        varint1 =5;
        varint1 = varint1 + 10;
        System.out.println("más 10: "+varint1);
        varint1 = 5;
        varint1 += 10;
        System.out.println("más 10: "+varint1);

        // 15-3
        varint1-=3;
        System.out.println(varint1);

        //12*2
        varint1*=2;
        System.out.println(varint1);

        varint1 =5;
        varint1/=2; //  /=0;
        System.out.println(varint1);
        //excepciones en cálculos aritméticos
        //System.out.println(var1/0);


        varint1 =5;
        varint1%=3;
        System.out.println(varint1);

        //Operadores lógicos
        boolean mayor16 = true;
        boolean esoAprobada = true;
        System.out.println("Pruebas and:"+ (mayor16 && esoAprobada));

        System.out.println("pruebas or: "+ (mayor16 || esoAprobada));

        //operadores unitarios
        System.out.println(mayor16);
        mayor16 = !mayor16;
        System.out.println(mayor16);

        varint1 = 8;
        varint1++;
        //++varint1;
        System.out.println(varint1);

        varint1 = 8;
        System.out.println(++varint1);
        System.out.println(varint1++);

        //Tipos primitivos -- Clases  --> Ejercicio 3!
        int minValor = Integer.MIN_VALUE;
        int maxValue =Integer.MAX_VALUE;
        System.out.println(minValor);
        System.out.println(maxValue);


    }
}


