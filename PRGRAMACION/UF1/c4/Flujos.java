package programacion.curso;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class Flujos {

    public static void main(String[] args) throws IOException {

        /***** comparar valores:
         *  tipos de datos primitivos contra Clases Wrapers
         *  Comparar con == y con equals() * **/
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String entrada;
        String estado = "casado";
        int var1 = 4;
        int var2 = 4;
        //probar ahora con:
        //Integer var1 = 4;
        //Integer var2 = 4;

        // esto funciona con int y con Integer
        if (var1 == var2)
            System.out.println("son iguales");

        System.out.println("Introduce un número: ");
        entrada = br.readLine();

        var1 = Integer.parseInt(entrada);
        System.out.println("Introduce otro número: ");
        entrada = br.readLine();
        var2 = Integer.parseInt(entrada);

        if (var1 == var2)
            System.out.println("has introducido el mismo número");


      /** problemas con double. comparaciones == y equals()**/

        System.out.println("Introduce un double: ");
        entrada = br.readLine();

        double var3 = Double.parseDouble(entrada);

        System.out.println("Introduce otro double: ");
        entrada = br.readLine();

        double var4 = Double.parseDouble(entrada);

        if (var3 == var4)
            System.out.println("has introducido el mismo número");

        System.out.println("Introduce un double: ");
        entrada = br.readLine();

       /* Double var3 = Double.parseDouble(entrada);

        System.out.println("Introduce otro double: ");
        entrada = br.readLine();

        Double var4 = Double.parseDouble(entrada);

        if (var3 == var4) {
            System.out.println("has introducido el mismo número ==");
        }


        if (var3.equals(var4)) {
            System.out.println("has introducido el mismo número equals()");
        }*/

      /* problema con el String*/

        System.out.println("Introduce tu estado civil: ");
        entrada = br.readLine();

        if (entrada == "casado") {
            System.out.println("Estas casado ==");
        }

        if (entrada.equals("casado")) {
            System.out.println("Estas casado equals()");
        }

        /* con scanner pasa lo mismo*/
         Scanner scanner = new Scanner(System.in);
        String entrada2;
        System.out.println("introduce tu estado:");
        entrada2 = scanner.nextLine();
        if (entrada2 =="viudo"){
            System.out.println("Te corresponde la pensión de viudedad");
        }

        if (entrada2.equals("viudo")){
            System.out.println("Te corresponde la pensión de viudedad -- equals()");
        }


        /* If - Else */
        if (entrada.equals("casado")) {
            System.out.println("Estas casado equals()");
        } else {
            System.out.println(" otro estado civil");
        }





        //comparaciones y paréntesis
        int num1 = 61;
        if ((num1 >= 50 && num1 <= 150) || num1 != 60) {
            System.out.println("opción if");
        } else {
            System.out.println("opción else");
        }



        //ejemplos de operador ternario
        int edad = 19;
        boolean mayorEdad = (edad >= 18) ? true : false;

        if (mayorEdad)
            System.out.println("Es mayor");
        else
            System.out.println("Es menor");

        boolean carnetConducir = mayorEdad ? true : false;
        System.out.println(carnetConducir);


//varios valores en if - else - if 
    boolean semaforoVerde = true;
        if (semaforoVerde == true) { // if (semaforo) también funcionaria
            // if (!semaforo) para negar
            System.out.println("pasa");
        } else {
            System.out.println("no pases");
        }

        System.out.println("de qué color está el semáforo?");
        entrada = br.readLine();

        if (entrada.equals("rojo")) {
            System.out.println("no puedes pasar");
        } else {
            System.out.println("puedes pasar");
        }

        // con if y con switch
        System.out.println("introduce el color del semáforo:");
        entrada = scanner.nextLine();
        if (entrada.equals("rojo")) {
            System.out.println("No pasar");
        } else if (entrada.equals("verde")){
            System.out.println("puedes pasar");
        } else {
            System.out.println("mira antes de pasar");
        }

        switch (entrada) {
            case "rojo":
                System.out.println("no puedes pasar");
                break;
            case "verde":
                System.out.println("puedes pasar");
                break;
            case "naranja":
                System.out.println("mira antes de cruzar");
                break;
        }

        if (entrada.equals("casado")) {
            System.out.println("Estas casado equals()");
        } else if (entrada.equals("soltero")) {
            System.out.println(" Estás soltero");
        } else if (entrada.equals("divorciado")) {
            System.out.println("estás divorciado");
        } else {
            System.out.println(" pues será otro...");
        }

        //Scanner scanner = new Scanner(System.in);
        //String entrada;
        double ayuda;

        System.out.println("Introduce tu estado civil:");
        entrada = scanner.nextLine();
        switch (entrada) {
            case "soltero":
                ayuda = 500;
                break;
            case "casado":
                ayuda = 250;
                break;
            case "viudo":
                ayuda = 1000;
                break;
            default:
                ayuda = 0;
                break;

        }

        System.out.println("la ayuda que te corresponde es de : " + ayuda + "€");


        // con valor entero
        int opcion = 1;
        switch (opcion) {
            case 1:
                System.out.println("es uno");
                break;
            case 2:
                System.out.println("es un dos");
                break;
            default:
                System.out.println("no es ni uno ni dos");
        }

    }
}