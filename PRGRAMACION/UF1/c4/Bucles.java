package programacion.curso;

import java.util.Scanner;

public class Bucles {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String entrada;
        double ayuda;
        boolean parar = false;

// do
        do {
            System.out.println("Introduce tu estado civil:");
            entrada = scanner.nextLine();
            switch (entrada) {
                case "soltero":
                    ayuda = 500;
                    break;
                case "casado":
                    ayuda = 250;
                    break;
                case "viudo":
                    ayuda = 1000;
                    break;
                default:
                    ayuda = 0;
                    parar = true;
                    break;

            }

            System.out.println("la ayuda que te corresponde es de : " + ayuda + "€");
            System.out.println("************************************************");
        } while (parar == false);  //   (!parar);


        // while
        while (parar == false) {
            System.out.println("Introduce tu estado civil:");
            entrada = scanner.nextLine();
            switch (entrada) {
                case "soltero":
                    ayuda = 500;
                    break;
                case "casado":
                    ayuda = 250;
                    break;
                case "viudo":
                    ayuda = 1000;
                    break;
                default:
                    ayuda = 0;
                    parar = true;
                    break;

            }

            System.out.println("la ayuda que te corresponde es de : " + ayuda + "€");
            System.out.println("************************************************");
        }

//for
        for (int i = 0; i < 10; ++i) {
            System.out.println("la variable i vale: " + i);
        }
/****************************************************************/
        // leer 4 número por consola y sumar el resultado
/****************************************************************/
        //Scanner scanner = new Scanner(System.in);
        int contador = 0;
        int total = 0;
        int numero = 0;
        boolean esEntero;

        do {
            try {
                System.out.println("introduce un número entero");
                numero = scanner.nextInt();
                total = total + numero;
                contador++;
            } catch (Exception e) {
                System.out.println(" has introducido un valor incorrecto. Solo enteros");
                scanner.nextLine();
            }
        } while (contador < 4);
        System.out.println("he leido: " + contador + " números");
        System.out.println("La suma es:" + total);

/********************** usando hasNextInt() versión 1********************************/
     //   Scanner scanner = new Scanner(System.in);
     //   int contador = 0;
     //   int total = 0;
     //   int numero = 0;
     //   boolean esEntero;

        do {

            System.out.println("introduce un número ");
            esEntero = scanner.hasNextInt();
            if (esEntero) {
                numero = scanner.nextInt();
                total += numero;
                contador++;
            } else {
                scanner.next();
                System.out.println(" No has introducido un entero. Vuelve a intentarlo");
            }

        } while (contador < 4);

        System.out.println(contador);
        System.out.println(total);


/********************** usando hasNextInt() versión 2********************************/


        //Scanner scanner = new Scanner(System.in);
        //int contador = 1, total = 0, numero = 0;
        do {
            System.out.println("introduce el número " + contador + "º");
            boolean isInt = scanner.hasNextInt();
            if (isInt) {
                numero = scanner.nextInt();
                total += numero;
                contador++;
            }
            scanner.nextLine();
        } while (contador <= 10);
        System.out.println("el total es: " + total);

    }
}
