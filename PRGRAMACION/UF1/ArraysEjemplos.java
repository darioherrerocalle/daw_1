package programacion.curso;

import java.util.Scanner;

public class ArraysEjemplos {

    public static void main(String[] args) {


        /******ejemplo de Array de enteros ***/
        //declararlo
        Integer[] miArray;
        // crearlo
        miArray = new Integer[5];
        //rellenamos manualmente por código
        miArray[0] = 15;
        miArray[1] = 3;
        miArray[2] = 10;
        miArray[3] = 6;
        miArray[4] = 2;

        // mostramos por pantalla
        for (int i = 0; i < miArray.length; i++) {
            System.out.println(miArray[i]);
        }
        //rellenamos en un bucle desde teclado
        Scanner scanner = new Scanner(System.in);
        for (int i = 0; i < miArray.length; i++) {
            miArray[i] = scanner.nextInt();
        }

        // mostramos por pantalla -- otra forma
        for (int num: miArray) {
            System.out.println(num);
        }

       // miArray.for
       // miArray.fori
       // miArray.forr

        /******ejemplo de Array de String ***/
        String[] nombres;
        nombres = new String[4];
        //rellenamos el array
        nombres[0] = "Kim";
        nombres[1] = "John";
        nombres[2] = "Cris";
        nombres[2] = "Kay";
        //recorremos el array con un for loop
        for (String nombre : nombres) {
            System.out.println(nombre);
        }

        /****** un String es un Array de carácteres realmente *******/
        char a = 'a';
        String cadena = "Paz ha ido a comprar naranjas";
        int contador = 0;

        for (int i = 0; i < cadena.length(); i++) {
            if (cadena.charAt(i) == a) {
                System.out.println("he encontrado una: " + a);
                contador++;
            }
        }

        System.out.println("En el texto: " + cadena + " tenemos: " + contador + " letras a");






    }
}
