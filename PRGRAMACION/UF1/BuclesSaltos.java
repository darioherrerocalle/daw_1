package programacion.curso;

public class BuclesSaltos {

    public static void main(String[] args) {
        String mensaje;
        /*********************************************************************/
        // break en bucle for --> se sale del for
        /*********************************************************************/
        for (int i = 0; i < 10; i++) {
            mensaje = "Esto en la vuelta " + i + " del bucle";
            System.out.println(mensaje);
            if (i == 4) {
                System.out.println("entro al if");
                break;
            }
            System.out.println("primera línea después del if");
        }
        System.out.println("Me acabo de salir del for");

        /*********************************************************************/
        // continue en bucle for --> salta a la siguiente vuelta de bucle
        /*********************************************************************/
        for (int i = 0; i < 10; i++) {
            mensaje = "Esto en la vuelta " + i + " del bucle";
            System.out.println(mensaje);
            if (i == 4) {
                System.out.println("entro al if");
                continue;
            }
            System.out.println("código después del if");
        }
        System.out.println("Me acabo de salir del for");

        /*********************************************************************/
        // break-continue en bucle while
        /*********************************************************************/
        int j = 0;
        while (j < 10) {
            ++j;
            System.out.println("estoy en la vuelta: " + j);
            if (j == 4) {
                System.out.println("entro al if");
                break; //continue
            }
            System.out.println("código después del if");
        }
        System.out.println("salgo del While");

        /*********************************************************************/
        //etiquetar el salto
        // ejemplo bucle for anidado, con dos bucles etiquetados
        /*********************************************************************/
        bucle1:
        for (int k = 1; k < 5; k++) {
            bucle2:
            for (int l = 1; l < 10; l++) {
                System.out.println("esto en la vuelta (k, l): " + k + " , " + l);
                if (k == 1 && l == 2) {
                    System.out.println("entro al if y salto");
                    break; // continue;
                    //break bucle1; //break bucle2;
                    //continue bucle1; //continue bucle2;
                }
                System.out.println("fuera del if");
            }
            System.out.println("fuera del for -- l");

        }
        System.out.println("fuera del for --- k");
    }
}