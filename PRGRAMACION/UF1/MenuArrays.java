package programacion.curso;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Scanner;

public class MenuArrays {

    public static void main(String[] args) {
        /*********************************************************************/
        /*  ---------------------- Menú opciones ----------------------------*/
        /*********************************************************************/
        Scanner scanner = new Scanner(System.in);
        String entrada;
        boolean salir = false;
        Double[] miArray = new Double[5];

        do {
            System.out.println("************************************************");
            System.out.println("Elige una de las siguientes opciones");
            System.out.println("1. Rellenar Array");
            System.out.println("2. Mostrar el Array");
            System.out.println("3. Ordenar y mostrar el Array");
            System.out.println("4. Sumar todos los elementos");
            System.out.println("5. Separar pares e impares -- solo con enteros");
            System.out.println("0. salir del programa");
            System.out.println("************************************************");
            System.out.println("Introduce la opción elegida:");
            entrada = scanner.next();

            switch (entrada) {
                case "1":
                    int i = 0;
                    while (i < 5) {
                        try {
                            System.out.println("Introduce un valor decimal");
                            miArray[i] = scanner.nextDouble();
                            i++;

                        } catch (Exception e) {
                            System.out.println("valor introducido incorrecto");
                            scanner.next();
                        }
                    }

                    break;
                case "2":
                    System.out.println("El array contiene los valores:");
                    // mostramos por pantalla
                    for (int j = 0; j < miArray.length; j++) {
                        System.out.println("el valor es: " + miArray[j]);
                    }
                    break;
                case "3":
                    Arrays.sort(miArray);
                    System.out.println("El array contiene los valores:");
                    // mostramos por pantalla
                    for (double num:miArray) {
                        System.out.println("el valor es: " + num);
                    }

                    break;
                case "4":
                    double total = 0;
                    for (int j = 0; j < miArray.length; j++) {
                        total += miArray[j];

                    }
                    System.out.println(" El total es: " + total);
                    break;
                case "5":
                    Double pares[] = new Double[5];
                    Double impares[] = new Double[5];
                    int a=0, b=0;
                    for (int j = 0; j < miArray.length; j++) {
                        if ((miArray[j]% 2) == 0){
                            pares[a] = miArray[j];
                            a++;
                        }else{
                            impares[b] = miArray[j];
                            b++;
                        }

                    }
                    System.out.println("Los números pares son: ");
                  for (Double numero: pares){
                      if (numero!=null)
                      System.out.println(numero);
                  }
                    System.out.println("y los impares: ");
                    for (Double numero: impares){
                        if (numero!=null)
                        System.out.println(numero);
                    }
                    break;

                case "0":
                    salir = true;
                    System.out.println("adios!!");
                    break;
                default:
                    System.out.println("opción no válida");
                    break;
            }
        } while (!salir);
    }
}
