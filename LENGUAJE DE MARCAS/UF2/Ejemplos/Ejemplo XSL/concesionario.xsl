﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:variable name="header">
  <tr>
    <th style="text-align: left;">Tipo</th>
    <th style="text-align: left;">Matricula</th>
  </tr>
</xsl:variable>

<xsl:template match="/">
  <html>
  <head>
    <style>
      .ficha{
        border-bottom:1px solid black; 
        margin-bottom:5px;
        padding:10px;
        width:50%;
      }
      .left{
        width:50%;
        float:left;
      }

      table{
         border-collapse: collapse;
         width:50%;
      }
    </style>  
  </head>
  <body>
  
  <xsl:for-each select="concesionario/vehiculo">
  
   <xsl:variable name="estilo-marca">
      <xsl:choose>
          <xsl:when test="tipo='Coche'">
            font-weight:bold;
          </xsl:when>
          <xsl:when test="tipo='Moto'">
            color:blue;
          </xsl:when>
          <xsl:otherwise>
            text-decoration:underline;
          </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>  
  
    <div class="ficha">
          
          <div class="left">
            <div class="datos">
              <xsl:attribute name="style">
                <xsl:value-of select="$estilo-marca"/> 
              </xsl:attribute>
              <xsl:value-of select="marca"/>
            </div>
          </div>
          <a href="{foto}"><img src="{foto}"  width="50%"/></a>
         
          <p class="coment"><xsl:value-of select="comentario"/></p>
          <table border="1">
          <xsl:copy-of select="$header" />
            <tr>
              <td><xsl:value-of select="tipo"/></td>
              <td><xsl:value-of select="matricula"/></td>
            </tr>
        </table>
        </div>
    </xsl:for-each>
  </body>
  </html>
</xsl:template>

</xsl:stylesheet>