(:Muestra todas los películas que son largometrajes con todos sus datos, en una tabla HTML.:)

<html>
<body>
<table border="1">
<tr>
  <th>Id</th>
  <th>Título</th>
  <th>Directores</th
  ><th>Tipo</th>
  <th>Precio</th>
  <th>Sinopsis</th>
</tr>
{
  for $x in db:open("cine")/cine/pelicula
    where $x/tipo ='Largometraje'
    return <tr>
      <td>{data($x/id)}</td>
      <td>{data($x/titulo)}</td>
      <td>{data($x/directores)}</td>
      <td>{data($x/tipo)}</td>
      <td>{data($x/precio)}</td>
      <td>{data($x/sinopsis)}</td>
    </tr>
}
</table>
</body>
</html>