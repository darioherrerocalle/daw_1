for $act in db:open("vehiculos")/coches/vehiculo
  let $marca := $act/marca
  let $modelo := $act/modelo
  where $act/precio > 20000
return (data($marca), data($modelo))