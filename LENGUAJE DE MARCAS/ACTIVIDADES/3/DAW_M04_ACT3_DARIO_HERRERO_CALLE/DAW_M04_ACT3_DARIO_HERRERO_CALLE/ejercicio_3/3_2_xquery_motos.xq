<html>
  <body>
    <table border="1">
      <tr>
        <td>Marca</td>
        <td>Modelo</td>
        <td>Matricula</td>
        <td>Puertas</td>
        <td>Foto</td>
        <td>Color</td>
        <td>Tipologia</td>
        <td>Precio</td>
        <td>Comentario</td>
        
        {
          for $act in db:open("vehiculos")/coches/vehiculo
            let $marca := $act/marca
            let $modelo := $act/modelo
            let $matricula := $act/matricula
            let $puertas := $act/puertas
            let $foto := $act/foto
            let $color := $act/color
            let $tipologia := $act/tipologia
            let $precio := $act/precio
            let $comentario := $act/comentario
            where $act/tipologia = 'Moto'
          return 
            <tr>
             <td>{data($marca)}</td>
             <td>{data($modelo)}</td>
             <td>{data($matricula)}</td>
             <td>{data($puertas)}</td>
             <td><a href="{data($foto)}"><img src="{data($foto)}" width="200px" height="112px" /></a></td>
             <td>{data($color)}</td>
             <td>{data($tipologia)}</td>
             <td>{data($precio)}</td>
             <td>{data($comentario)}</td>
            </tr>
        }
      </tr>
    </table>
  </body>
</html>