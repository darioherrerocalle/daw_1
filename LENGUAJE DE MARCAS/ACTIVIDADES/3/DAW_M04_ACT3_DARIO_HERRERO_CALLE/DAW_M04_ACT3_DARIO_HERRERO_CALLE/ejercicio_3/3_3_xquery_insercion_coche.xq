insert node
  <vehiculo>
    <marca>Audi</marca>
    <modelo>E-Tron</modelo>
    <matricula>4531-AAA</matricula>
    <puertas>5</puertas>
    <foto>https://www.diariomotor.com/imagenes/picscache/750x/audi-e-tron-sportback-011_750x.jpg</foto>
    <color>Negro</color>
    <tipologia>Coche</tipologia>
    <precio>80000</precio>
    <comentario>Electric has gone thrilling. SUV 100% eléctrico, coupé deportivo con paquete S line. Autonomía de 446 km (WLTP). Potencia de 300 kW (408 CV). Carga ultrarrápida de 150 kW.</comentario>
  </vehiculo>
into /coches