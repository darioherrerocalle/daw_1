﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:template match="coches">
    <html>
      <head>
        <style type="text/css">
          table, td, th{
           border: 1px solid black;
           font-family: "calibri", "Times New Roman", Times, serif;
           padding: 2px;
          }
          table{
            border-collapse: collapse;
            width: 70%;
          }
          
        </style>
      </head>
      <body>
        <table>
          <tr>
            <th>Tipo</th>
            <th>Matricula</th>
            <th>Modelo</th>
          </tr>
          <xsl:apply-templates select="vehiculo"/>
        </table>
      </body>
    </html>
  </xsl:template>
  
  <xsl:template match="vehiculo">
    <xsl:choose>
      <xsl:when test="tipologia='Coche'">
        <tr style="background-color: lime">
          <td><xsl:value-of select="tipologia"/></td>
          <td><xsl:value-of select="matricula"/></td>
          <td><xsl:value-of select="modelo"/></td>
         </tr>
      </xsl:when>
      <xsl:when test="tipologia='Moto'">
        <tr style="background-color: yellow">
          <td><xsl:value-of select="tipologia"/></td>
          <td><xsl:value-of select="matricula"/></td>
          <td><xsl:value-of select="modelo"/></td>
         </tr>
      </xsl:when>
      <xsl:otherwise>
        <tr style="background-color: cyan">
          <td><xsl:value-of select="tipologia"/></td>
          <td><xsl:value-of select="matricula"/></td>
          <td><xsl:value-of select="modelo"/></td>
         </tr>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
</xsl:stylesheet>