﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:template match="/">
    <html>
      <head>
        <style type="text/css">
          *{
            font-family: calibri;
          }
          .flex-container{
            display: flex;
          }
          
          .contenedor-principal{
            border: 2px solid blue;
            padding: 10px;
          }
          
          div{
            margin: 10px;
          }
          
          img{
            width: 400px;
            height: 225px;
            object-fit: cover;
          }
        </style>
      </head>
      <body>
        <xsl:for-each select="coches/vehiculo">
          <div class="contenedor-principal">
        
            <div class="flex-container">
              <div>
                <a href="{foto}">
                  <img>
                    <xsl:attribute name="src">
                      <xsl:value-of select="foto"/>
                    </xsl:attribute>
                  </img>
								</a>
              </div>
              <div>
                <p><b><xsl:value-of select="marca"/></b></p>
                <br/>
                <p>Matrícula: <xsl:value-of select="matricula"/></p>
                <p>Puertas: <xsl:value-of select="puertas"/></p>
                <p>Color: <xsl:value-of select="color"/></p>
                <p>Precio: <xsl:value-of select="precio"/></p>
              </div>
            </div>
        
         <div>
          <p style="padding: 10px"><xsl:value-of select="comentario"/></p>
        </div>
      </div>
    </xsl:for-each>
      </body>
    </html>
  </xsl:template> 
</xsl:stylesheet>
